<?php
include('db.php');
$id                               = $_POST['id'];
$bride_name                       = $_POST['bride_name'];
$groom_name                       = $_POST['groom_name'];
$referred_by                      = $_POST['referred_by'];
$event_date                       = $_POST['event_date'];
$number_guests                    = $_POST['number_guests'];
$contact_email                    = $_POST['contact_email'];
$contact_number_first             = $_POST['contact_number_first'];
$contact_number_second            = $_POST['contact_number_second'];
$event_notes_description          = $_POST['event_notes_description'];
$event_budget                     = $_POST['event_budget'];
$ceremony_start_time              = $_POST['ceremony_start_time'];
$ceremony_location                = $_POST['ceremony_location'];
$cocktails_start_time             = $_POST['cocktails_start_time'];
$cocktails_location               = $_POST['cocktails_location'];
$reception_start_time             = $_POST['reception_start_time'];
$reception_location               = $_POST['reception_location'];
$setup_available                  = $_POST['setup_available'];
$event_end                        = $_POST['event_end'];
$bouquet_deliverey_time           = $_POST['bouquet_deliverey_time'];
$bouquet_delivery_location        = $_POST['bouquet_delivery_location'];
$bridal_bouquet_desire_type       = $_POST['bridal_bouquet_desire_type'];
$bridalmaids_bouquet_desire_type  = $_POST['bridalmaids_bouquet_desire_type'];
$bridals_maids_bouquets_number    = $_POST['bridals_maids_bouquets_number'];
$groomsmen_boutnaries_desire_type = $_POST['groomsmen_boutnaries_desire_type'];
$groomsmen_boutnaries             = $_POST['groomsmen_boutnaries'];
$number_tables                    = $_POST['number_tables'];
$type_tables                      = $_POST['type_tables'];
$arch_florals_greenery            = $_POST['arch_florals_greenery'];
$arch_type                        = $_POST['arch_type'];
$status                           = $_POST['status'];
date_default_timezone_set("America/Los_Angeles");
$updated_at                       = date('Y-m-d H:i:s');
$sql = "UPDATE `event_questionare` SET `groom_name`='$groom_name',`bride_name`='$bride_name',`referred_by`='$referred_by',`event_date`='$event_date',`number_guests`='$number_guests',`contact_email`='$contact_email',`contact_number_first`='$contact_number_first',`contact_number_second`='$contact_number_second',`event_notes`='$event_notes_description',`event_budget`='$event_budget',`ceremony_start_time`='$ceremony_start_time',`ceremony_location`='$ceremony_location',`cocktails_start_time`='$cocktails_start_time',`cocktails_location`='$cocktails_location',`reception_start_time`='$reception_start_time',`reception_location`='$reception_location',`setup_available`='$setup_available',`event_end`='$event_end',`bouquet_deliverey_time`='$bouquet_deliverey_time',`bouquet_delivery_location`='$bouquet_delivery_location',`bridal_bouquet_desire_type`='$bridal_bouquet_desire_type',`bridalmaids_bouquet_desire_type`='$bridalmaids_bouquet_desire_type',`bridals_maids_bouquets_number`='$bridals_maids_bouquets_number',`groomsmen_boutnaries_desire_type`='$groomsmen_boutnaries_desire_type',`groomsmen_boutnaries`='$groomsmen_boutnaries',`number_tables`='$number_tables',`type_tables`='$type_tables',`arch_florals_greenery`='$arch_florals_greenery',`arch_type`='$arch_type', `status` = '$status', `last_updated` = '$updated_at' WHERE `event_id` = '$id'";

if ($con->query($sql) === TRUE) {

    header('location:index.php');
} 
else {
    header('location:error.php');
    }
$con->close();

?>