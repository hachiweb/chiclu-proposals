<?php
include('header.php');
?>
<section class="container-fluid mt-3">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-3">
            <div class="bg_color_set">
                <div class="p-3">
                    <h4 class="d-inline text-white font-weight-normal">Buyer</h4>
                </div>
            </div>
            <div class="bg-white" style="padding-bottom:129px;">
                <div class="row pt-5">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <img src="img/logo122.jpg" alt="img not found" width="150">
                            <p class="font-weight-normal mt-2"><span class="font-weight-bold"> E-mail:
                                </span>insservicesusa@gmail.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="bg_color_set ">
                <div class="p-3">
                    <span class="h4 text-white font-weight-normal">Quotes Info</span>
                    <button class="btn btn-info border-white text-white corner_set float-right" type="button"
                        data-toggle="modal" data-target="#myModal2">
                        <i class="fa fa-pencil fa-fw"></i> Edit</button>
                    <!-- The Modal -->
                    <div class="modal fade" id="myModal2">
                        <div class="modal-dialog  modal-dialog-centered">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header bg_color_set">
                                    <h4 class="modal-title text-white">Edit Quote Details</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="form-inline text-center">
                                        <span class="ml-5">Need by Date:</span>
                                        <input id="datepicker" width="200" style="border-radius:0;margin-left:20px;">
                                    </div>
                                </div>

                                <!-- Modal footer -->
                                <!-- <div class="border-top mx-3">
                                    <div class=" text-center">
                                        <button type="submit" class="btn btn-info corner_set btn_color"
                                            data-dismiss="modal">Done</button></div>
                                </div> -->

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="bg-white" style="padding-bottom:135px;">
                <div class="row pt-5">
                    <div class="col-lg-6">
                        <div class="ml-4 font-weight-bold">
                            <p>Event Date: </p>
                            <p>Need by Date:<span class="font-weight-normal"> Aug 5, 2019</span></p>
                            <span class="font-weight-normal">Shipping Info</span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mr-4 font-weight-bold float-right">
                            <p>Total Stems:<span class="font-weight-normal"> 0</span> </p>
                            <p>Ask Total:<span class="font-weight-normal"> $0.00</span></p>
                            <span style="cursor: pointer;" data-toggle="modal" data-target="#myModal"><i
                                    class="fa fa-pencil fa-fw"></i> Edit</span> &nbsp;&nbsp;&nbsp;
                            <span style="cursor: pointer;"><i class="fa fa-clipboard fa-fw"></i> Copy</span>
                        </div>
                    </div>
                    <!-- The Modal -->
                    <div class="modal fade" id="myModal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header bg_color_set">
                                    <h4 class="modal-title text-white"> Edit Address</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <form action="#" method="POST">
                                    <div class="modal-body">
                                        <table class="table table-striped">
                                            <tbody class="text-right">
                                                <tr>
                                                    <td>Address Line 1: </td>
                                                    <td>
                                                        <input type="text" class="form-control corner_set"
                                                            placeholder="Address Line 1" name="address_line_1" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Address Line 2: </td>
                                                    <td> <input type="text" class="form-control corner_set"
                                                            placeholder="Address Line 2" name="address_line_2" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>City:</td>
                                                    <td> <input type="text" class="form-control corner_set"
                                                            placeholder="City" name="city" value=""></td>
                                                </tr>
                                                <tr>
                                                    <td>State/Province/Region:</td>
                                                    <td> <input type="text" class="form-control corner_set"
                                                            placeholder="State/Province/Region"
                                                            name="state_province_region" value=""></td>
                                                </tr>
                                                <tr>
                                                    <td>ZIP / Postal Code: </td>
                                                    <td> <input type="number" class="form-control corner_set"
                                                            placeholder="ZIP / Postal Code" name="zip_postal_code"
                                                            value=""></td>
                                                </tr>
                                                <tr>
                                                    <td>Country:</td>
                                                    <td> <input type="text" class="form-control corner_set"
                                                            placeholder="Country" name="country" value=""></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="border-top mx-3">
                                        <div class=" text-center">
                                            <button type="submit" class="btn btn-info corner_set btn_color"
                                                data-dismiss="modal">Done</button></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="heading mx-2 mb-4"></div>
                <span class="ml-4">Allen Flowers</span>
            </div>

        </div>
        <div class="col-md-3">
            <div class="bg_color_set ">
                <div class="p-3">
                    <h4 class="d-inline text-white font-weight-normal">Seller</h4>
                </div>
            </div>
            <div class="bg-white" style="padding-bottom:89px;">
                <div class="row pt-3">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <img src="img/flower_farm.png" alt="img not found" width="250">
                            <p class="mt-2"><span class="font-weight-bold text-dark">E-mail: sales@flowerfarm.com
                                </span></p>
                            <p><span class="font-weight-bold text-dark">Phone: 6302971941
                                </span></p>
                            <p class="font-weight-normal "><span class="font-weight-bold text-dark">Website:</span>
                                <a href="http://www.flowerfarm.com" style="color: #009198;">
                                http://www.flowerfarm.com </a></p>
                            <button class="btn btn-info  border-white text-white corner_set" type="button">About Farm
                                Fresh Exports</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row mt-3">
        <div class="col-md-1"></div>
        <div class="col-md-5">
            <div class="bg_color_set ">
                <div class="p-3">
                    <span class="h4 text-white font-weight-normal">Message</span>
                    <button class="btn btn-info border-white text-white corner_set float-right" type="button">
                        <i class="fa fa-refresh fa-fw"></i> Check Messages</button>
                </div>
            </div>
            <div class="bg-white" style="">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mt-3 m-3 border_hover" style="height:250px;border:3px solid rgb(221,221,221);">
                            <p>Use this area to talk to the seller.</p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="p-3">
                            <textarea name="" class="form-control bg-color corner_set" id="" cols="" rows="6" placeholder="Type Message....."></textarea>
                        </div>
                        <div class="text-center pb-3">
                            <button class="btn btn-info border-white text-white corner_set" type="button">
                                <i class="fa fa-paper-plane fa-fw"></i> Send </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-5">
            <div class="bg_color_set ">
                <div class="p-3">
                    <span class="h4 text-white font-weight-normal">Quote Activity Log</span>
                    <p class="float-right text-white"><span class="h5">Status:</span> Strated</p>
                </div>
            </div>
            <div class="bg-white">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped">
                            <tbody class="">
                                <tr  style="cursor: pointer;">
                                    <td class="text-center" colspan="4"><span>---------- Monday, Aug. 19, 2019
                                            ----------</span></td>
                                </tr>
                                <tr class="text-center"  style="cursor: pointer;">
                                    <td>3:34 AM UTC</td>
                                    <td> <span class="font-weight-bold">Allen</span></td>
                                    <td></td>
                                    <td> <span> Started request for quote.</span></td>
                                </tr>
                                <tr class="text-center">
                                    <td> <span></span></td>
                                    <td> <span class="font-weight-bold"></span></td>
                                    <td></td>
                                    <td> <span> </span></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="text-center" style="margin-top:360px;">
                            <p><span>Last Change: </span>August 19, 2019 - 3:34 AM UTC</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row mt-3">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="bg_color_set ">
                <div class="p-3">
                    <span class="h4 text-white font-weight-normal">Request Items</span>
                    <button class="btn btn-info border-white text-white corner_set float-right print-button" type="button">
                        <i class="fa fa-print fa-fw"></i> Print Table</button>


                </div>
            </div>
            <div class="bg-white">
                <div class="row pb-3">
                    <div class="col-lg-12">
                        
                        <div class="text-center mt-3 ">
                            <p class="font-weight-bold">There are currently no items in this quote, please select items using the button below.</p>

                            <button class="btn btn-info  border-white text-white corner_set" type="button">Add Items</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="heading mt-3"></div>
    <div class="text-center mt-4 pb-4">
    <button class="btn btn-info  border-white text-white corner_set" type="button">Save & Send</button>
    </div>
</section>

<script>
$('#datepicker').datepicker({
    uiLibrary: 'bootstrap4'
}); //datepicker

$('.print-button').on('click', function() {  
  window.print();  
  return false; // why false?
});
</script>
</body>

</html>