<div class="">
  <div class="row row-5 mt-3">
    <div class="col-lg-12">

      <?php
      if ($page != "worksheet") {
        ?>
        <?php /* ?>
        <div class="bg_color_set p-1">
          <h4 class="font-weight-normal text-white my-3 ml-2">Design Board</h4>
        </div>
        <?php */ ?>

        <div class="bg_color_set title-dv py-3">
          <div class="row">
            <div class="col-lg-12">
              <h4 class="d-inline text-white ml-3">Design Board</h4>
            </div><!-- col -->

            <?php /* ?>
            <div class="col-lg-6">
              <div class="text-right">
                <div class="dropdown mr-3 worksheet-dropdown">
                  <span class="dropdown-toggle text-white" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#9776;</span>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <button id="design-board-click"  class="dropdown-item border-white hover_set" type="button">
                      <i class="fa fa-pencil fa-fw"></i> Design Board
                    </button>
                    <button id="edit-worksheet-click" class="dropdown-item border-white hover_set" type="button">
                      <i class="fa fa-wrench fa-fw"></i> Edit Worksheet
                    </button>
                    <button id="items-used-click" class="dropdown-item border-white hover_set" type="button">
                      <i class="fa fa-clipboard fa-fw"></i> Items Used
                    </button>
                  </div>
                </div>
              </div>
            </div><!-- col -->
            <?php */ ?>

          </div><!-- row -->
        </div>

        <?php
      } else {
        ?>  
        <div class="bg_color_set title-dv py-3">
          <div class="row">
            <div class="col-lg-6 col-md-4 col-sm-4">
              <h4 class="d-inline text-white ml-3">Design Board</h4>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-4">
              <div class="text-right">
                <div class="dropdown mr-5">
                  <span class="dropdown-toggle  text-white" style="font-size:25px;cursor:pointer"
                  id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                  aria-expanded="false">&#9776;</span>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <button class="dropdown-item border-white hover_set" type="button"><i
                      class="fa fa-pencil fa-fw"></i> Design Board</button>
                      <button class="dropdown-item border-white hover_set" type="button"><i
                        class="fa fa-wrench fa-fw"></i> Edit Worksheet</button>
                        <button class="dropdown-item border-white hover_set" type="button"><i
                          class="fa fa-clipboard fa-fw"></i> Design Board</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row p-2">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <p class="d-inline ml-2">
                    <i class="fa fa-caret-down fa-fw"></i>Design Board
                  </p>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="">
                    <input type="file" class="custom-file-input" id="customFile" name="filename">
                    <label class="custom-file-label" style="width:90%;" for="customFile">Add Recipe</label>
                  </div>
                </div>
              </div>
              <?php
            }

            ?>


            <div class="bg-white">
              <div class="pt-4">
                <section class="dropdown-click-left-sec dropdown-click-image-sec">
                 <div id="" class="">
                   <div class="border-bottom mx-3"> Recipe</div>
                   <div class="row mt-3 mx-2">
                     <?php 
                     $event_id = $_SESSION["ID"];  
                     $sql = "SELECT * FROM  bookmark  WHERE source = 'recipe' AND event_id = '$event_id'";
                     $image_result = $con->query($sql);
                     $recipe_images = array();
                     while ($row = $image_result->fetch_assoc()) {


                      array_push($recipe_images,$row["img"]);


                      ?>
                      <div class="image-grid-dv-bx " data-toggle="modal" data-target="#model">
                       <div class="image-grid-dv-one d-flex flex-column justify-content-end">
                         <div class="img">
                           <img data-toggle="modal" data-target="#recipe_model<?php echo $row['img_id']; ?>" src="img/<?=$row["img"] ?>" alt="Image">
                         </div>
                         <!-- <h6 class="title m-0 p-3"></h6> -->
                       </div>
                     </div>
                     <?php
                   }
                   ?>
                 </div><!-- row -->
               </div><!-- image-grid-bx -->


               <!-- item -->


               <?php
               $sql = "SELECT DISTINCT item_gallery.item_type FROM item_gallery INNER JOIN bookmark ON item_gallery.name = bookmark.img";
               $res = $con->query($sql);

               $categories = array();
               $images = array();
               while ($row = $res->fetch_assoc()) {
                 array_push($categories,$row["item_type"]);
               }

               foreach ($categories as $category) {
                 ?>
                 <div id="" class="">
                   <div class="border-bottom mx-3"> <?=$category?></div>
                   <div class="row mt-3 mx-2">
                     <?php 

                     $sql = "SELECT item_gallery.name,bookmark.img,bookmark.img_id FROM item_gallery INNER JOIN bookmark ON  item_gallery.name = bookmark.img WHERE bookmark.source = 'item' AND bookmark.event_id = '$event_id'  AND item_gallery.item_type = '$category'";


                     $image_result = $con->query($sql);

                     while ($row = $image_result->fetch_assoc()) {


                      array_push($images,$row["img"]);


                      ?>
                      <div class="image-grid-dv-bx" data-toggle="modal" data-target="#model">
                       <div class="image-grid-dv-one d-flex flex-column justify-content-end">
                         <div class="img">
                           <img data-toggle="modal" data-target="#item_model<?php echo $row['img_id']; ?>" src="img/<?=$row["img"] ?>" alt="Image">
                         </div>
                         <!-- <h6 class="title m-0 p-3"></h6> -->
                       </div>
                     </div>
                     <?php
                   }
                   ?>
                 </div><!-- row -->
               </div><!-- image-grid-bx -->
               <?php
             }
             ?>
           </section><!-- dropdown-click-left-sec -->

           
           <?php /* ?>
           <section class="dropdown-click-left-sec worksheet-left-sec d-none">
             <section class="dropdown-click-sec p-2 mb-4">
              <div class="heading px-3">
                <span class="dropdown-click">
                  <i class="fa fa-caret-down" aria-hidden="true"></i>
                </span>
                <span>Worksheet Sections</span>
              </div>

              <div class="dropdown-bx">
               <ul class="add-new-sec-list">
                <li class="box-shadow p-2 my-3 bg-gray">
                  <a class="" href="javascript:void(0);">
                    <i class="fa fa-th-list" aria-hidden="true"></i>
                    <span>Category Group</span>
                  </a>
                </li>
                <li class="box-shadow p-2 my-3 bg-gray">
                  <a class="" href="javascript:void(0);">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span>Tables &amp; Seating</span>
                  </a>
                </li>
                <li class="box-shadow p-2 my-3 bg-gray">
                  <a class="" href="javascript:void(0);">
                    <i class="fa fa-paint-brush" aria-hidden="true"></i>
                    <span>Color Palette</span>
                  </a>
                </li>
                <li class="box-shadow p-2 my-3 bg-gray">
                  <a class="" href="javascript:void(0);">
                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                    <span>Discounts</span>
                  </a>
                </li>
                <li class="box-shadow p-2 my-3 bg-gray">
                  <a class="" href="javascript:void(0);">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                    <span>Fees</span>
                  </a>
                </li>
                <li class="box-shadow p-2 my-3 bg-gray">
                  <a class="" href="javascript:void(0);">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                    <span>Taxes</span>
                  </a>
                </li>
                <li class="box-shadow p-2 my-3 bg-gray">
                  <a class="" href="javascript:void(0);">
                    <i class="fa fa-handshake-o" aria-hidden="true"></i>
                    <span>Staff &amp; Labor</span>
                  </a>
                </li>
                <li class="box-shadow p-2 my-3 bg-gray">
                  <a class="" href="javascript:void(0);">
                    <i class="fa fa-calculator" aria-hidden="true"></i>
                    <span>Summary</span>
                  </a>
                </li>
              </ul>
            </div><!-- dropdown-bx -->
          </section><!-- dropdown-click-sec -->

          <section class="dropdown-click-sec p-2 mb-4">
            <div class="heading px-3">
              <span class="dropdown-click">
                <i class="fa fa-caret-down" aria-hidden="true"></i>
              </span>
              <span>Line Items</span>
            </div>

            <div class="dropdown-bx">
             <ul class="add-new-sec-list">
              <li class="box-shadow p-2 my-3 bg-gray">
                <a class="" href="javascript:void(0);">
                  <i class="fa fa-th-list" aria-hidden="true"></i>
                  <span>Line Item</span>
                </a>
              </li>
              <li class="box-shadow p-2 my-3 bg-gray">
                <a class="" href="javascript:void(0);">
                  <i class="fa fa-users" aria-hidden="true"></i>
                  <span>Dicount</span>
                </a>
              </li>
              <li class="box-shadow p-2 my-3 bg-gray">
                <a class="" href="javascript:void(0);">
                  <i class="fa fa-paint-brush" aria-hidden="true"></i>
                  <span>Fee</span>
                </a>
              </li>
              <li class="box-shadow p-2 my-3 bg-gray">
                <a class="" href="javascript:void(0);">
                  <i class="fa fa-credit-card" aria-hidden="true"></i>
                  <span>Staff</span>
                </a>
              </li>
            </ul>
          </div><!-- dropdown-bx -->
        </section>

        <section class="dropdown-click-sec p-2 mb-4">
          <div class="heading px-3">
            <span class="dropdown-click">
              <i class="fa fa-caret-down" aria-hidden="true"></i>
            </span>
            <span>Removed Sections</span>
          </div>

          <div class="dropdown-bx">
            <div class="removed-info d-flex my-3 justify-content-between box-shadow p-2 bg-gray">
              <div class="data">
                <div class="text">taxes</div>
                <div class="date">Removed 9/3/2019, 12.56.19 AM</div>
              </div>
              <div class="delete"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
            </div>

            <div class="removed-info d-flex my-3 justify-content-between box-shadow p-2 bg-gray">
              <div class="data">
                <div class="text">taxes</div>
                <div class="date">Removed 9/3/2019, 12.56.19 AM</div>
              </div>
              <div class="delete"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
            </div>
          </div><!-- dropdown-bx -->
        </section>
      </section><!-- dropdown-click-left-sec -->
      <?php */ ?>


      <div class="row mt-3 pb-3">
        <div class="col-sm-12">
          <div class="text-center ">
            <button class="btn btn-info border-white text-white corner_set" type="button"> Up</button>
            <button class="btn btn-info border-white text-white corner_set" type="button"> Down</button>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
</div>
