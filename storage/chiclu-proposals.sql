-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2019 at 02:04 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chiclu-proposals`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

CREATE TABLE `bookmark` (
  `id` int(5) NOT NULL,
  `event_id` int(5) NOT NULL,
  `img` varchar(500) NOT NULL,
  `img_id` int(50) NOT NULL,
  `source` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bouquets`
--

CREATE TABLE `bouquets` (
  `id` int(11) NOT NULL,
  `bouquets_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bouquets_qty` int(11) NOT NULL,
  `bouquets_est_price` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bouquets_lock` int(11) NOT NULL,
  `bouquets_total` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bouquets_tax` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perryman_bridesmaids` blob NOT NULL,
  `ingredients_qty` int(11) NOT NULL,
  `ingredients_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ingredients_cost` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ingredients_price` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ingredients_tax` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ingredient_images` blob NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `apply_to` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `amount` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxed` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_staff_staff_title` int(11) NOT NULL,
  `event_staff_count` int(11) NOT NULL,
  `event_staff_hours` int(11) NOT NULL,
  `event_staff_rate` int(11) NOT NULL,
  `event_staff_taxed` int(11) NOT NULL,
  `event_staff_total` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `name_prefix` varchar(10) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `address` varchar(80) NOT NULL,
  `city` varchar(80) NOT NULL,
  `state` varchar(80) NOT NULL,
  `zip` int(11) NOT NULL,
  `role` varchar(10) NOT NULL DEFAULT 'Event Lead',
  `phone_one` varchar(50) NOT NULL,
  `phone_two` varchar(50) NOT NULL,
  `phone_type_one` varchar(20) NOT NULL,
  `phone_type_two` varchar(20) NOT NULL,
  `contact_notes` varchar(500) NOT NULL,
  `client_pic` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cost`
--

CREATE TABLE `cost` (
  `id` int(11) NOT NULL,
  `item` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `need_have` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty_needed` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `est_total` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty_ordered` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stem_cost` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_cost` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_info`
--

CREATE TABLE `delivery_info` (
  `id` int(11) NOT NULL,
  `bouquet_delivery` time NOT NULL,
  `bouquet_delivery_location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_details`
--

CREATE TABLE `event_details` (
  `id` int(11) NOT NULL,
  `event_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `referred_by` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_date` date NOT NULL,
  `number_of_guests` int(11) NOT NULL,
  `event_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reception_plated` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reception_buffet` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_list`
--

CREATE TABLE `event_list` (
  `id` int(11) NOT NULL,
  `event_date` date NOT NULL,
  `client_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_questionare`
--

CREATE TABLE `event_questionare` (
  `event_id` int(10) NOT NULL,
  `groom_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bride_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referred_by` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_date` date NOT NULL,
  `number_guests` int(10) NOT NULL,
  `contact_email` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number_first` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number_second` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_notes` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_budget` int(10) NOT NULL,
  `ceremony_start_time` time NOT NULL,
  `ceremony_location` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cocktails_start_time` time NOT NULL,
  `cocktails_location` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reception_start_time` time NOT NULL,
  `reception_location` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `setup_available` time NOT NULL,
  `event_end` time NOT NULL,
  `bouquet_deliverey_time` time NOT NULL,
  `bouquet_delivery_location` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bridal_bouquet_desire_type` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bridalmaids_bouquet_desire_type` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bridals_maids_bouquets_number` int(10) NOT NULL,
  `groomsmen_boutnaries_desire_type` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `groomsmen_boutnaries` int(10) NOT NULL,
  `number_tables` int(10) NOT NULL,
  `type_tables` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arch_florals_greenery` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arch_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_questionare`
--

INSERT INTO `event_questionare` (`event_id`, `groom_name`, `bride_name`, `referred_by`, `event_date`, `number_guests`, `contact_email`, `contact_number_first`, `contact_number_second`, `event_notes`, `event_budget`, `ceremony_start_time`, `ceremony_location`, `cocktails_start_time`, `cocktails_location`, `reception_start_time`, `reception_location`, `setup_available`, `event_end`, `bouquet_deliverey_time`, `bouquet_delivery_location`, `bridal_bouquet_desire_type`, `bridalmaids_bouquet_desire_type`, `bridals_maids_bouquets_number`, `groomsmen_boutnaries_desire_type`, `groomsmen_boutnaries`, `number_tables`, `type_tables`, `arch_florals_greenery`, `arch_type`, `status`, `last_updated`) VALUES
(1, 'husband', 'wife', 'tetidu@mailinator.net', '2019-04-16', 9, 'piqe@mailinator.net', 'pufagyred@mailinator.com', 'luzygeba@m', 'Aliquam voluptate ve', 378, '20:34:00', 'dorigisyv@mailinator.com', '19:05:00', 'vowajose@mailinator.com', '21:04:00', 'tajybam@mailinator.com', '03:07:00', '03:34:00', '10:40:00', 'zuqe@mailinator.net', 'Est atque minus sun', 'Optio omnis sunt e', 0, 'Sint excepteur eu ac', 0, 0, 'lunep@mailinator.com', 'Dolores nobis est ad', 'ruvyzy@mailinator.com', 'Proposal Needed', '2019-08-25 10:41:45'),
(2, 'quvejevam@mailinator.net', 'muheqixyx@mailinator.com', 'zaqepocoq@mailinator.com', '1970-06-01', 442, 'vofabefimi@mailinator.net', '335', '352', 'Ad quis a et enim ob', 591, '13:47:00', 'fykalamapo@mailinator.com', '18:34:00', 'togucul@mailinator.com', '03:07:00', 'pozawexero@mailinator.com', '03:07:00', '05:46:00', '23:49:00', 'jozeb@mailinator.net', 'Voluptatum dignissim', 'Quis expedita obcaec', 132, 'Qui nostrud cumque a', 217, 921, 'ziqu@mailinator.net', 'In autem cupiditate ', 'podexyv@mailinator.net', '', '2019-08-25 10:40:20');

-- --------------------------------------------------------

--
-- Table structure for table `event_schedule`
--

CREATE TABLE `event_schedule` (
  `id` int(11) NOT NULL,
  `ceremony_start_time` time NOT NULL,
  `ceremony_location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cocktails_start_time` time NOT NULL,
  `cocktails_location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reception_start_time` time NOT NULL,
  `reception_location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `setup_available` time NOT NULL,
  `photographer_start` time NOT NULL,
  `strike_begin` time NOT NULL,
  `company_arrival` time NOT NULL,
  `event_end` time NOT NULL,
  `strike_concludes` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `imagef` longtext NOT NULL,
  `recipe_name` text NOT NULL,
  `description` varchar(200) NOT NULL,
  `cost_a` float NOT NULL,
  `cost_b` float NOT NULL,
  `cost_c` float NOT NULL,
  `markup` text NOT NULL,
  `price` float NOT NULL,
  `category` text NOT NULL,
  `styles` text NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `parent_id` int(11) NOT NULL,
  `source` text NOT NULL,
  `item_name` text NOT NULL,
  `qty` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `name`, `imagef`, `recipe_name`, `description`, `cost_a`, `cost_b`, `cost_c`, `markup`, `price`, `category`, `styles`, `is_active`, `parent_id`, `source`, `item_name`, `qty`) VALUES
(1, 'FL (3).jpeg', '', 'Recipe 1', 'Between a Rock and a Hard Place', 0, 0, 0, 'Jig Is Up', 7.19, 'Aisle Décor', 'Traditional', 0, 0, 'Recipe', 'Sydney Gonzales,Astra Larson', '23,43'),
(2, 'FL (13).jpg', '', 'Kirsten Hays', 'Blanditiis aut provident ullam in laboris nostrum', 0, 0, 0, 'Et accusamus ex provident reprehenderit ducimus', 819, 'Aisle Décor', 'Beach,Birthday,Destination,Edgy,Elegant,Garden,Holiday,Modern,Nautical,Organic,Outdoor,Romantic,Rustic,Tall,Themed,Traditional,Vintage', 0, 0, 'Recipe', 'Item 1,Astra Larson,Driscoll Trevino', '34,43,34'),
(6, 'image4.jpg', '', 'Germaine Nicholson', 'Nam quos pariatur Q', 0, 0, 0, 'Eum numquam nihil ex', 715, 'Aisle Décor', 'Beach,Destination,Elegant,Garden,Holiday,Modern,Outdoor,Tall,Themed,Vintage', 0, 0, 'Recipe', 'Sydney Gonzales,Item 1', '543,43'),
(7, 'image.jpg', '', 'Xantha Espinoza', 'Velit labore laborum', 0, 0, 0, 'Nisi inventore corpo', 888, 'Aisle Décor', 'Birthday,Garden,Modern,Romantic,Themed', 0, 0, 'Recipe', 'Driscoll Trevino,Astra Larson', '942,43'),
(9, 'flower.jpeg', '', 'Stacey Avila', 'Quis rerum excepturi', 0, 0, 0, 'Duis ut aperiam maio', 992, 'Aisle Décor', 'Birthday,Elegant,Holiday,Nautical,Organic,Romantic,Tall,Traditional', 0, 0, 'Recipe', 'Sydney Gonzales,Astra Larson', '923,43'),
(10, 'dahlia-flowers.png', '', 'Olympia Hutchinson', 'Sit ipsum illo beat', 0, 0, 0, 'Consequat Numquam a', 696, 'Aisle Décor', 'Beach,Birthday,Elegant,Nautical,Romantic,Rustic,Tall,Vintage', 0, 0, 'Recipe', 'Sydney Gonzales,Astra Larson,Item 1,Driscoll Trevino', '25,24,31,34'),
(12, 'flower.jpeg', '', 'Blair Merrill', 'Et excepteur ipsa N', 0, 0, 0, 'Doloribus mollitia e', 864, 'Aisle Décor', 'Edgy,Elegant,Garden,Holiday,Modern,Nautical,Organic,Outdoor,Tall,Traditional', 0, 0, 'Recipe', 'Astra Larson,Sydney Gonzales,Item 1,', '764,43,31,'),
(13, '43-439598_painting-flower-bouquet-clip-art-leaves-transprent-transparent.png', '', 'Ryder Mcpherson', 'Eum cumque enim quis', 0, 0, 0, 'Aute amet suscipit ', 350, 'Aisle Décor', 'Beach,Birthday,Destination,Elegant,Garden,Holiday,Modern,Nautical,Outdoor,Rustic,Tall,Themed,Traditional', 0, 0, 'Recipe', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL,
  `recipe_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `recipe_name`, `item_name`, `qty`) VALUES
(1, 'Recipe 1', 'Sydney Gonzales,Astra Larson', 23),
(2, 'Kirsten Hays', 'Item 1,Astra Larson,Driscoll Trevino', 34),
(3, '', 'Freedom Rose', 1),
(4, 'Testing', 'Q', 1),
(5, 'Recipe 3', '', 0),
(6, 'ttrerer', '', 0),
(7, 'rtrtrttrt', '', 0),
(8, 'Ha', 'Ha', 1),
(9, 'Testing', 'Hd', 2),
(10, 'Hiram Valentine', 'Astra Larson', 211),
(11, 'Jackson Weber', 'Astra Larson', 610),
(12, 'Sheila Chapman', 'Casey Madden', 713),
(13, 'Joelle Skinner', 'Sopoline Jackson', 414),
(14, 'Germaine Nicholson', 'Sydney Gonzales,Item 1', 543),
(15, 'Xantha Espinoza', 'Driscoll Trevino,Astra Larson', 942),
(16, 'Ian Rosales', 'salad', 945),
(17, 'Stacey Avila', 'Sydney Gonzales,Astra Larson', 923),
(18, 'Kelsie Stark', 'sfsfsd', 334),
(19, 'Kelly Park', 'Xavier Carpenter, Jason Wise', 338),
(20, '', 'c3', 0),
(21, 'Blair Merrill', 'Astra Larson,Sydney Gonzales,Item 1,', 764),
(22, 'Nissim Baker', 'Oprah Haley', 541),
(23, 'Jillian Cochran', 'Abraham Underwood', 518),
(24, 'Ryder Mcpherson', ',Sydney Gonzales,Item 1,h', 0),
(25, 'Odessa Stevenson', '', 0),
(26, 'Amir Mccarthy', 'Astra Larson', 19),
(27, 'Madeline Caldwell', 'Sydney Gonzales', 419),
(28, 'Nayda Miles', 'Driscoll Trevino', 700),
(29, 'Malik Trujillo', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `item_gallery`
--

CREATE TABLE `item_gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` longtext NOT NULL,
  `item_name` text NOT NULL,
  `item_type` text NOT NULL,
  `default_stem_cost` float NOT NULL,
  `stems_per_bunch` int(11) NOT NULL,
  `colors` text NOT NULL,
  `months` text NOT NULL,
  `recipe_name` text NOT NULL,
  `description` varchar(200) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `source` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_gallery`
--

INSERT INTO `item_gallery` (`id`, `name`, `image`, `item_name`, `item_type`, `default_stem_cost`, `stems_per_bunch`, `colors`, `months`, `recipe_name`, `description`, `parent_id`, `source`) VALUES
(1, 'FL (1).jpg', '', 'Item 1', 'Blooms & Greenery', 2.3, 6, 'Green, Red, Purple', 'Array', 'Recipe 1', 'Birds of a Feather Flock Together', 0, 'Item'),
(2, 'FL (4).jpeg', '', 'Sydney Gonzales', 'Blooms & Greenery', 1, 2, 'Green, Red, Purple', 'Array', 'Fugiat cupiditate laboris ea dolores molestiae quo', 'Description', 0, 'Item'),
(3, 'FL (9).jpg', '', 'Astra Larson', 'Table Numbers', 3, 62, 'White, Blue, Yellow, Brown, Valvet', 'April, May, June, September', 'Sapiente ex ea dolores quis sit esse quia veniam', 'Non nesciunt consequuntur deserunt voluptatibus q', 0, 'Item'),
(4, 'FL (11).jpg', '', 'Driscoll Trevino', 'Blooms & Greenery', 5, 10, 'Red, Purple', 'March, April, May', 'Officiis excepteur quaerat pariatur Minim itaque ', 'Lorem Ipsum', 0, 'Item');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `payment_name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `due` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `received_date` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `credit_card` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `proposals`
--

CREATE TABLE `proposals` (
  `id` int(11) NOT NULL,
  `theme` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf_history` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration_date` date NOT NULL,
  `proposal_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `editable_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_visibility` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_name_1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_name_2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `line_1_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `show_bottom_contact_info` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tables_seating`
--

CREATE TABLE `tables_seating` (
  `id` int(11) NOT NULL,
  `estimated_number_of_guests` int(11) NOT NULL,
  `people_at_head_table` int(11) NOT NULL,
  `people_at_sweetheart_table` int(11) NOT NULL,
  `guests_per_table` int(11) NOT NULL,
  `guest_tables_needed` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `team_access`
--

CREATE TABLE `team_access` (
  `id` int(11) NOT NULL,
  `team_member_name` int(11) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `username` varchar(80) NOT NULL,
  `password` text NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'Customer',
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `alias`, `username`, `password`, `role`, `is_active`, `created_at`) VALUES
(1, '', 'abc@mail.com', '25f9e794323b453885f5181f1b624d0b', 'Customer', 1, '2019-08-08 11:10:35'),
(5, '', 'testhachiweb@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Customer', 1, '2019-08-11 05:36:37');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_teams`
--

CREATE TABLE `vendor_teams` (
  `id` int(11) NOT NULL,
  `team_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_name` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `worksheet`
--

CREATE TABLE `worksheet` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` varchar(1000) NOT NULL,
  `item_price` varchar(1000) NOT NULL,
  `Estimated_Number_of_Guests` varchar(50) NOT NULL,
  `People_at_Head_Table` varchar(50) NOT NULL,
  `People_at_Sweetheart_Table` varchar(50) NOT NULL,
  `Guests_Per_Table` varchar(50) NOT NULL,
  `Guest_Tables_Needed` varchar(50) NOT NULL,
  `discount_name` varchar(1000) NOT NULL,
  `discount_apply_to` varchar(1000) NOT NULL,
  `discount_apply_to_2` varchar(1000) NOT NULL,
  `discount_type` varchar(1000) NOT NULL,
  `discount_amt` varchar(1000) NOT NULL,
  `fee_name` varchar(1000) NOT NULL,
  `fee_apply_to` varchar(1000) NOT NULL,
  `fee_type` varchar(1000) NOT NULL,
  `fee_amt` varchar(100) NOT NULL,
  `fee_tax` varchar(1000) NOT NULL,
  `tax_product` varchar(50) NOT NULL,
  `tax_service` varchar(50) NOT NULL,
  `tax_labor` varchar(50) NOT NULL,
  `staff_title` varchar(1000) NOT NULL,
  `staff_count` varchar(1000) NOT NULL,
  `staff_hours` varchar(1000) NOT NULL,
  `staff_rate` varchar(1000) NOT NULL,
  `staff_tax` varchar(1000) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `tags` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `worksheet`
--

INSERT INTO `worksheet` (`id`, `user_id`, `type`, `item_name`, `item_qty`, `item_price`, `Estimated_Number_of_Guests`, `People_at_Head_Table`, `People_at_Sweetheart_Table`, `Guests_Per_Table`, `Guest_Tables_Needed`, `discount_name`, `discount_apply_to`, `discount_apply_to_2`, `discount_type`, `discount_amt`, `fee_name`, `fee_apply_to`, `fee_type`, `fee_amt`, `fee_tax`, `tax_product`, `tax_service`, `tax_labor`, `staff_title`, `staff_count`, `staff_hours`, `staff_rate`, `staff_tax`, `name`, `description`, `tags`, `created_at`) VALUES
(1, 1, 'New Templates', '', 'Fisrt', '$0.00', '0', '0', '0', '0', '0', '', '', '', '', '6.5', '', '', '', '0', '', '2', '6.5', '6.5', '', '0', '0', '6.5', 'no', 'First', 'DEC', 'TAG', '2019-09-03 11:33:03'),
(2, 1, 'New Templates', '', 'TWO', '$0.00', '0', '0', '0', '0', '0', '', '', '', '', '6.5', '', '', '', '0', '', '2', '6.5', '6.5', '', '0', '0', '6.5', 'no', 'TWO', '', '', '2019-09-03 11:46:07'),
(5, 1, 'New Version', 'Hii,Hi', '2,3', '$0.00,$0.00', '0', '0', '0', '0', '0', '', '', '', '', '6.5', '', '', '', '0', '', '2', '6.5', '6.5', '', '0', '0', '6.5', 'no', '', '', '', '2019-09-03 11:57:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookmark`
--
ALTER TABLE `bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bouquets`
--
ALTER TABLE `bouquets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_info`
--
ALTER TABLE `delivery_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_details`
--
ALTER TABLE `event_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_list`
--
ALTER TABLE `event_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_questionare`
--
ALTER TABLE `event_questionare`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `event_schedule`
--
ALTER TABLE `event_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_gallery`
--
ALTER TABLE `item_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tables_seating`
--
ALTER TABLE `tables_seating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_access`
--
ALTER TABLE `team_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE` (`username`);

--
-- Indexes for table `vendor_teams`
--
ALTER TABLE `vendor_teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worksheet`
--
ALTER TABLE `worksheet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookmark`
--
ALTER TABLE `bookmark`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `bouquets`
--
ALTER TABLE `bouquets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cost`
--
ALTER TABLE `cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `delivery_info`
--
ALTER TABLE `delivery_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_details`
--
ALTER TABLE `event_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_list`
--
ALTER TABLE `event_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_questionare`
--
ALTER TABLE `event_questionare`
  MODIFY `event_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `event_schedule`
--
ALTER TABLE `event_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `item_gallery`
--
ALTER TABLE `item_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tables_seating`
--
ALTER TABLE `tables_seating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `team_access`
--
ALTER TABLE `team_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vendor_teams`
--
ALTER TABLE `vendor_teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `worksheet`
--
ALTER TABLE `worksheet`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
