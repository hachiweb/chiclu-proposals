<div class="modal-content">
    <div class="modal-header bg-info text-white">
        <h5 class="modal-title">Add Content</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body text-left">
        <ul class="add-new-sec-list">
            <li class="box-shadow mb-3">
                <a class="p-3 bg-gray d-block plus-cat-grup" href="javascript:void(0);">
                    <i class="fa fa-th-list" aria-hidden="true"></i>
                    <span>Category Group</span>
                </a>
            </li>
            <li class="box-shadow mb-3">
                <a class="p-3 bg-gray d-block plus-colorPalette-grup" href="javascript:void(0);">
                    <i class="fa fa-paint-brush" aria-hidden="true"></i>
                    <span>Color Palette</span>
                </a>
            </li>
            <li class="box-shadow mb-3">
                <a class="p-3 bg-gray d-block plus-discounts-grup" href="javascript:void(0);">
                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                    <span>Discounts/Taxes</span>
                </a>
            </li>
        </ul>
    </div>
</div>