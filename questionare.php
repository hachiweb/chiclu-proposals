<?php
$page ="Questionnaire";
include('header.php');
?> <!--header added-->
    <section class="container">
        <div class="row mt-4">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <h2 class="text-center p-3 text-white text-capitalize" style="background-color:rgb(23,162,184)">
                   Questionnaire Form
               </h2>
               <form method="POST" action="sub-questionare.php">

                <div class="questi-form-bx p-4">
                    <h4 class="font-weight-normal mb-1">Event Details:-</h4>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Bride Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="bride_name">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Groom Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="groom_name">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Referred By<span class="text-danger">*</span> </label>
                                <input type="text" class="form-control" name="referred_by">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Event Date<span class="text-danger">*</span></label>
                                <div class="time-icon"><input type="date" class="form-control" name="event_date"></div>
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Number of Guests<span class="text-danger">*</span> </label>
                                <input type="number" class="form-control" name="number_guests">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact Email<span class="text-danger">*</span> </label>
                                <input type="email" class="form-control" name="contact_email">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact Phone number 1<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="contact_number_first">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact Phone number 2<span class="text-danger">*</span> </label>
                                <input type="text" class="form-control" name="contact_number_second">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        
                        <?php /* ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Address: </label>
                                <input type="text" class="form-control" name="address">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>City: </label>
                                <input type="text" class="form-control" name="city">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>State: </label>
                                <input type="text" class="form-control" name="state">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>ZIP: </label>
                                <input type="number" class="form-control" name="zip">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <?php */ ?>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Event Notes</label>
                                <textarea class="form-control" name="event_notes_description" id="vent_notes_description" rows="5"></textarea>
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Event Budget $<span class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="event_budget">
                            </div><!-- form-group -->
                        </div><!-- col -->
                    </div><!-- row -->

                    <hr>

                    <h4 class="font-weight-normal mb-1">Event Schedule:-</h4>
                    <h5 class="font-weight-normal mb-1">Times & Location (Venue)</h5>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Ceremony Start Time<span class="text-danger">*</span></label>
                                <div class="time-icon"><input type="time" class="form-control" name="ceremony_start_time"></div>
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> Ceremony Location<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="ceremony_location">
                            </div><!-- form-group -->
                        </div><!-- col -->
                        <div class="col-md-6">
                         <div class="form-group">
                            <label>Cocktails Start Time<span class="text-danger">*</span></label>
                            <div class="time-icon"><input type="time" class="form-control" name="ocktails_start_time"></div>
                        </div><!-- form-group -->
                    </div><!-- col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> Cocktails Location<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="cocktails_location">
                        </div><!-- form-group -->
                    </div><!-- col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Reception Start Time<span class="text-danger">*</span></label>
                            <div class="time-icon"><input type="time" class="form-control" name="reception_start_time"></div>
                        </div><!-- form-group -->
                    </div><!-- col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Reception Location<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="reception_location">
                        </div><!-- form-group -->
                    </div><!-- col -->
                </div><!-- row -->

                <hr>

                <h4 class="font-weight-normal mb-1">Important Times:-</h4>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Setup Available<span class="text-danger">*</span></label>
                            <div class="time-icon"><input type="time" class="form-control" name="setup_available"></div>
                        </div><!-- form-group -->
                    </div><!-- col -->

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Event End<span class="text-danger">*</span></label>
                            <div class="time-icon"><input type="time" class="form-control" name="event_end"></div>
                        </div><!-- form-group -->
                    </div><!-- col -->
                </div><!-- row -->

                <hr>

                <h4 class="font-weight-normal mb-1">Delivery Info if different from Ceremony Venue</h4>
                <h5 class="font-weight-normal mb-1">Address</h5>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Bouquet Delivery time<span class="text-danger">*</span></label>
                            <div class="time-icon"><input type="time" class="form-control" name="bouquet_delivery_time"></div>
                        </div><!-- form-group -->
                    </div><!-- col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Bouquet Delivery Location<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="bouquet_delivery_location">
                        </div><!-- form-group -->
                    </div><!-- col -->
                </div><!-- row -->

                <div class="form-group">
                    <label>Bridal bouquet desire type (description)</label>
                    <textarea class="form-control" name="bridal_bouquet_desire_type" id="bridal_bouquet_desire_type" rows="5"></textarea>
                </div><!-- form-group -->

                <div class="form-group">
                    <label>Bridesmaids bouquets desire type (description)</label>
                    <textarea class="form-control" name="bridalmaids_bouquet_desire_type" id="bridalmaids_bouquet_desire_type" rows="5"></textarea>
                </div><!-- form-group -->
                <div class="form-group">
                    <label>How many Bridesmaids bouquets :<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="bridals_maids_bouquets_number">
                </div><!-- form-group -->
                <div class="form-group">
                    <label>Groomsmen boutonnières desire type (description) :</label>
                    <textarea class="form-control" name="groomsmen_boutnaries_desire_type" id="groomsmen_boutnaries_desire_type" rows="5"></textarea>
                </div><!-- form-group -->
                <div class="form-group">
                    <label>How many Groomsmen boutonnières :<span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="groomsmen_boutnaries">
                </div><!-- form-group -->
                <div class="form-group">
                    <label>How Many tables?<span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="number_tables">
                </div><!-- form-group -->
                <div class="form-group">
                    <label>Tables type(round/rectangular)<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="type_tables">
                </div><!-- form-group -->

                <div class="form-group">
                    <label>
                        Would you like an arch with ﬂorals and greenery?<br>desire type (description)
                    </label>
                    <textarea class="form-control" name="arch_florals_greenery" id="arch_florals_greenery" rows="5"></textarea>
                </div><!-- form-group -->
                <div class="form-group">
                    <label>Arch type<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="arch_type">
                    <span>Will venue provide arch? or you want it rented from us?</span>
                </div><!-- form-group -->
                <div class="form-group text-right">
                    <input type="submit" class="btn btn-info btn-lg " id="submit_button" name="submit" value="SEND">
                </div><!-- form-group -->
            </div><!-- questi-form-bx -->
        </form>
    </div>
</section>
</body>
</html>
