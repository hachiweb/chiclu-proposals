<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}  
if(empty($_SESSION["username"])){
    if ($page == "Questionnaire") {

    } 
    else {
        header("location:users/login.php"); 
        exit();
    }

}
else{
    $username = $_SESSION["username"];
    if($page != "Home" && !isset($_GET["hide_header"])){
        $id = $_SESSION["ID"];
    }
}
include('db.php'); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Proposals ChicLu</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/jquery.auto-complete.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <?php if ($page != "Worksheet") { ?>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <?php } ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> <!--datepicker-->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> <!--datepicker-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.4.0/clipboard.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



    <link href="css/jquery.colorpicker.bygiro.css" rel="stylesheet">
    <style>
      #sortable1, #sortable2 {

       
       
         
          
          
        
      }
      #sortable1 li, #sortable2 li {
         cursor:pointer;
     }
 </style>

</head>

<body class="bg-gray">
    <div class="wrapper">

        <div id="mySidenav" class="sidenav">
            <div class="sidenav-inn">
                <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> -->
                <div class="text-center p-3">
                    <a href="index.php">
                        <img class="img-responsive" src="img/logo.png" alt="Logo" >
                    </a>
                </div>
                <p class="text-center">Proposals Chiclu</p>
                <div id="accordion">
                    <div class="card borderless1">
                        <div class="card-header back_color_set borderless1">
                            <a class="card-link" data-toggle="collapse" href="">
                                <a href="index.php">
                                    <i class="fa fa-calendar fa-fw"></i> 
                                    Event List
                                </a>
                            </a>
                        </div>
                    </div>
                    <div class="card borderless1">
                        <div class="card-header back_color_set borderless1">
                            <a class="card-link" data-toggle="collapse" href="">
                                <a href="questionare.php">
                                    <i class="fa fa-list fa-fw"></i> 
                                    Questionare
                                </a>
                            </a>
                        </div>
                    </div>
                <?php /* ?>
                <div class="card borderless1">
                    <div class="card-header back_color_set borderless1">
                        <a class="card-link" data-toggle="collapse" href="">
                            <a href="questionare.php">
                                <i class="fa fa-list fa-fw"></i> 
                                Questionare
                            </a>
                        </a>
                    </div>
                </div>
                <div class="card borderless1">
                    <div class="card-header back_color_set borderless1">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne">
                            <a href="worksheet.php">
                                <i class="fa fa-bar-chart fa-fw"></i> 
                                Worksheet
                            </a>

                        </a>
                    </div>
                </div>
                <?php */ ?>
                <div class="card borderless1">
                    <div class="card-header back_color_set borderless1">
                        <a class="collapsed card-link" data-toggle="collapse" href="#reports">
                            <i class="fa fa-database" aria-hidden="true"></i>
                            Reports
                        </a>
                    </div>
                    <div id="reports" class="collapse" data-parent="#accordion">
                        <div class="card-body">
                            <ul class="list_dot_remove">
                                <li>
                                    <a href="">
                                        <i class="fa fa-info fa-fw"></i> 
                                        Items Needed
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card borderless1">
                    <div class="card-header back_color_set borderless1">
                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                            <i class="fa fa-folder fa-fw"></i> 
                            Resources
                        </a>
                    </div>
                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                        <div class="card-body">
                            <ul class="list_dot_remove">
                                <li><a href="inspiration-gallery.php?hide_header=1"><i class="fa fa-info fa-fw"></i> Inspiration Gallery</a></li>
                                <li><a href="recipe-gallery.php?hide_header=1"><i class="fa fa-sliders fa-fw"></i> Recipe Gallery</a></li>
                                <li><a href="item-gallery.php?hide_header=1"><i class="fa fa-asterisk fa-fw"></i> Item Gallery</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- sidenav-inn -->
    </div>

    <header class="main-header bg-white">
        <nav class="navbar navbar-expand-sm top-bar-nav">
            <ul class="navbar-nav flex-wrap align-items-baseline">
                <li>
                    <div class="h-main-left">
                        <!-- <span style="font-size:26px;cursor:pointer" onclick="openNav()">&#9776;</span> -->
                        <!-- <span class="click-sidebar-menu" onclick="openNav()"> -->
                            <span class="click-sidebar-menu">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        </div>
                    </li>

                    <?php if ($page == "Home") { ?>
                        <li class="h3 m-0"><a href="index.php">Home</a></li>
                        <li><a href="index.php">Event List</a></li>
                    <?php } ?>
                    <?php if ($page != "Home" && $page != "Questionnaire" && !isset($_GET["hide_header"])) { ?>
                        <li class="h3 m-0"><a href="index.php">Event</a></li>
                        <li><a href="detail_form.php">Event Details</a>
                        </li>
                        <!-- <li><a href="detail_form.php">Details</a></li> -->
                        <li><a href="worksheet.php">Worksheet</a></li>
                        <li class="dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                Resources
                            </a>
                            <div class="dropdown-menu">
                                <a href="inspiration-gallery.php">Inspiration Gallery</a>
                                <a href="recipe-gallery.php">Recipe Gallery</a>
                                <a href="item-gallery.php">Item Gallery</a>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                Financials
                            </a>
                            <div class="dropdown-menu">
                                <a href="payment.php"><i class="fa fa-money fa-fw text-dark"></i> Payment</a>
                            </div>
                        </li>
                        <li><a href="proposal.php">Proposal</a></li>
                        <li><a href="document.php">Documents</a></li>
                        <li><a href="cost.php">Costs</a></li>
                    <?php } if (isset($_GET["hide_header"])) {
                        ?>
                        <li class="dropdown ml-3">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                Resources
                            </a>
                            <div class="dropdown-menu">
                                <a href="inspiration-gallery.php?hide_header=1">Inspiration Gallery</a>
                                <a href="recipe-gallery.php?hide_header=1">Recipe Gallery</a>
                                <a href="item-gallery.php?hide_header=1">Item Gallery</a>
                            </div>
                        </li>
                        <?php
                    } ?>

                </ul>
                <?php
                if($page != "Questionnaire"){
                 ?>
                 <ul class="navbar-nav flex-wrap ml-auto">
                  <li> <?php  if(isset($_SESSION["groom_bride"])  ){echo $_SESSION["groom_bride"];} ?> </li>
                  <div class="dropdown">
                    <span  class="dropdown-toggle" data-toggle="dropdown" style="cursor:pointer;">
                        <i class="fa fa-user fa-fw user_setup"></i>
                    </span>
                    <div class="dropdown-menu">
                        <ul class="back_color_set">
                            <li><a class="dropdown-item" href="#"><i class="fa fa-address-card fa-fw"></i> My Account</a></li>
                            <li><a href="users/logout.php" class="dropdown-item"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>

                        </ul>
                    </div>
                </div>

            </ul>
            <?php
        }
        ?>
    </nav>
</header>