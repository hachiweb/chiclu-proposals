<?php
$page ="NULL";
include('header.php');
?>
<section class="container-fluid">
    <form action="sub-edit-client.php" method="POST">
        <div class="row mt-3">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="bg_color_set p-3">
                    <span class="h4 font-weight-normal text-white my-3">Required Payment</span>
                    <span class="text-white h4 float-right">
                        Total: <span class="h5">$123,456,789.00</span>
                    </span>

                    <span class="text-white h4 float-right">
                        Outstanding: <span class="h5">$123,456,789.00</span>&nbsp;&nbsp;|&nbsp;&nbsp;
                    </span>

                    <span class="text-white h4 float-right">
                        Paid: <span class="h5">$0.00</span>&nbsp;&nbsp;|&nbsp;&nbsp;
                    </span>

                </div>
                <div class="bg-white" style="">
                    <div class="pt-4">
                        <div class="border-bottom mx-3">
                            <span>Required Payments</span>
                            <div class="text-right clicks">
                                <span class="ingredients_eye"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                <span class="ingredients_show">Show</span>
                                <span class="ingredients_delete">Deleted</span>
                            </div>
                        </div>
                        <div class="dropdown-bx">

                            <table class="table table-striped table-list m-0 text-center">
                                <thead>
                                    <tr>
                                        <th>Payment Name</th>
                                        <th></th>
                                        <th></th>
                                        <th>Due</th>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th></th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="body-clone-payment">
                                    <tr>
                                        <td><button type="button" class="item-name-bx payment_border_set">
                                                <span class="item-name-txt">Second Payment</span>
                                                <input type="text" name="payment_name" id="" class="item-name-input"
                                                    oninput="set_val(this)">
                                            </button>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td class=""><button type="button" class="item-name-bx payment_border_set">
                                                <span class="item-name-txt">Now</span>
                                                <input type="text" name="due" id="" class="item-name-input"
                                                    oninput="set_val(this)">
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="item-name-bx">
                                                <span class="item-name-txt"></span>
                                                <input type="date" name="date" id="" class="form-control corner_set">
                                            </button>
                                        </td>
                                        <td>
                                            <select name="type" id="" class="form-control corner_set">
                                                <option value="">$</option>
                                                <option value="">%</option>
                                            </select>
                                        </td>
                                        <td><button type="button" class="item-name-bx payment_border_set">
                                                <span class="item-name-txt">percentage</span>
                                                <input type="text" name="percentage" id="" class="item-name-input"
                                                    oninput="set_val(this)">
                                            </button>
                                        </td>
                                        <td>$0.00</td>
                                        <td><span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            <div>
                                <div class="text-center mt-4 pb-5">
                                    <h6>The amounts are more than grand total.</h6>
                                    <button type="button"
                                        class="btn btn-info corner_set border-white text-white add-row-set">Add Required
                                        Payment</button>
                                </div>
                            </div>
                        </div><!-- dropdown-bx -->
                    </div>
                </div>
            </div>
        </div>


        <div class="row mt-3">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="bg_color_set p-3">
                    <span class="h4 font-weight-normal text-white my-3">Received Payment</span>
                </div>
                <div class="bg-white" style="">
                    <div class="pt-4">
                        <div class="border-bottom mx-3">
                            <span>Received Payments</span>
                            <div class="text-right clicks">
                                <span class="ingredients_eye"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                <span class="ingredients_show">Show</span>
                                <span class="ingredients_delete">Deleted</span>
                            </div>
                        </div>
                        <div class="dropdown-bx">

                            <table class="table table-striped table-list m-0 text-center">
                                <thead>
                                    <tr>
                                        <th colspan="5">Date</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th colspan="5">Description</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>Creadit Card</th>
                                        <th>Amount</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="body-clone-payment-next">
                                    <tr>
                                        <td colspan="5">
                                            <button type="button" class="item-name-bx">
                                                <span class="item-name-txt"></span>
                                                <input type="date" name="item_name[]" id="" class="form-control corner_set ">
                                            </button>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="5">
                                            <textarea name="received_payments_description" name="item_name[]" id=""
                                                class="item-name-input w-100" cols="" rows="1"
                                                style="height:35px;">
                                            </textarea>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="form-check text-center">
                                                <input class="form-check-input mt-0"
                                                    name="received_payments_creadit_card" type="checkbox">
                                            </div>
                                        </td>
                                        <td>
                                            <button type="button" class="item-name-bx payment_border_set ">
                                                <span class="item-name-txt ">$0.00</span>
                                                <input type="text" name="received_payments_amount" id=""
                                                    class="item-name-input" oninput="set_val(this)">
                                            </button>
                                        </td>
                                        <td>
                                            <span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div>
                                <div class="text-center mt-4" style="padding-bottom:60px;margin-bottom:60px;">
                                    <button type="button" class="btn btn-info corner_set border-white text-white add-row-set">Record New Payment</button>
                                </div>
                            </div>
                        </div><!-- dropdown-bx -->
                    </div>
                </div>
            </div>
        </div>

        <div class="h-20 mt-3 pb-5 " style="border-top:2px solid rgb(206,212,218);">
            <div class="text-center mt-3">
                <button type="button" class="btn btn-info corner_set border-white text-white add-row-set"><i
                        class="fa fa-save fa-fw"></i> Save</button>
            </div>
        </div>
        </div>
    </form>
</section>
</body>

</html>