<?php
$page ="NULL";
include('header.php');
?>
<section class="container-fluid">
    <form action="sub-edit-client.php" method="POST">
        <div class="row row-5 mt-3">
            <div class="col-md-6">
                <div class="bg_color_set px-2 py-3">
                    <h4 class="font-weight-normal text-white m-0">Proposals</h4>
                </div>
                <div class="bg-white p-4 proposal-form-sec">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="proposal-form-dv">
                                <h6 class="heading">Settings</h6>
                                <div class="text-dv">
                                    <label class="mb-0" for="">Theme</label>
                                    <select name="theme" id="" class="form-control corner_set" onchange="set_file1(this)">
                                        <option value="" name="">--Select Any Value--</option>
                                        <option value="Blooming" name="blooming">blooming</option>
                                        <option value="Blossom" name="blossom">Blossom</option>
                                        <option value="Vanilla" name="vanilla">Vanilla</option>
                                    </select>

                                    <div class="submit mt-3">
                                        <button class="btn btn-info" type="submit">Save & Publish</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- col -->

                        <div class="col-lg-6">
                            <div class="proposal-form-dv">
                                <div class="proposal-form-dv">
                                    <h6 class="heading">PDF History</h6>
                                    <div class="text-dv">
                                        <div id="proposal-version-table-container">
                                            <table class="w-100 small">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input checked="checked" name="proposalVersion" type="radio" value="oGL2gdxa">
                                                        </td>
                                                        <td>
                                                            <i aria-hidden="true" class="fa fa-eye" title="This is the current visible version."></i>
                                                        </td>
                                                        <td>Sep 21, 2019 12:50 PM</td>
                                                        <td>
                                                            <i aria-hidden="true" class="fa fa-trash fa-fw" version-id="oGL2gdxa"></i>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="submit mt-3">
                                            <button class="btn btn-info" type="submit">Change Proposal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- col -->
                    </div><!-- row -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="bg_color_set px-2 py-3">
                    <h4 class="font-weight-normal text-white m-0">Proposal Access</h4>
                </div>
                <div class="bg-white p-4 proposal-form-sec">
                    <div class="proposal-form-dv">
                        <h6 class="heading">Proposal Access</h6>

                        <div class="text-dv">
                            <label class="mb-0" for="">Expiration Date</label>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="d-flex flex-wrap">
                                        <input type="text" class="mr-3" name="" id="">
                                        <button class="btn btn-info" type="submit">Save</button>                                        
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="mt-4">
                        <div class="heading font-weight-normal"></div>
                        <p class="mt-3 text-dark">There must be a published version.</p>
                    </div>
                </div>
            </div>
        </div>
        <section class="mt-3">
            <div class="row">
                <div class="col-md-3">
                    <div class="bg_color_set p-2">
                        <h4 class="font-weight-normal text-white my-3" id="put_value">Proposal Editor-</h4>
                    </div>
                    <div class="bg-white">
                        <div class="p-3">
                            <div id="show_form_one">
                                <div class="form-group">
                                    <label for="" class="mb-0">Editable Content</label>
                                    <select name="editable_content" id="" class="form-control corner_set">
                                        <option value="Page Details" name="page_detail">Page Details</option>
                                        <option value="Focal Images" name="focal_images">Focal Images</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Page Visibility</label>
                                    <select name="page_visibility" id="" class="form-control corner_set">
                                        <option value="" name="visible">Visible</option>
                                        <option value="" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mb-0">Featured Name 1</label>
                                    <input type="text" class="form-control corner_set" name="featured_name_1" id="featured_name_1">
                                    <label for="" class="mt-3 mb-0">Featured Name 2</label>
                                    <input type="text" class="form-control corner_set" value="" name="featured_name_2" id="featured_name_2">
                                    <label for="" class="mt-3 mb-0">Featured Title</label>
                                    <input type="text" class="form-control corner_set" value="" name="featured_title">
                                    <label for="" class="mt-3 mb-0">Line 1 Text</label>
                                    <input type="text" class="form-control corner_set" value="Proposal For"
                                    name="line_1_text">
                                    <label for="" class="mt-3 mb-0">Client Name</label>
                                    <input type="text" class="form-control corner_set"
                                    value="Whitney Cardenas Ivory Douglas" name="client_name">
                                    <label for="" class="mt-3 mb-0">Show Bottom Contact Info</label>
                                    <select name="show_bottom_contact_info" id="" class="form-control corner_set">
                                        <option value="" name="visible">Visible</option>
                                        <option value="" name="hidden">Hidden</option>
                                    </select>
                                </div>
                            </div>
                            <!--cover form--->


                            <div style="display:none;" id="show_form_two">
                                <div class="form-group">
                                    <label for="" class="mt-3 mb-0">Editable Content</label>
                                    <select name="concept_editable_content" id="" class="form-control corner_set">
                                        <option value="Page Details" name="page_details">Page Details</option>
                                        <option value="Event Inspiration Images" name="event_inspiration_images">Event
                                        Inspiration Images</option>
                                        <option value="Floral Inspiration Images" name="floral_inspiration_images">
                                        Floral Inspiration Images</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Page Visibility</label>
                                    <select name="concept_page_visibility" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Page Heading</label>
                                    <input type="text" class="form-control corner_set" value="Conceptual Design"
                                    name="concept_page_heading">
                                    <label for="" class="mt-3 mb-0">Description Title</label>
                                    <input type="text" class="form-control corner_set" value="Overall Feeling:"
                                    name="concept_description_title">
                                    <label for="" class="mt-3 mb-0">Concept Description</label>
                                    <textarea name="concept_description" id="" cols="" rows="6"
                                    class="form-control corner_set"></textarea>
                                </div>
                            </div>
                            <!--concept form--->


                            <div style="display:none;" id="show_form_three">
                                <div class="form-group">
                                    <label for="" class="mt-3 mb-0">Page Visibility</label>
                                    <select name="design_page_visibility" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Page Heading</label>
                                    <input type="text" class="form-control corner_set" value="Event Design Agreement"
                                    name="design_page_heading">
                                    <label for="" class="mt-3 mb-0">Show Vendor Teams</label>
                                    <select name="design_show_vendor_teams" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Vendor Teams Heading</label>
                                    <input type="text" class="form-control corner_set" value="Vendor Teams"
                                    name="design_vendor_teams_heading">
                                </div>
                            </div>
                            <!--design agreement form--->



                            <div style="display:none;" id="show_form_four">
                                <div class="form-group">
                                    <label for="" class="mt-3 mb-0">Page Visibility</label>
                                    <select name="item_spage_visibility" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>

                                    <p class="mt-4 mb-0">Blooms & Stylings</p>
                                    <div class="border-bottom"></div>
                                    <label for="" class="mt-3 mb-0">Show Category</label>
                                    <select name="item_show_category_1" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Category Name</label>
                                    <input type="text" class="form-control corner_set" value=""
                                    name="blooms_stylings_category_name">

                                    <p class="mt-4 mb-0">Chairs</p>
                                    <div class="border-bottom"></div>
                                    <label for="" class="mt-3 mb-0">Show Category</label>
                                    <select name="item_show_category_2" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Category Name</label>
                                    <input type="text" class="form-control corner_set" value="Chairs"
                                    name="chairs_category_name">

                                    <p class="mt-4 mb-0">Chargers</p>
                                    <div class="border-bottom"></div>
                                    <label for="" class="mt-3 mb-0">Show Category</label>
                                    <select name="item_show_category_3" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Category Name</label>
                                    <input type="text" class="form-control corner_set" value="Chairs"
                                    name="chargers_category_name">

                                    <p class="mt-4 mb-0">Furniture</p>
                                    <div class="border-bottom"></div>
                                    <label for="" class="mt-3 mb-0">Show Category</label>
                                    <select name="item_show_category_4" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Category Name</label>
                                    <input type="text" class="form-control corner_set" value="Chairs"
                                    name="furniture_category_name">

                                    <p class="mt-4 mb-0">Linens</p>
                                    <div class="border-bottom"></div>
                                    <label for="" class="mt-3 mb-0">Show Category</label>
                                    <select name="item_show_category_5" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Category Name</label>
                                    <input type="text" class="form-control corner_set" value="Chairs"
                                    name="linens_category_name">

                                    <p class="mt-4 mb-0">Miscellaneous</p>
                                    <div class="border-bottom"></div>
                                    <label for="" class="mt-3 mb-0">Show Category</label>
                                    <select name="item_show_category_6" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Category Name</label>
                                    <input type="text" class="form-control corner_set" value="Chairs"
                                    name="miscellaneous_category_name">

                                    <p class="mt-4 mb-0">Table Numbers</p>
                                    <div class="border-bottom"></div>
                                    <label for="" class="mt-3 mb-0">Show Category</label>
                                    <select name="item_show_category_7" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Category Name</label>
                                    <input type="text" class="form-control corner_set" value="Chairs"
                                    name="table_number_category_name">

                                    <p class="mt-4 mb-0">Vases & Rentals</p>
                                    <div class="border-bottom"></div>
                                    <label for="" class="mt-3 mb-0">Show Category</label>
                                    <select name="item_show_category_8" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>
                                    <label for="" class="mt-3 mb-0">Category Name</label>
                                    <input type="text" class="form-control corner_set" value="Chairs"
                                    name="vases_rentals_category_name">
                                </div>
                            </div>
                            <!--items form--->


                            <div style="display:none;" id="show_form_five">
                                <div class="form-group">
                                    <label for="" class="mt-3 mb-0">Page Visibility</label>
                                    <select name="line_item_spage_visibility" id="" class="form-control corner_set">
                                        <option value="visible" name="visible">Visible</option>
                                        <option value="hidden" name="hidden">Hidden</option>
                                    </select>

                                    <label for="" class="mt-3 mb-0">Show Line Item Prices</label>
                                    <select name="list_item_show_line_item_prices" id=""
                                    class="form-control corner_set">
                                    <option value="visible" name="visible">Visible</option>
                                    <option value="hidden" name="hidden">Hidden</option>
                                </select>

                                <label for="" class="mt-3 mb-0">Show Recipe Image</label>
                                <select name="list_item_show_recipe_image" id="" class="form-control corner_set">
                                    <option value="visible" name="visible">Visible</option>
                                    <option value="hidden" name="hidden">Hidden</option>
                                </select>

                                <label for="" class="mt-3 mb-0">Show Recipe Name</label>
                                <select name="list_item_show_recipe_name" id="" class="form-control corner_set">
                                    <option value="visible" name="visible">Visible</option>
                                    <option value="hidden" name="hidden">Hidden</option>
                                </select>

                                <label for="" class="mt-3 mb-0">Show Ingredient Images</label>
                                <select name="list_item_show_ingredient_images" id=""
                                class="form-control corner_set">
                                <option value="visible" name="visible">Visible</option>
                                <option value="hidden" name="hidden">Hidden</option>
                            </select>

                            <label for="" class="mt-3 mb-0">Show Recipe Description</label>
                            <select name="list_item_show_recipe_description" id=""
                            class="form-control corner_set">
                            <option value="visible" name="visible">Visible</option>
                            <option value="hidden" name="hidden">Hidden</option>
                        </select>

                        <label for="" class="mt-3 mb-0">Show Recipe Note</label>
                        <select name="list_item_show_recipe_note" id="" class="form-control corner_set">
                            <option value="visible" name="visible">Visible</option>
                            <option value="hidden" name="hidden">Hidden</option>
                        </select>

                        <label for="" class="mt-3 mb-0">Show Category</label>
                        <select name="list_item_show_category_subtotal" id=""
                        class="form-control corner_set">
                        <option value="visible" name="visible">Visible</option>
                        <option value="hidden" name="hidden">Hidden</option>
                    </select>
                </div>
            </div>
            <!--line_items form--->

            <div style="display:none;" id="show_form_six">
                <div class="form-group">
                    <label for="" class="mt-3 mb-0">Page Visibility</label>
                    <select name="term_spage_visibility" id="" class="form-control corner_set">
                        <option value="visible" name="visible">Visible</option>
                        <option value="hidden" name="hidden">Hidden</option>
                    </select>
                    <label for="" class="mt-3 mb-0">Page Heading</label>
                    <input type="text" class="form-control corner_set"
                    value="Payment Terms & Instructions" name="term_page_heading">
                </div>
            </div>
            <!--terms form--->

            <div style="display:none;" id="show_form_seven">
                <div class="form-group">
                    <label for="" class="mt-3 mb-0">Page Visibility</label>
                    <select name="breakdown_page_visibility" id="" class="form-control corner_set">
                        <option value="visible" name="visible">Visible</option>
                        <option value="hidden" name="hidden">Hidden</option>
                    </select>
                    <label for="" class="mt-3 mb-0">Page Heading</label>
                    <input type="text" class="form-control corner_set" value="Event Total Breakdown"
                    name="breakdown_page_heading">
                    <label for="" class="mt-3 mb-0">Hide Discount Column (if no discount)</label>
                    <select name="breakdown_hide_discount_column" id="" class="form-control corner_set">
                        <option value="visible" name="visible">Visible</option>
                        <option value="hidden" name="hidden">Hidden</option>
                    </select>
                </div>
            </div>
            <!--terms form--->
            <div style="display:none;" id="show_form_eight">
                <div class="form-group">
                    <label for="" class="mt-3 mb-0">Page Visibility</label>
                    <select name="summary_page_visibility" id="" class="form-control corner_set">
                        <option value="visible" name="visible">Visible</option>
                        <option value="hidden" name="hidden">Hidden</option>
                    </select>
                    <label for="" class="mt-3 mb-0">Page Heading</label>
                    <input type="text" class="form-control corner_set" value="Summary"
                    name="summary_page_heading">
                    <label for="" class="mt-3 mb-0">Show Invoice Number</label>
                    <select name="summary_show_invoice_number" id="" class="form-control corner_set">
                        <option value="visible" name="visible">Visible</option>
                        <option value="hidden" name="hidden">Hidden</option>
                    </select>
                    <label for="" class="mt-3 mb-0">Show Company Signature Line</label>
                    <select name="summary_show_company_signature_line" id=""
                    class="form-control corner_set">
                    <option value="visible" name="visible">Visible</option>
                    <option value="hidden" name="hidden">Hidden</option>
                </select>
                <label for="" class="mt-3 mb-0">Company Rep Signature</label>
                <input type="text" class="form-control corner_set" value=""
                name="summary_company_rep_signature">
                <label for="" class="mt-3 mb-0">Signature Date</label>
                <input type="date" class="form-control corner_set" value=""
                name="summary_signature_date">
            </div>
        </div>
        <!--summary form--->
    </div>
</div>
</div>
<div class="col-md-9">
    <div class="bg_color_set p-4">
        <h4 class="font-weight-normal text-white my-3 d-inline">Preview</h4>
        <div class="float-right form-inline mx-3">
            <label for="" class="font-weight-normal text-white h5 mr-2">Current Page:</label>
            <select name="current_page" id="" class="form-control corner_set"
            onchange="set_file_one(this)">
            <option value="Cover" name="cover">Cover</option>
            <option value="concept" name="concept">Concept</option>
            <option value="Design Agreement" name="design_agreement">Design Agreement</option>
            <option value="Items" name="items">Items</option>
            <option value="Line Items" name="line_items">Line Items</option>
            <option value="Terms" name="terms">Terms</option>
            <option value="Breakdown" name="breakdown">Breakdown</option>
            <option value="Summary" name="summary">Summary</option>
        </select>
    </div>

</div>

<div class="bg-white p-3">
    <div class="space_set p-4">

        <?php include('proposal/cover-template-proposal.php'); ?>
        <?php include('proposal/concept-template-proposal.php'); ?>
        <?php include('proposal/design-agreement-template-proposal.php'); ?>
        <?php include('proposal/items-template-proposal.php'); ?>
        <?php include('proposal/line-items-template-proposal.php'); ?>
        <?php include('proposal/terms-template-proposal.php'); ?>
        <?php include('proposal/breakdown-template-proposal.php'); ?>

    </div><!-- space-set -->
</div>

</div>
</div>
</section>

<div class="row border-top mt-3 pb-5 m-0">
    <div class="col-md-12 mt-2" >
        <div class="text-center">
            <button class="btn btn-info border-white text-white corner_set" type="button"> Save </button>
            <button type="reset" class="btn btn-info border-white text-white corner_set" type="button"> Reset </button>
        </div>
    </div>
</div>
</form>
</section>

<script src="js/proposal.js"></script>

<script>
    function set_file1(element) {
        if (element.value == "Blooming") {
            $("#put_value").html("Proposal Editor-Blooming");
        } else if (element.value == "Blossom") {
            $("#put_value").html("Proposal Editor-Blossom");
        } else if (element.value == "Vanilla") {
            $("#put_value").html("Proposal Editor-Vanilla");
        } else {
            $("#put_value").html("Proposal Editor-");
        }

    }

    // function set_file_one(element) {
    //     if (element.value == "Cover") {
    //         $("#show_form_one").show();
    //         $("#show_form_two").hide();
    //         $("#show_form_three").hide();
    //         $("#show_form_four").hide();
    //         $("#show_form_five").hide();
    //         $("#show_form_six").hide();
    //         $("#show_form_seven").hide();
    //         $("#show_form_eight").hide();
    //     } else if (element.value == "concept") {
    //         $("#show_form_one").hide();
    //         $("#show_form_two").show();
    //         $("#show_form_three").hide();
    //         $("#show_form_four").hide();
    //         $("#show_form_five").hide();
    //         $("#show_form_six").hide();
    //         $("#show_form_seven").hide();
    //         $("#show_form_eight").hide();
    //     } else if (element.value == "Design Agreement") {
    //         $("#show_form_one").hide();
    //         $("#show_form_two").hide();
    //         $("#show_form_three").show();
    //         $("#show_form_four").hide();
    //         $("#show_form_five").hide();
    //         $("#show_form_six").hide();
    //         $("#show_form_seven").hide();
    //         $("#show_form_eight").hide();
    //     } else if (element.value == "Items") {
    //         $("#show_form_one").hide();
    //         $("#show_form_two").hide();
    //         $("#show_form_three").hide();
    //         $("#show_form_four").show();
    //         $("#show_form_five").hide();
    //         $("#show_form_six").hide();
    //         $("#show_form_seven").hide();
    //         $("#show_form_eight").hide();
    //     } else if (element.value == "Line Items") {
    //         $("#show_form_one").hide();
    //         $("#show_form_two").hide();
    //         $("#show_form_three").hide();
    //         $("#show_form_four").hide();
    //         $("#show_form_five").show();
    //         $("#show_form_six").hide();
    //         $("#show_form_seven").hide();
    //         $("#show_form_eight").hide();
    //     } else if (element.value == "Terms") {
    //         $("#show_form_one").hide();
    //         $("#show_form_two").hide();
    //         $("#show_form_three").hide();
    //         $("#show_form_four").hide();
    //         $("#show_form_five").hide();
    //         $("#show_form_six").show();
    //         $("#show_form_seven").hide();
    //         $("#show_form_eight").hide();
    //     } else if (element.value == "Breakdown") {
    //         $("#show_form_one").hide();
    //         $("#show_form_two").hide();
    //         $("#show_form_three").hide();
    //         $("#show_form_four").hide();
    //         $("#show_form_five").hide();
    //         $("#show_form_six").hide();
    //         $("#show_form_seven").show();
    //         $("#show_form_eight").hide();
    //     } else if (element.value == "Summary") {
    //         $("#show_form_one").hide();
    //         $("#show_form_two").hide();
    //         $("#show_form_three").hide();
    //         $("#show_form_four").hide();
    //         $("#show_form_five").hide();
    //         $("#show_form_six").hide();
    //         $("#show_form_seven").hide();
    //         $("#show_form_eight").show();
    //     } 

    //}
</script>
</body>

</html>