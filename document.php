<?php
$page ="NULL";
include('header.php');
?>
<section class="container-fluid">
    <form action="sub-edit-client.php" method="POST">
        <div class="row mt-3">
            <div class="col-md-3">
                <div class="bg_color_set p-3">
                    <h4 class="text-white pt-1">Documents</h4>
                </div>
                <div class="bg-white">
                    <div class="pt-3">
                        <div class="pill_padding">
                            <div class="">
                                <ul class="list-group list-group-flush">
                                   <span class="border-top"></span>
                                    <li class="nav-link list-group-item" id="text_show1"><i
                                            class="fa fa-fw fa-file "></i> Executive PDF</li>
                                    <li class="nav-link list-group-item" id="text_show2"><i
                                            class="fa fa-fw fa-utensil-spoon"></i> Recipe PDF</li>
                                    <li class="nav-link list-group-item" id="text_show3"><i
                                            class="fa fa-fw fa-truck"></i> Delivery PDF</li>
                                    <span class="border-top"></span>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="text-center pb-3 mt-3">
                       <a href="proposal.php"><button class="btn btn-info border-white text-white corner_set" type="button"> Open Proposal
                            Editor</button></a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="bg_color_set p-4">
                    <span class="h4 text-white d-inline" id="txt_show">Executive PDF</span>
                    <button class="btn btn-info border-white text-white corner_set float-right  print-button" type="button">
                        <i class="fa fa-print fa-fw"></i> Print</button>
                    <button class="btn btn-info border-white text-white corner_set float-right mr-3" type="button">
                        <i class="fa fa-download fa-fw"></i> Downloaded PDF</button>
                </div>
                <div class="bg-white p-3">
                    <div class="space_set">

                    </div>
                </div>
            </div>
        </div>


    </form>
</section>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
$(document).ready(function() {
    $("#text_show1").click(function() {
        $("#txt_show").html("Executive PDF");
    });
    $("#text_show2").click(function() {
        $("#txt_show").html("Recipe PDF");
    });
    $("#text_show3").click(function() {
        $("#txt_show").html("Delivery PDF");
    });
});

$('.print-button').on('click', function() {  
  window.print();  
  return false; // why false?
});  //---print script----
</script>


</body>

</html>