<?php
session_start();
$mod_id = $_SESSION["ID"];
include 'db.php';
$name            = $_FILES['file']['name'];
$id              = $_POST['id'];
$recipe_name     = $_POST['recipe_name'];
$description     = $_POST['description'];
$markup          = $_POST['markup'];
$price           = $_POST['price'];
$category        = $_POST['category'];
$tempstyles      = $_POST['styles'];
$styles          = implode(",", $tempstyles);
$source          = $_POST['source'];
$is_active       = !empty($_POST['is_active']) && $_POST['is_active'] =='Show'?1:0;
$in              = implode(",",$_POST['item_name']);
$qt              = implode(",",$_POST['qty']);
$target_dir      = "img/";
$target_file     = $target_dir . basename($_FILES["file"]["name"]);
// Select file type
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Valid file extensions
$extensions_arr = array("jpg", "jpeg", "png", "gif", "webp");
// Check extension
  if (isset($_POST['img_upload'])) {
    if (in_array($imageFileType, $extensions_arr)) {
    // Insert record
    $query = "UPDATE gallery SET name='".$name."', recipe_name='$recipe_name', description='$description', price='$price', category='$category', markup='$markup', styles='$styles', item_name='$in',qty='$qt', is_active='$is_active' WHERE id='$id'";
    mysqli_query($con, $query);
    $modisql = "UPDATE `event_questionare` SET  `last_updated`= current_timestamp() WHERE `event_id` = '$mod_id'";
    mysqli_query($con, $modisql);
    // Upload file
    move_uploaded_file($_FILES['file']['tmp_name'], $target_dir . $name);
    }
  }
  $query1 = "UPDATE gallery SET recipe_name='$recipe_name', description='$description', price='$price', category='$category', markup='$markup', styles='$styles', item_name='$in',qty='$qt', is_active='$is_active' WHERE id='$id'";
  mysqli_query($con, $query1);
  $modisql = "UPDATE `event_questionare` SET  `last_updated`= current_timestamp() WHERE `event_id` = '$mod_id'";
  mysqli_query($con, $modisql);

  if (isset($in, $qt)) {
    $query2 = "UPDATE `ingredients` SET recipe_name='$recipe_name', item_name='$in', qty='$qt' WHERE recipe_name='$recipe_name'";
    mysqli_query($con, $query2);
    $modisql = "UPDATE `event_questionare` SET  `last_updated`= current_timestamp() WHERE `event_id` = '$mod_id'";
    mysqli_query($con, $modisql);
  }

  if ($source=='Recipe') {
    if (isset($_POST["hide_header"])) {
      header("location: recipe-gallery.php?hide_header=1");
    } else {
      header("location: recipe-gallery.php");
    }
  }
  elseif ($source=='Item') {
    if (isset($_POST["hide_header"])) {
      header("location: item-gallery.php?hide_header=1");
    } else {
      header("location: item-gallery.php");
    }
  }
?>