/* custom js */
$(document).ready(function(){
	// sidebar menu
	$(".click-sidebar-menu").click(function(){
		$("#mySidenav").toggleClass('trsansformmenu');
	});
	
	$(document).on('click touch', function(event) {
		if ($(event.target).closest('.click-sidebar-menu,#mySidenav').length == 0) {
			if ($("#mySidenav").hasClass('sidenav')) {
				$("#mySidenav").removeClass('trsansformmenu');
			}
		}
	});

	$(window).scroll(function() {
		if ($(this).scrollTop() > 0) {
			//console.log($(this).scrollTop());
			$(".main-header").addClass('header-fixed');
			$("body>.wrapper").addClass('mt-65');
		}else{
			$(".main-header").removeClass('header-fixed');
			$("body>.wrapper").removeClass('mt-65');
		}
	});
	$(window).scroll(function() {
		if ($(this).scrollTop() > 65) {
			$(".worksheet-left-dv .title-dv").addClass('header-fixed');
			$(".worksheet-right-dv>.title-dv").addClass('header-fixed');
		}else{
			$(".worksheet-left-dv>.title-dv").removeClass('header-fixed');
			$(".worksheet-right-dv>.title-dv").removeClass('header-fixed');
		}
	});




	//payment page
	$('.add-row-set').on('click', function(){
	//$(this).parents('.dropdown-bx').find('.tr-clone').first().clone().appendTo('.body-clone-dv');
	$(this).parents('.dropdown-bx').find('.body-clone-payment').append(
		'<tr>'+
		'<td><button type="button" class="item-name-bx payment_border_set">'+
		'<span class="item-name-txt">Second Payment</span>'+
		'<input type="text" name="payment_name" id="" class="item-name-input" oninput="set_val(this)">'+
		'</button>'+
		'</td>'+
		'<td></td>'+
		'<td></td>'+
		'<td>'+'<button type="button" class="item-name-bx payment_border_set">'+
		'<span class="item-name-txt">Now</span>'+
		'<input type="text" name="due" id="" class="item-name-input" oninput="set_val(this)">'+
		'</button>'+
		'</td>'+
		'<td>'+
		'<button type="button" class="item-name-bx">'+
		'<span class="item-name-txt"></span>'+
		'<input type="date" name="date" id="" class="form-control corner_set">'+
		'</button>'+
		'</td>'+
		'<td>'+
		'<select name="type" id="" class="form-control corner_set">'+
		'<option value="">$</option>'+
		'<option value="">%</option>'+
		'</select>'+
		'</td>'+
		'<td>'+
		'<button type="button" class="item-name-bx payment_border_set">'+
		'<span class="item-name-txt">percentage</span>'+
		'<input type="text" name="percentage" id="" class="item-name-input" oninput="set_val(this)">'+
		'</button>'+
		'</td>'+
		'<td>$0.00</td>'+
		'<td>'+'<span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span>'+
		'</td>'+
		'</tr>'
		);
});

	$('.add-row-set').on('click', function(){
		//$(this).parents('.dropdown-bx').find('.tr-clone').first().clone().appendTo('.body-clone-dv');
		$(this).parents('.dropdown-bx').find('.body-clone-payment-next').append(
			'<tr>'+
			'<td colspan="5">'+
			'<button type="button" class="item-name-bx">'+
			'<span class="item-name-txt"></span>'+
			'<input type="date" name="received_payments_date" id="" class="form-control corner_set ">'+
			'</button>'+
			'</td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td colspan="5">'+
			'<textarea name=""  name="received_payments_description" id="" class="item-name-input w-100" cols="" rows="1" style="height:35px;"></textarea>'+                                      
			'</td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td>'+
			'<div class="form-check text-center">'+                  
			'<input class="form-check-input mt-0" name="received_payments_creadit_card" type="checkbox">'+                                           
			'</div>'+
			'</td>'+
			'<td  class="text-center"><button type="button" class="item-name-bx payment_border_set">'+
			'<span class="item-name-txt">$0.00</span>'+
			'<input type="text" name="received_payments_amount" id="" class="item-name-input" oninput="set_val(this)">'+
			'</button>'+
			'</td>'+                                 
			'<td>'+
			'<span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span>'+
			'</td>'+
			'</tr>' 
			);
	});


	$(document).on("click",".cross-td",function(){
		$(this).parents('tr').remove();
	});





	// $(document).on("focusin",".item-name-bx",function(e){
	// 	alert('hhhh');
	// 	$(this).find('.item-name-txt').addClass('d-none');
	// 	$(this).find('.item-name-input').addClass('d-block').focus();
	// });
	// $(document).on("focusout",".item-name-bx",function(e){
	// 	alert('fgfhg');
	// 	$(this).find('.item-name-txt').removeClass('d-none');
	// 	$(this).find('.item-name-input').removeClass('d-block');
	// });



	
	$(document).on("focusin, click",".rename-item, .item-name-bx",function(e){
		if($(this).attr("class")=='rename-item'){
			$(this).find('.item-name-txt').addClass('d-none');
			$(this).find('.item-name-input').addClass('d-block').focus();
		}
		else{
			$(this).find('.item-name-txt').addClass('d-none');
			$(this).find('.item-name-input').addClass('d-block').focus();
		}
	});
	$(document).on("blur",".rename-item, .item-name-bx", function(e){
		if ($(e.target).is(".item-name-bx") == false) {
			$(this).find('.item-name-txt').removeClass('d-none');
			$(this).find('.item-name-input').removeClass('d-block');
		}
	});
	// .focusout(function (){
	// 	$(this).find('.item-name-txt').removeClass('d-none');
	// 	$(this).find('.item-name-input').removeClass('d-block').focus();
	// });


	//worksheet page scripts

	$("#filter-btn").click(function(){
		$("#filter-dv").slideToggle('fast');
	});

	$("#table-image-btn").click(function(){
		$("#image-grid-bx").toggleClass('d-none');
		$("#table-list-bx").toggleClass('d-none');
	});

	
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});


	$(document).on("click",".editbtn",function(){
		curr = $(this);
		curr.parent().next().children(".before-edit").addClass('d-none');
		curr.parent().next().children(".after-edit").addClass('d-flex');
	});
	$(document).on("click",".custom-modal .close",function(){
		curr = $(this);
		curr.parent().next().children(".before-edit").removeClass('d-none');
		curr.parent().next().children(".after-edit").removeClass('d-flex');
	});
	$(".custom-modal").on("hidden.bs.modal", function() {
		$( ".close" ).trigger( "click" );
	});

	
	// $('.clone-sec').on('click', '.add_ingredient', function() {
	// 	$('.clone-sec').find('.clone-row').first().clone().appendTo('.clone-result');
	// });

	// $(document).on("click",".clone-result .cross",function(){
	// 	$(this).parents('.clone-row').remove();
	// });


	$(".add_ingredient").bind("click", function (e) {
		var div = $("<div />");
		div.html(GetDynamicTextBox(""));
		$(".clone-result").append(div);
	});
	$("body").on("click", ".clone-result .cross", function () {
		$(this).closest(".clone-row").remove();
	});

	function GetDynamicTextBox(value) {
		return '<div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 clone-row"><div class="mr-2"><input class="form-control ingredient_auto" type="text" value="' + value + '" name="item_name[]" id=""></div><div><span><input class="form-control" type="number" value="' + value + '" name="qty[]" id=""></span><span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span></div></div>';
	}


	$(".edit_ingredient_btn").bind("click", function (e) {
		var div = $("<div />");
		div.html(GetDynamicTextBox1(""));
		$(".clone-result").append(div);
	});
	$("body").on("click", ".clone-result .cross", function () {
		$(this).closest(".clone-row").remove();
	});

	function GetDynamicTextBox1(value) {
		return '<div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 clone-row"><div class="mr-2"><input class="form-control ingredient_auto" type="text" value="' + value + '" name="item_name[]" id=""></div><div><span><input class="form-control" type="number" value="' + value + '" name="qty[]" id=""></span><span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span></div></div>';
	}
	
	//All months checked
	$(".month-filter").prop('checked', true);

	////Upload recipe image
	// $('#recipe_img_btn').on('click',(function(e) {
    //     e.preventDefault();
    //     // var formData = new FormData(this);
	// 	var formData = new FormData();
	// 	formData.append('recipe_image', $('#inputGroupFile01')); 

    //     $.ajax({
    //         type:'POST',
    //         url:'../sub-recipe.php',
    //         data:formData,
    //         cache:false,
    //         contentType: false,
    //         processData: false,
    //         success:function(data){
    //             console.log("success");
    //             console.log(data);
    //         },
    //         error: function(data){
    //             console.log("error");
    //             console.log(data);
    //         }
    //     });
    // }));

    // $("#ImageBrowse").on("change", function() {
    //     $("#imageUploadForm").submit();
	// });
	
	/** 
		Preview recipe image
		*/
		
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#preview_recipe').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#inputGroupFile01").change(function(){
			readURL(this);
		});

  //end document

  // Ingredient auto complete
  $(document).on("click",".ingredient_auto", function(){
	  	//console.log(this);
	  	$(this).autoComplete({
	  		source: function(name,response) {
	  			$.ajax({
	  				type: 'POST',
	  				dataType: 'json',
	  				url: 'auto_suggestions.php/',
	  				data: {term:name},
	  				success: function(data) {
	  					response(data);
	  				}
	  			});
	  		}
	  	});
	  });

  //Render item cost
  $(document).on('input','input[name="default_stem_cost"],input[name="stems_per_bunch"]',function(){
  	if($('input[name="default_stem_cost"]').val() != '' && $('input[name="stems_per_bunch"]').val() != ''){
  		var cost = parseFloat($('input[name="default_stem_cost"]').val())*parseFloat($('input[name="stems_per_bunch"]').val());
  		$("#item_cost").html('$'+cost.toFixed(2));
  	} 
  });
  
});

//navigation script
/*
function openNav() {
	document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
}
*/
//navigation script

function set_check(element) {

	var parent = element.parentNode;
	var child = parent.firstElementChild;

	if (element.checked == true) {

		child.checked = false;
	} else {

		child.checked = true;
	}
	
}
function set_val(element) {

	var parent = element.parentNode;
	var child = parent.firstElementChild;

	child.innerHTML = element.value;
	
}

function save_img(element,_source,_imgID) {
	var parent = element.parentNode;
	var child = parent.childNodes;
	//console.log(child);


	var img = child[5];
	//console.log(img);
	//alert(img.src);

	$.ajax({
		url: "sub_gallery_img.php",
		data : {image : img.src, action: "save",source : _source,imgID: _imgID},
		method: "POST"
	}).done(()=>{
		child[1].style.display = "block";
		element.style.display = "none";
		window.location.reload();
	});

	return true;
}
function del_img(element,_source) {
	var parent = element.parentNode;
	var child = parent.childNodes;
//	console.log(child);


var img = child[5];
	//console.log(img);
	//alert(img.src);

	$.ajax({
		url: "sub_gallery_img.php",
		data : {image : img.src, action: "delete",source : _source},
		method: "POST"
	}).done(()=>{
		child[3].style.display = "block";
		element.style.display = "none";
		window.location.reload();
	});

	return true;
}


