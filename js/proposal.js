/* proposal js */
$(document).ready(function(){

	$('#featured_name_1').keypress(function() {
		var txtVal = this.value;
		$("#text-input-1").text(txtVal);
	});
	
	$('#featured_name_2').keypress(function() {
		var txtVal = this.value;
		$("#text-input-2").text(txtVal);
	});

	
	$('.corner_set').change(function() {
		if($(this).val() == 'Cover') {
			$('.template-sec').hide();
			$('.cover-template-sec').show();
		}
		if($(this).val() == 'concept') {
			$('.template-sec').hide();
			$('.concept-template-sec').show();
		}
		if($(this).val() == 'Design Agreement') {
			$('.template-sec').hide();
			$('.design-agreement-template-sec').show();
		}
		if($(this).val() == 'Items') {
			$('.template-sec').hide();
			$('.items-template-sec').show();
		}
		if($(this).val() == 'Line Items') {
			$('.template-sec').hide();
			$('.line-items-template-sec').show();
		}
		if($(this).val() == 'Terms') {
			$('.template-sec').hide();
			$('.terms-template-sec').show();
		}
		if($(this).val() == 'Breakdown') {
			$('.template-sec').hide();
			$('.breakdown-template-sec').show();
		}
	});
	
//document close
});