// Worksheet js
$(document).ready(function(){
//worksheet page scripts

$(document).on("click",".dropdown-click",function(){
	$(this).first().parents('.dropdown-click-sec').find('.dropdown-bx').toggleClass('d-none');
	$(this).find('.fa').toggleClass('fa-caret-down fa-caret-right');
});

$(document).on("click",".dropdown-click-inner",function(){
	$(this).first().parents('.dropdown-click-sec-inner').find('.dropdown-bx').toggleClass('d-none');
	$(this).find('.fa').toggleClass('fa-caret-down fa-caret-right');
});

$(document).on("click",".delete-clk",function(){
	$(this).parents(".dropdown-click-sec").remove();
});
$(document).on("click",".delete-clk-inner",function(){
	$(this).parents('tr').remove();
});
$(document).on("click",".cross-td-inner",function(){
	$(this).closest('.tr-inner').remove();
});

$(document).on("click",".lock",function(){
	$(this).find('.fa').toggleClass('fa-unlock fa-lock');
});

//append unique identifier
function uniqueId(){
	unique_id = Math.ceil(Math.random() * 100)*Math.ceil(Math.random() * 10);
	return unique_id;
}

// $(document).on("click",".color-remove",function(){
// 	$(this).parents(".platti-back").remove();
// });




// $(document).on("click",".ingredients-toggle",function(){
// 	$(this).parents('tr').next('tr.none-display').fadeToggle(300);
// });

$(document).on("click",".ingredients-toggle",function(){
	if (!$(this).parents('tr').next('tr').hasClass("none-display")) {
		// $(this).parents('tr').after(ingredientsVar);
		$(this).parents('tr').next('tr.none-display').fadeIn(300);
	}else{
		$(this).parents('tr').next('tr.none-display').fadeToggle(300);
	}
});

$(document).on("click",".cat-rename",function(){
	curr_cat = $(this);
	unique_id = uniqueId();
	curr_cat.find('.cat-name').attr('id',unique_id);
	$('#curr_cat_ref').val(unique_id);
	$('#changeNameMd').modal('show');
});

var ingredientsVar = '<tr class="none-display initial_ingredient">'+
'<td colspan="10" class="bg-greenish">'+
'<div class="text-left ingredients-section">'+
'<div class="row">'+
'<div class="col-lg-3">'+
'<div class="left-image-bx mb-5">'+
'<h6 class="heading item-heading">Delude Inspiration</h6>'+
'<div class="img-dv">'+
'<img src="img/2019_bridescom-Editorial_Images-06-Peony-Wedding-Bouquets-Large-Peony-Bouquet-Refresh-Catherine-Hall-Studios.jpg" alt="Image">'+
'</div>'+
'</div>'+
'</div>'+
'<div class="col-lg-9">'+
'<section class="dropdown-click-sec-inner p-2 mb-4" id="ingredient-sec">'+
'<div class="heading px-3">'+
'<span class="dropdown-click-inner">'+
'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
'</span>'+
'<a href="javascript:void(0);" class="text-black">'+
'<span>Ingredients</span>'+
'</a>'+
'<span class="float-right">'+
'<span class="delete-clk-inner"><i class="fa fa-trash fa-fw"></i></span>'+
'</span>'+
'</div>'+

'<div class="dropdown-bx">'+
'<table class="table table-striped table-list m-0 text-center">'+
'<thead>'+
'<tr>'+
'<th></th>'+
'<th>Qty</th>'+
'<th>Item Name</th>'+
'<th>Cost</th>'+
'<th>Price</th>'+
'<th>Tax</th>'+
'<th></th>'+
'</tr>'+
'</thead>'+
'<tbody>'+
'<tr class="ui-sortable-handle tr-inner">'+
'<td><span><i class="fa fa-plus" aria-hidden="true"></i></span></td>'+
'<td><input type="text" class="ingredient_qty" name="ingredient_qty[]" value="1"></td>'+
'<td>Text Name</td>'+
'<td><input type="text" name="ingredient_price[]" value="$0.00"></td>'+
'<td>$17.00</td>'+
'<td>TP</td>'+
'<td><span class="cross-td-inner"><i class="fa fa-times" aria-hidden="true"></i></span></td>'+
'</tr>'+
'</tbody>'+
'<tfoot>'+
'<tr>'+
'<td colspan="2">Total Cost: <input type="text" name="total_ingred_cost" value="$300"></td>'+
'<td>$46.95</td>'+
'<td>Markup %</td>'+
'<td><input type="text" class="ingredient_markup" name="total_ingredient_markup" value="$300"></td>'+
'<td>Total Price:</td>'+
'<td><input type="text" name="total_ingred_price" value="$187.80" disabled="disable"></td>'+
'</tr>'+
'</tfoot>'+
'</table>'+
'</div>'+
'</section>'+

'<section class="dropdown-click-sec-inner p-2 mb-4">'+
'<div class="heading px-3">'+
'<span class="dropdown-click-inner">'+
'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
'</span>'+
'<a href="javascript:void(0);" class="text-black">'+
'<span>Description</span>'+
'</a>'+
'<span class="float-right">'+
'<span class="delete-clk-inner">'+
'<i class="fa fa-trash fa-fw"></i>'+
'</span>'+
'</span>'+
'</div>'+

'<div class="text">'+
'<textarea class="form-control" rows="5"></textarea>'+
'</div>'+
'</section>'+

'<section class="dropdown-click-sec-inner p-2 mb-4">'+
'<div class="heading px-3">'+
'<span class="dropdown-click-inner">'+
'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
'</span>'+
'<a href="javascript:void(0);" class="text-black">'+
'<span>Notes</span>'+
'</a>'+
'<span class="float-right">'+
'<span class="delete-clk-inner">'+
'<i class="fa fa-trash fa-fw"></i>'+
'</span>'+
'</span>'+
'</div>'+

'<div class="text">'+
'<textarea class="form-control" rows="5"></textarea>'+
'</div>'+
'</section>'+
'</div>'+
'</div>'+
'</div>'+
'</td>'+
'</tr>';


$(document).on("click",".add-row",function(){
	unique_id = uniqueId();
	var curr_unique_id = $(this).parents('.dropdown-bx').find('.body-clone-category').find('.curr_unique_id').val();
	$(this).parents('.dropdown-bx').find('.body-clone-category').append(
		'<tr class="ui-sortable-handle">'+
		'<td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>'+
		'<td>'+
		'<span class="design-board-image-click" id="design-meta_'+unique_id+'" data-toggle="modal" data-target="#designBModal">'+
		'<i class="fa fa-picture-o" aria-hidden="true"></i>'+
		'</span>'+
		'</td>'+
		'<td class="rename-item">'+
		'<button type="button" class="item-name-bx">'+
		'<span class="item-name-txt">Bride</span>'+
		'<input type="text" name="item_name'+curr_unique_id+'[]" class="item-name-input" value="Bride" oninput="set_val(this)" >'+
		'</button>'+
		'</td>'+
		'<td><span class="ingredients-toggle"><i class="fa fa-plus" aria-hidden="true"></i></span></td>'+
		'<td><input type="text" name="item_qty'+curr_unique_id+'[]" class="qty" value="1"></td>'+
		'<td><input type="text" name="item_price'+curr_unique_id+'[]" class="price" value="$0.00"></td>'+
		// '<td><span class="lock"><i class="fa fa-unlock" aria-hidden="true"></i></span></td>'+
		'<td class="total">$0.00</td>'+'<input type="hidden" name="line_total'+curr_unique_id+'[]" value="0.00">'+
		'<td>TP</td><input type="hidden" name="line_item_tax'+curr_unique_id+'[]" value="TP"/>'+
		'<td><span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span></td>'+
		'</tr>' +ingredientsVar
		);
});


var Vtaxes = '<tr>'+
'<td width="80px">Taxes</td>'+
'<td colspan="2">'+
'<span class="mr-2">Product</span>'+
'<input type="text" name="tax[product]" value="2">'+
'</td>'+
'<td colspan="2">'+
'<span class="mr-2">Service</span>'+
'<input type="text" name="tax[service]" value="6.5">'+
'</td>'+
'<td colspan="2">'+
'<span class="mr-2">Labor</span>'+
'<input type="text" name="tax[labor]" value="6.5">'+
'</td>'+
'</tr>';

$(document).on("click",".add-row",function(){
	$(this).parents('.dropdown-bx').find('.body-clone-discounts').append(
		'<tr>'+
		'<td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>'+
		'<td class="rename-item">'+
		'<button type="button" class="item-name-bx">'+
		'<span class="item-name-txt">Discount Name</span>'+
		'<input type="text" name="discount_name[]" class="item-name-input" value="Bride" oninput="set_val(this)">'+
		'</button>'+
		'</td>'+
		'<td>'+
		'<select class="form-control" name="discount_apply_to[]">'+
		'<option value="Products">Products</option>'+
		'<option value="Services">Services</option>'+
		'<option value="Labor">Labor</option>'+
		'<option value="Event">Event</option>'+
		'<option value="Total">Total</option>'+
		'</select>'+
		'</td>'+
		'<td>'+
		'<select class="form-control" name="discount_apply_to_2[]">'+
		'<option value="Evently">Evently</option>'+
		'<option value="Taxable First">Taxable First</option>'+
		'<option value="Non-Taxable First">Non-Taxable First</option>'+
		'</select>'+
		'</td>'+
		'<td>'+
		'<select class="form-control" name="discount_type[]">'+
		'<option value="Flat Amount">Flat Amount</option>'+
		'<option value="Percentage">Percentage</option>'+
		'</select>'+
		'</td>'+
		'<td><input type="text" name="discount_amt[]" value="6.5"></td>'+
		'<td><span class="cross-distx-td"><i class="fa fa-times" aria-hidden="true"></i></span></td>'+
		'</tr>'+Vtaxes
		);
});

$(document).on("click",".add-row",function(){
	$(this).parents('.dropdown-bx').find('.body-clone-fees').append(
		'<tr>'+
		'<td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>'+
		'<td class="rename-item">'+
		'<button type="button" class="item-name-bx">'+
		'<span class="item-name-txt">Fee Name</span>'+
		'<input type="text" name="fee_name[]" class="item-name-input" value="Bride" oninput="set_val(this)">'+
		'</button>'+
		'</td>'+
		'<td>'+
		'<select class="form-control" name="fee_apply_to[]">'+
		'<option value="Products">Products</option>'+
		'<option value="Services">Services</option>'+
		'<option value="Labor">Labor</option>'+
		'<option value="Event">Event</option>'+
		'<option value="Total">Total</option>'+
		'</select>'+
		'</td>'+
		'<td>'+
		'<select class="form-control" name="fee_type[]">'+
		'<option value="Flat Amount">Flat Amount</option>'+
		'<option value="Percentage">Percentage</option>'+
		'</select>'+
		'</td>'+
		'<td><input type="text" name="fee_amt[]" value="6.5"></td>'+
		'<td>'+
		'<select class="form-control" name="fee_tax[]">'+
		'<option value="TP">TP</option>'+
		'<option value="TS">TS</option>'+
		'<option value="TL">TL</option>'+
		'<option value="NT">NT</option>'+
		'</select>'+
		'</td>'+
		'<td><span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span></td>'+
		'</tr>'
		);
});

$(document).on("click",".add-row",function(){
	$(this).parents('.dropdown-bx').find('.body-clone-staff').append(
		'<tr>'+
		'<td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>'+
		'<td class="rename-item">'+
		'<button type="button" class="item-name-bx">'+
		'<span class="item-name-txt"> Name of Staff</span>'+
		'<input type="text" name="staff_title[]" class="item-name-input" value="Bride" oninput="set_val(this)">'+
		'</button>'+
		'</td>'+
		'<td><input type="text" name="staff_count[]" value="0"></td>'+
		'<td><input type="text" name="staff_hours[]" value="0"></td>'+
		'<td><input type="text" name="staff_rate[]" value="6.5"></td>'+
		'<td><input style="display:none;" checked type="checkbox" name="staff_tax[]" value="no"><input type="checkbox" name="staff_tax[]" value="yes" onclick="set_check(this)"></td>'+
		'<td><span>$0.00</span></td>'+
		'<td><span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span></td>'+
		'</tr>'
		);
});

$(document).on("click",".cross-td",function(){
	// $(this).parents('tr').next('tr').remove();
	$(this).parents('tr').remove();
});


$(document).on("click",".cross-distx-td",function(){
	$(this).parents('tr').next('tr').remove();
	$(this).parents('tr').remove();
});


$(function(){
	$( ".body-clone-category,.body-clone-fees,.body-clone-discounts,.body-clone-staff" ).sortable();
	$( ".body-clone-category,.body-clone-fees,.body-clone-discounts,.body-clone-staff" ).disableSelection();
});

// $(".click-cat-btn").click(function(){
// 	$( ".dropdown-cat-group" ).appendTo( ".dropdown-insert-sec" );
// });




$(".click-colorPalette-btn").click(function(){
	unique_id = uniqueId();
	colorPaletteSectionVar = '<section class="dropdown-click-sec p-2 mb-4 dropdown-colorPalette" id="color-sec-'+unique_id+'">'+
	'<div class="heading px-3">'+
	'<span class="dropdown-click">'+
	'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
	'</span>'+
	'<span>Color Palette</span>'+
	'<span class="float-right">'+
	'<span class="delete-clk">'+
	'<i class="fa fa-trash fa-fw"></i>'+
	'<span class="txt">Remove Section</span>'+
	'</span>'+
	'</span>'+
	'</div>'+

	'<div class="dropdown-bx">'+
	'<table class="table table-striped table-list m-0 text-center">'+
	'<tbody class="body-clone-colorPalette">'+
	'<tr>'+
	'<td class="p-0" colspan="">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus myModalpalette" data-toggle="modal" data-target="#myModalpalette"></div>'+
	'</div>'+
	'</td>'+
	'</tr>'+

	'<tr>'+
	'<td class="p-0 bg-white" colspan="3">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus" data-toggle="modal" data-target="#newModalPlusColor"></div>'+
	'</div>'+
	'<div class="modal fade" id="newModalPlusColor" tabindex="-1" role="dialog" aria-labelledby="newModalPlusColor" aria-hidden="true" style="display: none;">'+
	'<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">'+
	'<div class="modal-content">'+
	'<div class="modal-header bg-info text-white">'+
	'<h5 class="modal-title">Add Content</h5>'+
	'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
	'<span aria-hidden="true">×</span>'+
	'</button>'+
	'</div>'+
	'<div class="modal-body text-left">'+
	'<ul class="add-new-sec-list">'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-cat-grup" href="javascript:void(0);">'+
	'<i class="fa fa-th-list" aria-hidden="true"></i>'+
	'<span>Category Group</span> <input type="hidden" class="curr_cat_name" name="curr_cat_name_'+unique_id+'" value="Category Group"/>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-colorPalette-grup" href="javascript:void(0);">'+
	'<i class="fa fa-paint-brush" aria-hidden="true"></i>'+
	'<span>Color Palette</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-discounts-grup" href="javascript:void(0);">'+
	'<i class="fa fa-credit-card" aria-hidden="true"></i>'+
	'<span>Discounts/Taxes</span>'+
	'</a>'+
	'</li>'+
	'</ul>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</td>'+
	'</tr>'+

	'</tbody>'+
	'</table>'+
	'</div>'+
	'</section>';
	$('.dropdown-insert-sec').append(colorPaletteSectionVar);
});

$(".click-cat-btn").click(function(){
	unique_id = uniqueId();
	catSectionVar = '<section class="dropdown-click-sec p-2 mb-4 dropdown-cat-group" id="cat-sec-'+unique_id+'">'+
	'<div class="heading px-3">'+
	'<span class="dropdown-click">'+
	'<input type="hidden" id="curr_cat_ref" name="curr_cat_ref"/>'+
	'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
	'</span>'+
	'<a href="javascript:void(0);" class="text-black cat-rename">'+
	'<span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>'+
	'<span class="cat-name" id="catnewname">Category Group</span> <input type="hidden" class="curr_cat_name" name="curr_cat_name_'+unique_id+'" value="Category Group"/>'+
	'</a>'+
	'<span class="float-right">'+
	'<span class="delete-clk">'+
	'<i class="fa fa-trash fa-fw"></i>'+
	'<span class="txt">Remove Section</span>'+
	'</span>'+
	'</span>'+
	'</div>'+

	'<div class="dropdown-bx">'+
	'<table class="table table-striped table-list m-0 text-center">'+
	'<thead>'+
	'<tr>'+
	'<th></th>'+
	'<th></th>'+
	'<th>Item Name</th>'+
	'<th></th>'+
	'<th>Qty</th>'+
	'<th>Est Price</th>'+
	// '<th>Lock</th>'+
	'<th>Total</th>'+
	'<th>Tax</th>'+
	'<th></th>'+
	'</tr>'+
	'</thead>'+
	'<tbody class="body-clone-category">'+
	'<input type="hidden" class="curr_unique_id" value="'+unique_id+'"/>'+
	'<tr class="ui-sortable-handle">'+
	'<td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>'+
	'<td>'+
	'<span class="design-board-image-click" id="design-meta_'+unique_id+'" data-toggle="modal" data-target="#designBModal">'+
	'<i class="fa fa-picture-o" aria-hidden="true"></i>'+
	'</span>'+
	'</td>'+
	'<td class="rename-item">'+
	'<button type="button" class="item-name-bx">'+
	'<span class="item-name-txt">Bride</span>'+
	'<input type="text" name="item_name'+unique_id+'[]" class="item-name-input" value="Bride" oninput="set_val(this)" >'+
	'</button>'+
	'</td>'+
	'<td><span class="ingredients-toggle"><i class="fa fa-plus" aria-hidden="true"></i></span></td>'+
	'<td><input type="text" name="item_qty'+unique_id+'[]" class="qty" value="1"></td>'+
	'<td><input type="text" name="item_price'+unique_id+'[]" class="price" value="$0.00"></td>'+
	// '<td><span class="lock"><i class="fa fa-unlock" aria-hidden="true"></i></span></td>'+
	'<td class="total">$0.00</td>'+'<input type="hidden" name="line_total'+unique_id+'[]" value="0.00">'+
	'<td>TP</td><input type="hidden" name="line_item_tax'+unique_id+'[]" value="TP"/>'+
	'<td><span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span></td>'+
	'</tr>'+ ingredientsVar+
	'<tfoot>'+
	'<tr>'+
	'<td class="p-0" colspan="10"><button type="button" class="btn w-100 add-row"> + Add New Row</button></td>'+
	'</tr>'+
	'<tr>'+
	'<th class="text-right" colspan="8">Section Subtotal:</th>'+
	'<th>$0.00</th>'+
	'</tr>'+
	'<tr>'+
	'<td class="p-0" colspan="10">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus addplus-cat" data-toggle="modal" data-target="#newModalPlusCat"></div>'+
	'</div>'+
	'<div class="modal fade" id="newModalPlusCat" tabindex="-1" role="dialog" aria-labelledby="newModalPlusCat" aria-hidden="true">'+
	'<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">'+
	'<div class="modal-content">'+
	'<div class="modal-header bg-info text-white">'+
	'<h5 class="modal-title">Add Content</h5>'+
	'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
	'<span aria-hidden="true">×</span>'+
	'</button>'+
	'</div>'+
	'<div class="modal-body text-left">'+
	'<ul class="add-new-sec-list">'+
	'<li class="box-shadow my-3">'+
	'<a class="p-3 bg-gray d-block plus-cat-grup" href="javascript:void(0);">'+
	'<i class="fa fa-th-list" aria-hidden="true"></i>'+
	'<span>Category Group</span> <input type="hidden"/>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow my-3">'+
	'<a class="p-2 bg-gray d-block plus-colorPalette-grup" href="javascript:void(0);">'+
	'<i class="fa fa-paint-brush" aria-hidden="true"></i>'+
	'<span>Color Palette</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow my-3">'+
	'<a class="p-3 bg-gray d-block plus-discounts-grup" href="javascript:void(0);">'+
	'<i class="fa fa-credit-card" aria-hidden="true"></i>'+
	'<span>Discounts/Taxes</span>'+
	'</a>'+
	'</li>'+
	'</ul>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</td>'+
	'</tr>'+
	'</tfoot>'+
	'</tbody>'+
	'</table>'+
	'</div>'+
	'</section>';
	$('.dropdown-insert-sec').append(catSectionVar);
});
$(".click-discounts-btn").click(function(){
	unique_id = uniqueId();
	discountsSectionVar = '<section class="dropdown-click-sec p-2 mb-4 dropdown-discounts" id="discount-sec-'+unique_id+'">'+
	'<div class="heading px-3">'+
	'<span class="dropdown-click">'+
	'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
	'</span>'+
	'<span>Discounts</span>'+
	'<span class="float-right">'+
	'<span class="delete-clk">'+
	'<i class="fa fa-trash fa-fw"></i>'+
	'<span class="txt">Remove Section</span>'+
	'</span>'+
	'</span>'+
	'</div>'+

	'<div class="dropdown-bx">'+
	'<table class="table table-striped table-list m-0 text-center">'+
	'<thead>'+
	'<tr>'+
	'<th></th>'+
	'<th>Discount Name</th>'+
	'<th>Apply To</th>'+
	'<th></th>'+
	'<th>Type</th>'+
	'<th>Amount</th>'+
	'<th></th>'+
	'</tr>'+
	'</thead>'+
	'<tbody class="body-clone-discounts">'+
	'<tr>'+
	'<td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>'+
	'<td class="rename-item">'+
	'<button type="button" class="item-name-bx">'+
	'<span class="item-name-txt">Name</span>'+
	'<input type="text" name="discount_name[]" class="item-name-input" value="Bride" oninput="set_val(this)" value="">'+
	'</button>'+
	'</td>'+
	'<td>'+
	'<select class="form-control" name="discount_apply_to[]">'+
	'<option selected value=""></option>'+
	'<option value="Products">Products</option>'+
	'<option value="Services">Services</option>'+
	'<option value="Labor">Labor</option>'+
	'<option value="Event">Event</option>'+
	'<option value="Total">Total</option>'+
	'</select>'+
	'</td>'+
	'<td>'+
	'<select class="form-control" name="discount_apply_to_2[]">'+
	'<option selected value=""></option>'+
	'<option value="Evently">Evently</option>'+
	'<option value="Taxable First">Taxable First</option>'+
	'<option value="Non-Taxable First">Non-Taxable First</option>'+
	'</select>'+
	'</td>'+
	'<td>'+
	'<select class="form-control" name="discount_type[]">'+
	'<option selected value=""></option>'+
	'<option value="Flat Amount">Flat Amount</option>'+
	'<option value="Percentage">Percentage</option>'+
	'</select>'+
	'</td>'+
	'<td><input type="text" name="discount_amt[]" value=""></td>'+
	'<td><span class="cross-distx-td"><i class="fa fa-times" aria-hidden="true"></i></span>'+
	'</td>'+
	'</tr>'+ Vtaxes +
	'<tfoot>'+
	'<tr>'+
	'<td class="p-0" colspan="7"><button type="button" class="btn w-100 add-row">+ Add New Row</button></td>'+
	'</tr>'+
	'<tr>'+
	'<td class="p-0" colspan="9">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus addplus-cat" data-toggle="modal" data-target="#newModalPlusDiscounts"></div>'+
	'</div>'+
	'<div class="modal fade" id="newModalPlusDiscounts" tabindex="-1" role="dialog" aria-labelledby="newModalPlusDiscounts" aria-hidden="true">'+
	'<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">'+
	'<div class="modal-content">'+
	'<div class="modal-header bg-info text-white">'+
	'<h5 class="modal-title">Add Content</h5>'+
	'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
	'<span aria-hidden="true">×</span>'+
	'</button>'+
	'</div>'+
	'<div class="modal-body text-left">'+
	'<ul class="add-new-sec-list">'+
	'<li class="box-shadow my-3">'+
	'<a class="p-3 bg-gray d-block plus-cat-grup" href="javascript:void(0);">'+
	'<i class="fa fa-th-list" aria-hidden="true"></i>'+
	'<span>Category Group</span> <input type="hidden" class="curr_cat_name" name="curr_cat_name_'+unique_id+'" value="Category Group"/>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow my-3">'+
	'<a class="p-2 bg-gray d-block plus-colorPalette-grup" href="javascript:void(0);">'+
	'<i class="fa fa-paint-brush" aria-hidden="true"></i>'+
	'<span>Color Palette</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow my-3">'+
	'<a class="p-3 bg-gray d-block plus-discounts-grup" href="javascript:void(0);">'+
	'<i class="fa fa-credit-card" aria-hidden="true"></i>'+
	'<span>Discounts/Taxes</span>'+
	'</a>'+
	'</li>'+
	'</ul>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</td>'+
	'</tr>'+
	'</tfoot>'+
	'</tbody>'+
	'</table>'+
	'</div>'+
	'</section>';
	$('.dropdown-insert-sec').append(discountsSectionVar);
});
$(".click-taxes-btn").click(function(){
	unique_id = uniqueId();
	taxesSectionVar = '<section class="dropdown-click-sec p-2 mb-4 dropdown-taxes" id="tax-sec-'+unique_id+'">'+
	'<div class="heading px-3">'+
	'<span class="dropdown-click"><i class="fa fa-caret-down" aria-hidden="true"></i></span>'+
	'<span>Taxes</span>'+
	'<span class="float-right">'+
	'<span class="delete-clk">'+
	'<i class="fa fa-trash fa-fw"></i>'+
	'<span class="txt">Remove Section</span>'+
	'</span>'+
	'</span>'+
	'</div>'+

	'<div class="dropdown-bx">'+
	'<table class="table table-striped table-list m-0 text-center">'+
	'<tbody>'+
	'<tr>'+
	'<th>Percentage</th>'+
	'<th>Percentage</th>'+
	'<th>Percentage</th>'+
	'</tr>'+
	'<tr>'+
	'<td>'+
	'<span class="mr-2">Product</span>'+
	'<input type="text" name="tax[product]" value="">'+
	'</td>'+
	'<td>'+
	'<span class="mr-2">Service</span>'+
	'<input type="text" name="tax[service]" value="">'+
	'</td>'+
	'<td>'+
	'<span class="mr-2">Labor</span>'+
	'<input type="text" name="tax[labor]" value="">'+
	'</td>'+
	'</tr>'+
	'<tr>'+
	'<td class="p-0 bg-white" colspan="3">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus" data-toggle="modal" data-target="#newModalPlusTaxes"></div>'+
	'</div>'+

	'<div class="modal fade" id="newModalPlusTaxes" tabindex="-1" role="dialog" aria-labelledby="newModalPlusTaxes" aria-hidden="true" style="display: none;">'+
	'<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">'+
	'<div class="modal-content">'+
	'<div class="modal-header bg-info text-white">'+
	'<h5 class="modal-title">Add Content</h5>'+
	'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
	'<span aria-hidden="true">×</span>'+
	'</button>'+
	'</div>'+
	'<div class="modal-body text-left">'+
	'<ul class="add-new-sec-list">'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-cat-grup" href="javascript:void(0);">'+
	'<i class="fa fa-th-list" aria-hidden="true"></i>'+
	'<span>Category Group</span> <input type="hidden" class="curr_cat_name" name="curr_cat_name_'+unique_id+'" value="Category Group"/>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-colorPalette-grup" href="javascript:void(0);">'+
	'<i class="fa fa-paint-brush" aria-hidden="true"></i>'+
	'<span>Color Palette</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-discounts-grup" href="javascript:void(0);">'+
	'<i class="fa fa-credit-card" aria-hidden="true"></i>'+
	'<span>Discounts/Taxes</span>'+
	'</a>'+
	'</li>'+
	'</ul>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</td>'+
	'</tr>'+
	'</tbody>'+
	'</table>'+
	'</div>'+
	'</section>';
	$('.dropdown-insert-sec').append(taxesSectionVar);
});


$(document).on("click",".plus-cat-grup",function(){
	unique_id = uniqueId();
	catSectionVar = '<section class="dropdown-click-sec p-2 mb-4 dropdown-cat-group" id="cat-sec-'+unique_id+'">'+
	'<div class="heading px-3">'+
	'<span class="dropdown-click">'+
	'<input type="hidden" id="curr_cat_ref" name="curr_cat_ref"/>'+
	'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
	'</span>'+
	'<a href="javascript:void(0);" class="text-black cat-rename">'+
	'<span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>'+
	'<span class="cat-name" id="catnewname">Category Group</span> <input type="hidden" class="curr_cat_name" name="curr_cat_name_'+unique_id+'" value="Category Group"/>'+
	'</a>'+
	'<span class="float-right">'+
	'<span class="delete-clk">'+
	'<i class="fa fa-trash fa-fw"></i>'+
	'<span class="txt">Remove Section</span>'+
	'</span>'+
	'</span>'+
	'</div>'+

	'<div class="dropdown-bx">'+
	'<table class="table table-striped table-list m-0 text-center">'+
	'<thead>'+
	'<tr>'+
	'<th></th>'+
	'<th></th>'+
	'<th>Item Name</th>'+
	'<th></th>'+
	'<th>Qty</th>'+
	'<th>Est Price</th>'+
	// '<th>Lock</th>'+
	'<th>Total</th>'+
	'<th>Tax</th>'+
	'<th></th>'+
	'</tr>'+
	'</thead>'+
	'<tbody class="body-clone-category">'+
	'<input type="hidden" class="curr_unique_id" value="'+unique_id+'"/>'+
	'<tr class="ui-sortable-handle">'+
	'<td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>'+
	'<td>'+
	'<span class="design-board-image-click" id="design-meta_'+unique_id+'" data-toggle="modal" data-target="#designBModal">'+
	'<i class="fa fa-picture-o" aria-hidden="true"></i>'+
	'</span>'+
	'</td>'+
	'<td class="rename-item">'+
	'<button type="button" class="item-name-bx">'+
	'<span class="item-name-txt">Bride</span>'+
	'<input type="text" name="item_name'+unique_id+'[]" class="item-name-input" value="Bride" oninput="set_val(this)" >'+
	'</button>'+
	'</td>'+
	'<td><span class="ingredients-toggle"><i class="fa fa-plus" aria-hidden="true"></i></span></td>'+
	'<td><input type="text" name="item_qty'+unique_id+'[]" class="qty" value="1"></td>'+
	'<td><input type="text" name="item_price'+unique_id+'[]" class="price" value="$0.00"></td>'+
	// '<td><span class="lock"><i class="fa fa-unlock" aria-hidden="true"></i></span></td>'+
	'<td class="total">$0.00</td>'+'<input type="hidden" name="line_total'+unique_id+'[]" value="0.00">'+
	'<td>TP</td><input type="hidden" name="line_item_tax'+unique_id+'[]" value="TP"/>'+
	'<td><span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span></td>'+
	'</tr>'+ ingredientsVar+
	'<tfoot>'+
	'<tr>'+
	'<td class="p-0" colspan="10"><button type="button" class="btn w-100 add-row"> + Add New Row</button></td>'+
	'</tr>'+
	'<tr>'+
	'<th class="text-right" colspan="8">Section Subtotal:</th>'+
	'<th>$0.00</th>'+
	'</tr>'+
	'<tr>'+
	'<td class="p-0" colspan="10">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus addplus-cat" data-toggle="modal" data-target="#newModalPlusCat"></div>'+
	'</div>'+
	'<div class="modal fade" id="newModalPlusCat" tabindex="-1" role="dialog" aria-labelledby="newModalPlusCat" aria-hidden="true">'+
	'<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">'+
	'<div class="modal-content">'+
	'<div class="modal-header bg-info text-white">'+
	'<h5 class="modal-title">Add Content</h5>'+
	'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
	'<span aria-hidden="true">×</span>'+
	'</button>'+
	'</div>'+
	'<div class="modal-body text-left">'+
	'<ul class="add-new-sec-list">'+
	'<li class="box-shadow my-3">'+
	'<a class="p-3 bg-gray d-block plus-cat-grup" href="javascript:void(0);">'+
	'<i class="fa fa-th-list" aria-hidden="true"></i>'+
	'<span>Category Group</span> <input type="hidden"/>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow my-3">'+
	'<a class="p-2 bg-gray d-block plus-colorPalette-grup" href="javascript:void(0);">'+
	'<i class="fa fa-paint-brush" aria-hidden="true"></i>'+
	'<span>Color Palette</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow my-3">'+
	'<a class="p-3 bg-gray d-block plus-discounts-grup" href="javascript:void(0);">'+
	'<i class="fa fa-credit-card" aria-hidden="true"></i>'+
	'<span>Discounts/Taxes</span>'+
	'</a>'+
	'</li>'+
	'</ul>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</td>'+
	'</tr>'+
	'</tfoot>'+
	'</tbody>'+
	'</table>'+
	'</div>'+
	'</section>';
	curr_id = $("#curr_sec").val();
	$(curr_id).after(catSectionVar);
	// $(this).parents('.dropdown-click-sec').after(catSectionVar);
});
$(document).on("click",".plus-discounts-grup",function(){
	unique_id = uniqueId();
	discountsSectionVar = '<section class="dropdown-click-sec p-2 mb-4 dropdown-discounts" id="discount-sec-'+unique_id+'">'+
	'<div class="heading px-3">'+
	'<span class="dropdown-click">'+
	'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
	'</span>'+
	'<span>Discounts</span>'+
	'<span class="float-right">'+
	'<span class="delete-clk">'+
	'<i class="fa fa-trash fa-fw"></i>'+
	'<span class="txt">Remove Section</span>'+
	'</span>'+
	'</span>'+
	'</div>'+

	'<div class="dropdown-bx">'+
	'<table class="table table-striped table-list m-0 text-center">'+
	'<thead>'+
	'<tr>'+
	'<th></th>'+
	'<th>Discount Name</th>'+
	'<th>Apply To</th>'+
	'<th></th>'+
	'<th>Type</th>'+
	'<th>Amount</th>'+
	'<th></th>'+
	'</tr>'+
	'</thead>'+
	'<tbody class="body-clone-discounts">'+
	'<tr>'+
	'<td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>'+
	'<td class="rename-item">'+
	'<button type="button" class="item-name-bx">'+
	'<span class="item-name-txt">Name</span>'+
	'<input type="text" name="discount_name[]" class="item-name-input" value="Bride" oninput="set_val(this)" value="">'+
	'</button>'+
	'</td>'+
	'<td>'+
	'<select class="form-control" name="discount_apply_to[]">'+
	'<option selected value=""></option>'+
	'<option value="Products">Products</option>'+
	'<option value="Services">Services</option>'+
	'<option value="Labor">Labor</option>'+
	'<option value="Event">Event</option>'+
	'<option value="Total">Total</option>'+
	'</select>'+
	'</td>'+
	'<td>'+
	'<select class="form-control" name="discount_apply_to_2[]">'+
	'<option selected value=""></option>'+
	'<option value="Evently">Evently</option>'+
	'<option value="Taxable First">Taxable First</option>'+
	'<option value="Non-Taxable First">Non-Taxable First</option>'+
	'</select>'+
	'</td>'+
	'<td>'+
	'<select class="form-control" name="discount_type[]">'+
	'<option selected value=""></option>'+
	'<option value="Flat Amount">Flat Amount</option>'+
	'<option value="Percentage">Percentage</option>'+
	'</select>'+
	'</td>'+
	'<td><input type="text" name="discount_amt[]" value=""></td>'+
	'<td><span class="cross-distx-td"><i class="fa fa-times" aria-hidden="true"></i></span>'+
	'</td>'+
	'</tr>'+ Vtaxes +
	'<tfoot>'+
	'<tr>'+
	'<td class="p-0" colspan="7"><button type="button" class="btn w-100 add-row">+ Add New Row</button></td>'+
	'</tr>'+
	'<tr>'+
	'<td class="p-0" colspan="9">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus addplus-cat" data-toggle="modal" data-target="#newModalPlusDiscounts"></div>'+
	'</div>'+
	'<div class="modal fade" id="newModalPlusDiscounts" tabindex="-1" role="dialog" aria-labelledby="newModalPlusDiscounts" aria-hidden="true">'+
	'<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">'+
	'<div class="modal-content">'+
	'<div class="modal-header bg-info text-white">'+
	'<h5 class="modal-title">Add Content</h5>'+
	'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
	'<span aria-hidden="true">×</span>'+
	'</button>'+
	'</div>'+
	'<div class="modal-body text-left">'+
	'<ul class="add-new-sec-list">'+
	'<li class="box-shadow my-3">'+
	'<a class="p-3 bg-gray d-block plus-cat-grup" href="javascript:void(0);">'+
	'<i class="fa fa-th-list" aria-hidden="true"></i>'+
	'<span>Category Group</span> <input type="hidden" class="curr_cat_name" name="curr_cat_name_'+unique_id+'" value="Category Group"/>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow my-3">'+
	'<a class="p-2 bg-gray d-block plus-colorPalette-grup" href="javascript:void(0);">'+
	'<i class="fa fa-paint-brush" aria-hidden="true"></i>'+
	'<span>Color Palette</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow my-3">'+
	'<a class="p-3 bg-gray d-block plus-discounts-grup" href="javascript:void(0);">'+
	'<i class="fa fa-credit-card" aria-hidden="true"></i>'+
	'<span>Discounts/Taxes</span>'+
	'</a>'+
	'</li>'+
	'</ul>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</td>'+
	'</tr>'+
	'</tfoot>'+
	'</tbody>'+
	'</table>'+
	'</div>'+
	'</section>';
	curr_id = $("#curr_sec").val();
	$(curr_id).after(discountsSectionVar);
	// $(this).parents('.dropdown-click-sec').after(discountsSectionVar);
});


/*
$(document).on("click",".plus-taxes-grup",function(){
	unique_id = uniqueId();
	taxesSectionVar = '<section class="dropdown-click-sec p-2 mb-4 dropdown-taxes" id="tax-sec-'+unique_id+'">'+
	'<div class="heading px-3">'+
	'<span class="dropdown-click"><i class="fa fa-caret-down" aria-hidden="true"></i></span>'+
	'<span>Taxes</span>'+
	'<span class="float-right">'+
	'<span class="delete-clk">'+
	'<i class="fa fa-trash fa-fw"></i>'+
	'<span class="txt">Remove Section</span>'+
	'</span>'+
	'</span>'+
	'</div>'+

	'<div class="dropdown-bx">'+
	'<table class="table table-striped table-list m-0 text-center">'+
	'<tbody>'+
	'<tr>'+
	'<th>Percentage</th>'+
	'<th>Percentage</th>'+
	'<th>Percentage</th>'+
	'</tr>'+
	'<tr>'+
	'<td>'+
	'<span class="mr-2">Product</span>'+
	'<input type="text" name="tax[product]" value="">'+
	'</td>'+
	'<td>'+
	'<span class="mr-2">Service</span>'+
	'<input type="text" name="tax[service]" value="">'+
	'</td>'+
	'<td>'+
	'<span class="mr-2">Labor</span>'+
	'<input type="text" name="tax[labor]" value="">'+
	'</td>'+
	'</tr>'+
	'<tr>'+
	'<td class="p-0 bg-white" colspan="3">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus" data-toggle="modal" data-target="#newModalPlusTaxes"></div>'+
	'</div>'+

	'<div class="modal fade" id="newModalPlusTaxes" tabindex="-1" role="dialog" aria-labelledby="newModalPlusTaxes" aria-hidden="true" style="display: none;">'+
	'<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">'+
	'<div class="modal-content">'+
	'<div class="modal-header bg-info text-white">'+
	'<h5 class="modal-title">Add Content</h5>'+
	'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
	'<span aria-hidden="true">×</span>'+
	'</button>'+
	'</div>'+
	'<div class="modal-body text-left">'+
	'<ul class="add-new-sec-list">'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-cat-grup" href="javascript:void(0);">'+
	'<i class="fa fa-th-list" aria-hidden="true"></i>'+
	'<span>Category Group</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-colorPalette-grup" href="javascript:void(0);">'+
	'<i class="fa fa-paint-brush" aria-hidden="true"></i>'+
	'<span>Color Palette</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-discounts-grup" href="javascript:void(0);">'+
	'<i class="fa fa-credit-card" aria-hidden="true"></i>'+
	'<span>Discounts</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-taxes-grup" href="javascript:void(0);">'+
	'<i class="fa fa-usd" aria-hidden="true"></i>'+
	'<span>Taxes</span>'+
	'</a>'+
	'</li>'+
	'</ul>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</td>'+
	'</tr>'+
	'</tbody>'+
	'</table>'+
	'</div>'+
	'</section>';
	curr_id = $("#curr_sec").val();
	$(curr_id).after(taxesSectionVar);
	// $(this).parents('.dropdown-click-sec').after(taxesSectionVar);
});
*/


$(document).on("click",".plus-colorPalette-grup",function(){
	unique_id = uniqueId();
	colorPaletteSectionVar = '<section class="dropdown-click-sec p-2 mb-4 dropdown-colorPalette" id="color-sec-'+unique_id+'">'+
	'<div class="heading px-3">'+
	'<span class="dropdown-click">'+
	'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
	'</span>'+
	'<span>Color Palette</span>'+
	'<span class="float-right">'+
	'<span class="delete-clk">'+
	'<i class="fa fa-trash fa-fw"></i>'+
	'<span class="txt">Remove Section</span>'+
	'</span>'+
	'</span>'+
	'</div>'+

	'<div class="dropdown-bx">'+
	'<table class="table table-striped table-list m-0 text-center">'+
	'<tbody class="body-clone-colorPalette">'+
	'<tr>'+
	'<td class="p-0" colspan="">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus myModalpalette" data-toggle="modal" data-target="#myModalpalette"></div>'+
	'</div>'+
	'</td>'+
	'</tr>'+

	'<tr>'+
	'<td class="p-0 bg-white" colspan="3">'+
	'<div class="text-center p-3">'+
	'<div class="add-sec-plus" data-toggle="modal" data-target="#newModalPlusColor"></div>'+
	'</div>'+
	'<div class="modal fade" id="newModalPlusColor" tabindex="-1" role="dialog" aria-labelledby="newModalPlusColor" aria-hidden="true" style="display: none;">'+
	'<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">'+
	'<div class="modal-content">'+
	'<div class="modal-header bg-info text-white">'+
	'<h5 class="modal-title">Add Content</h5>'+
	'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
	'<span aria-hidden="true">×</span>'+
	'</button>'+
	'</div>'+
	'<div class="modal-body text-left">'+
	'<ul class="add-new-sec-list">'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-cat-grup" href="javascript:void(0);">'+
	'<i class="fa fa-th-list" aria-hidden="true"></i>'+
	'<span>Category Group</span> <input type="hidden" class="curr_cat_name" name="curr_cat_name_'+unique_id+'" value="Category Group"/>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-colorPalette-grup" href="javascript:void(0);">'+
	'<i class="fa fa-paint-brush" aria-hidden="true"></i>'+
	'<span>Color Palette</span>'+
	'</a>'+
	'</li>'+
	'<li class="box-shadow mb-3">'+
	'<a class="p-3 bg-gray d-block plus-discounts-grup" href="javascript:void(0);">'+
	'<i class="fa fa-credit-card" aria-hidden="true"></i>'+
	'<span>Discounts/Taxes</span>'+
	'</a>'+
	'</li>'+
	'</ul>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</div>'+
	'</td>'+
	'</tr>'+

	'</tbody>'+
	'</table>'+
	'</div>'+
	'</section>';
	curr_id = $("#curr_sec").val();
	$(curr_id).after(colorPaletteSectionVar);
	// $(this).parents('.dropdown-click-sec').after(colorPaletteSectionVar);
});


$(document).on("click",".click-new-sec",function(){
	$( ".add-new-sec .close" ).trigger( "click" );
});

$(document).on("click",".add-new-sec-list a",function(){
	$(this).parents('.modal').find('.close').trigger( "click" );
});


$('#design-board-click').on('click', function(){
	$('.dropdown-click-left-sec').addClass('d-none');
	$('.dropdown-click-image-sec').removeClass('d-none');
});
$('#edit-worksheet-click').on('click', function(){
	$('.dropdown-click-left-sec').addClass('d-none');
	$('.worksheet-left-sec').removeClass('d-none');
});


$(document).on("click",".add-sec-plus",function(){
	var curr = $(this);
	$("#newModalPlus").modal('show');
	curr_id = curr.parents('.dropdown-click-sec').attr('id');
	$("#curr_sec").val('#'+curr_id);
});



$('.add-sec-plus').on('click', function(){
	$( ".add-new-sec .close" ).trigger( "click" );
});


//Currently added target
$(document).on("mouseover",".add-sec-plus",function(){
	$("#curr_add").val(JSON.stringify($(this)));
});




//add color to favourite
$(document).on("click","#addtofav",function(e){
	e.preventDefault();
	hexcode = $("#hex").val();
	user_id = $("#curr_user_id").val();
	if(hexcode != '')
	{
		$.ajax({
			url: "../add-color-to-favourite.php",
			type: "post",
			data: {
				hexcode:hexcode,
				user_id:user_id
			},
			success: function(message) {
				$("#FCUPDATE").load(" #FCUPDATE");
			}
		});
	}
});

//Select color
$(document).on("click","#selectcolor",function(e){
	e.preventDefault();
	hexcode = $("#hex").val();
	if(hexcode != '')
	{
		append_select_color_div = '<div class="col-1 platti-back p-3 ml-2 mt-3 mb-3" style="background-color:'+hexcode+'"><input type="hidden" name="Colors[]" value="'+hexcode+'" /> <span class="color-remove"><i class="fa fa-trash fa-fw"></i></span></div>';
		$(".slcolors").prepend(append_select_color_div);
		$( "#ColorClose" ).trigger( "click" );
	}
	
});

//Select color from favorite start
$(document).on("click",".favcdiv",function(e){
	e.preventDefault();
	hexcode = $(this).attr('id');
	if(hexcode != '')
	{
		document.getElementById('back-pickcolor-set').style.backgroundColor = hexcode;
		document.getElementById('hex').value = hexcode;
	}
	
});
//Select color from favorite end

// Rename Cat
$(document).on("click","#CatNameSub",function(e){
	var catname = $("#CatNameVal").val();
	e.preventDefault();
	if(catname != '')
	{	target_cat = $("#curr_cat_ref").val();
		$("#"+target_cat).text(catname);
		$("#"+target_cat).next(".curr_cat_name").removeAttr("value");
		$("#"+target_cat).next("input[class=curr_cat_name]").val(catname);
		$("#CatNameVal").val('');
		
	}
	
});

//Store current designboard meta ref
$(document).on("click", ".design-board-image-click", function(){
	curr_id = $(this).attr("id");
	$("#curr_design_meta").val("#"+curr_id);
});

//add designboard item to worksheet 
$(document).on("click",".image-grid-dv-one",function(e){
	e.preventDefault();
	var curr = $(this);
	var unique_id = uniqueId();
	var curr_img_target = curr.find('img').attr('data-target');
	var curr_img = curr.find('img').attr('src');
	var curr_design_meta = $("#curr_design_meta").val();
	var curr_item_id = curr_design_meta.split("#design-meta_")[1];

	var flag = 0;
	if(curr_img_target.indexOf("recipe")!=-1){
		strlen = curr_img_target.length;
		id = curr_img_target.substring(curr_img_target.indexOf("model")+5, strlen);
	}
	else if(curr_img_target.indexOf("item")!=-1){
		// $(curr_design_meta).parent().next().next().find(".ingredients-toggle").remove();
		strlen = curr_img_target.length;
		id = curr_img_target.substring(curr_img_target.indexOf("model")+5, strlen);
		flag = 1;
	}
	$(curr_design_meta).find(".fa-picture-o").remove();
	curr_image_name = $(curr_design_meta).parents(".body-clone-category").find(".item_image").attr("name");
	if(typeof curr_image_name == "undefined"){	
		design_image = '<img src="'+curr_img+'" class="fa fa-picture-o" height="60" width="80"><input type="hidden" class="item_image" name="item_image'+curr_item_id+'[]" value="'+curr_img+'">';
	}
	else{
		design_image = '<img src="'+curr_img+'" class="fa fa-picture-o" height="60" width="80"><input type="hidden" class="item_image" name="'+curr_image_name+'" value="'+curr_img+'">';
	}
	$(curr_design_meta).after(design_image); 	
	
	// call target item/recipe
	if(flag == 0){
		$.ajax({
			url: "../auto_suggestions.php",
			type: "post",
			async: false,
			dataType: 'json',
			// contentType: "html",
			data: {
				type:'recipe',
				id:id
			},
			success: function(message) {
				console.log(message);
				$(curr_design_meta).parents(".ui-sortable-handle").next().find(".img-dv img").attr("src","/img/"+message.recipe.name).attr("alt",message.recipe.name);
				recipe_name = message.recipe.recipe_name;
				recipe_price = message.recipe.price;
				$(curr_design_meta).parents(".ui-sortable-handle:first").next().find(".item-heading").text(recipe_name);
				// $(curr_design_meta).parent().next().find(".item-name-txt").text(recipe_name);
				// $(curr_design_meta).parent().next().find(".item-name-input").val(recipe_name);
				var qty = $(curr_design_meta).parent().next().next().next().find(".qty").val();
				$(curr_design_meta).parent().next().next().next().next().find(".price").val("$"+recipe_price);
				$(curr_design_meta).parent().next().next().next().next().next().text("$"+parseFloat(recipe_price*parseInt(qty)).toFixed(2));
				$(curr_design_meta).parent().next().next().next().next().next().next().val("$"+parseFloat(recipe_price*parseInt(qty)).toFixed(2));
				
				if(typeof message.item != "undefined"){	
					// $(curr_design_meta).parent().next().next().find(".fa-plus").remove();
					ingredient_qty = message.recipe.qty.replace(/^,/,"").split(",");
				
					var append_items = '<tr class="none-display">'+
												'<td colspan="10" class="bg-greenish">'+
                                                    '<div class="text-left ingredients-section">'+
                                                        '<div class="row">'+
                                                            '<div class="col-lg-3">'+
                                                                '<div class="left-image-bx mb-5">'+
																'<h6 class="heading item-heading">'+recipe_name+'</h6>'+
                                                                    '<div class="img-dv">'+
                                                                        '<img src="'+curr_img+'" alt="Image">'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</div><!-- col -->'+
                                                            '<div class="col-lg-9">'+
                                                                '<section class="dropdown-click-sec-inner p-2 mb-4" id="ingredient-sec">'+
                                                                    '<div class="heading px-3">'+
                                                                        '<span class="dropdown-click-inner">'+
                                                                            '<i class="fa fa-caret-down" aria-hidden="true"></i>'+
                                                                        '</span>'+
                                                                        '<a href="javascript:void(0);" class="text-black">'+       
                                                                            '<span>Ingredients</span>'+
                                                                        '</a>'+
                                                                        '<span class="float-right">'+
                                                                            '<span class="delete-clk-inner">'+
                                                                                '<i class="fa fa-trash fa-fw"></i>'+
                                                                            '</span>'+
                                                                        '</span>'+
                                                                    '</div>'+
                                                                    '<div class="dropdown-bx">'+
                                                                        '<table class="table table-striped table-list m-0 text-center">'+
                                                                            '<thead>'+
                                                                                '<tr>'+           
                                                                                    '<th></th>'+
                                                                                    '<th>Qty</th>'+
                                                                                    '<th>Item Name</th>'+
                                                                                    '<th>Cost</th>'+
                                                                                    '<th>Price</th>'+           
                                                                                    '<th>Tax</th>'+
                                                                                    '<th></th>'+
                                                                                '</tr>'+
																			'</thead><tbody>';
					var total_stem_cost = 0;
					var append_items_2 = '';
					var append_items_3 = '';
					total_ingredient_price = 0;
					$.each(message.item, function (key, data) {
						// $.each(data, function (index, data) {
							item_type = data.item_type;
							total_stem_cost = total_stem_cost+data.default_stem_cost;
							total_default_item = key+1;
							append_items_2 = append_items_2+'<tr class="ui-sortable-handle tr-inner">'+
								'<td><span><i class="fa fa-list" aria-hidden="true"></i></span></td>'+
								'<td><input type="text" class="ingredient_qty" name="ingredient_qty'+curr_item_id+'[]" value="'+ingredient_qty[key]+'"></td>'+
								'<td class="db_item_name" name="dbname'+curr_item_id+'[]">'+data.item_name+'</td><input type="hidden" name="ingredient_name'+curr_item_id+'[]" value="'+data.item_name+'"/>'+
								'<td><input type="text" name="ingredient_cost'+curr_item_id+'[]" class="ingredient_cost" value="$'+data.default_stem_cost+'"></td>'+
								'<td>'+
									'$'+data.default_stem_cost*ingredient_qty[key]+''+
								'</td><input type="hidden" name="ingredient_price'+curr_item_id+'[]" value="$'+data.default_stem_cost*ingredient_qty[key]+'"/>'+
								'<td>TP<input type="hidden" name="ingredient_tax'+curr_item_id+'[]" value="'+"TP"+'"/></td>'+           
								'<td><span class="cross-td-inner"><i class="fa fa-times" aria-hidden="true"></i></span>'+
								'</td>'+
							' </tr>';
						// });
						total_ingredient_price = total_ingredient_price+data.default_stem_cost*ingredient_qty[key];
					});
					if(message.recipe.markup == ''){
						message.recipe.markup = 100;
					}
					append_items_3  = '</tbody><tfoot>'+
											'<tr>'+
												'<td colspan="2">Total Cost: </td>'+
												'<td>$'+total_ingredient_price+'</td><input type="hidden" class="total_ingredient_cost" name="total_ingred_cost'+curr_item_id+'" value="$'+total_ingredient_price+'"/>'+
												'<td>Markup %</td>'+
												'<td><input type="text" class="ingredient_markup" name="total_ingredient_markup'+curr_item_id+'" value="'+message.recipe.markup+'"></td>'+
												'<td>Total Price:</td>'+       
												'<td><input type="text" name="total_ingred_price'+curr_item_id+'" value="$'+Math.round(total_ingredient_price*(message.recipe.markup))/100+'" disabled="disable"></td>'+
											'</tr>'+
									'</tfoot>'+
									'</table>'+
										'</div><!-- dropdown-bx -->'+
										'</section>'+
												'<section class="dropdown-click-sec-inner p-2 mb-4">'+
													'<div class="heading px-3">'+
														'<span class="dropdown-click-inner">'+
															'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
														'</span>'+
														'<a href="javascript:void(0);" class="text-black">'+
															'<span>Description</span>'+
														'</a>'+
														'<span class="float-right">'+
															'<span class="delete-clk-inner">'+
																'<i class="fa fa-trash fa-fw"></i>'+
															'</span>'+
														'</span>'+
													'</div>'+
													'<div class="text">'+
														' <textarea class="form-control" rows="5" name="ingredient_description'+curr_item_id+'">'+message.recipe.description+'</textarea>'+
													' </div>'+
												'</section>'+
												'<section class="dropdown-click-sec-inner p-2 mb-4">'+
													'<div class="heading px-3">'+
														'<span class="dropdown-click-inner">'+
															'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
														'</span>'+
														'<a href="javascript:void(0);" class="text-black">'+
															' <span>Notes</span>'+
														'</a>'+
														'<span class="float-right">'+
															'<span class="delete-clk-inner">'+
																'<i class="fa fa-trash fa-fw"></i>'+
															'</span>'+
														'</span>'+
													'</div>'+
													'<div class="text">'+
														'<textarea class="form-control" rows="5" name="ingredient_notes'+curr_item_id+'">'+item_type+'</textarea>'+
													'</div>'+
												'</section>'+
											'</div><!-- col -->'+
										'</div><!-- row -->'+
									'</div>'+
								'</td>'+
							'</tr>';					
				}
				else{
					var append_items = '<tr class="none-display">'+
												'<td colspan="10" class="bg-greenish">'+
                                                    '<div class="text-left ingredients-section">'+
                                                        '<div class="row">'+
                                                            '<div class="col-lg-3">'+
                                                                '<div class="left-image-bx mb-5">'+
                                                                    '<h6 class="heading item-heading">'+recipe_name+'</h6>'+
                                                                    '<div class="img-dv">'+
                                                                        '<img src="'+curr_img+'" alt="Image">'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</div><!-- col -->'+
                                                            '<div class="col-lg-9">'+
                                                                '<section class="dropdown-click-sec-inner p-2 mb-4" id="ingredient-sec">'+
                                                                    '<div class="heading px-3">'+
                                                                        '<span class="dropdown-click-inner">'+
                                                                            '<i class="fa fa-caret-down" aria-hidden="true"></i>'+
                                                                        '</span>'+
                                                                        '<a href="javascript:void(0);" class="text-black">'+       
                                                                            '<span>Ingredients</span>'+
                                                                        '</a>'+
                                                                        '<span class="float-right">'+
                                                                            '<span class="delete-clk-inner">'+
                                                                                '<i class="fa fa-trash fa-fw"></i>'+
                                                                            '</span>'+
                                                                        '</span>'+
                                                                    '</div>'+
                                                                    '<div class="dropdown-bx">'+
                                                                        '<table class="table table-striped table-list m-0 text-center">'+
                                                                            '<thead>'+
                                                                                '<tr>'+                                 
                                                                                    '<th></th>'+
                                                                                    '<th>Qty</th>'+
                                                                                    '<th>Item Name</th>'+
                                                                                    '<th>Cost</th>'+
                                                                                    '<th>Price</th>'+           
                                                                                    '<th>Tax</th>'+
                                                                                    '<th></th>'+
                                                                                '</tr>'+
																			'</thead><tbody>';
					var append_items_2 = '';
					var append_items_3 = '';
					append_items_2 = append_items_2+'<tr class="ui-sortable-handle tr-inner">'+
						'<td><span><i class="fa fa-list" aria-hidden="true"></i></span></td>'+
						'<td><input type="text" class="ingredient_qty" name="ingredient_qty'+curr_item_id+'[]" value="1"></td>'+
						'<td class="db_item_name" name="dbname'+curr_item_id+'[]">'+recipe_name+'</td></td><input type="hidden" name="ingredient_name'+curr_item_id+'[]" value="'+recipe_name+'"/>'+
						'<td><input type="text" class="ingredient_cost" name="ingredient_cost'+curr_item_id+'[]" value="$'+message.recipe.price+'"></td>'+
						'<td>'+
							'$'+message.recipe.price+''+
						'</td><input type="hidden" name="ingredient_price'+curr_item_id+'[]" value="'+message.recipe.price+'"/>'+
						'<td>TP</td><input type="hidden" name="ingredient_tax'+curr_item_id+'[]" value="'+"TP"+'"/>'+
						'<td><span class="cross-td-inner"><i class="fa fa-times" aria-hidden="true"></i></span>'+
						'</td>'+
					' </tr>';
					append_items_3  = '</tbody><tfoot>'+
											'<tr>'+
												'<td colspan="2">Total Cost: </td>'+
												'<td>$'+0.00+'</td><input type="hidden" class="total_ingredient_cost" name="total_ingred_cost'+curr_item_id+'" value="$187.80"/>'+
												'<td>Markup %</td>'+
												'<td><input type="text" class="ingredient_markup" name="total_ingredient_markup'+curr_item_id+'"  value="$0.00'+message.recipe.markup+'"></td>'+
												'<td>Total Price:</td>'+       
												'<td><input type="text" name="total_ingred_price'+curr_item_id+'"  value="$187.80" disabled="disable"></td>'+
											'</tr>'+
									'</tfoot>'+
									'</table>'+
										'</div><!-- dropdown-bx -->'+
										'</section>'+
												'<section class="dropdown-click-sec-inner p-2 mb-4">'+
													'<div class="heading px-3">'+
														'<span class="dropdown-click-inner">'+
															'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
														'</span>'+
														'<a href="javascript:void(0);" class="text-black">'+
															'<span>Description</span>'+
														'</a>'+
														'<span class="float-right">'+
															'<span class="delete-clk-inner">'+
																'<i class="fa fa-trash fa-fw"></i>'+
															'</span>'+
														'</span>'+
													'</div>'+
													'<div class="text">'+
														' <textarea class="form-control" name="ingredient_description'+curr_item_id+'" rows="5">'+message.recipe.description+'</textarea>'+
													' </div>'+
												'</section>'+
												'<section class="dropdown-click-sec-inner p-2 mb-4">'+
													'<div class="heading px-3">'+
														'<span class="dropdown-click-inner">'+
															'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
														'</span>'+
														'<a href="javascript:void(0);" class="text-black">'+
															' <span>Notes</span>'+
														'</a>'+
														'<span class="float-right">'+
															'<span class="delete-clk-inner">'+
																'<i class="fa fa-trash fa-fw"></i>'+
															'</span>'+
														'</span>'+
													'</div>'+
													'<div class="text">'+
														'<textarea class="form-control" rows="5" name="ingredient_notes'+curr_item_id+'" rows="5">'+message.recipe.category+'</textarea>'+
													'</div>'+
												'</section>'+
											'</div><!-- col -->'+
										'</div><!-- row -->'+
									'</div>'+
								'</td>'+
							'</tr>';	
				}
				append_items = append_items+append_items_2+append_items_3;
				$(".initial_ingredient").remove();
				$(curr_design_meta).parents(".ui-sortable-handle").after(append_items);
			}
		});
	}
	else{
		$.ajax({
			url: "../auto_suggestions.php",
			type: "post",
			async: false,
			dataType: 'json',
			data: {
				type:'item',
				id:id
			},
			success: function(message) {
				console.log(message);
				$(curr_design_meta).parents(".ui-sortable-handle").next().find(".img-dv img").attr("src","/img/"+message.item.name).attr("alt",message.item.name);
				item_name = message.item.item_name;
				item_price = message.item.default_stem_cost;
				$(curr_design_meta).parents(".ui-sortable-handle:first").next().find(".item-heading").text(item_name);
				$(curr_design_meta).parents(".ui-sortable-handle").next().find(".img-dv img").attr("src","/img/"+message.item.name).attr("alt",message.item.name);
				// $(curr_design_meta).parent().next().find(".item-name-txt").text(item_name);
				// $(curr_design_meta).parent().next().find(".item-name-input").val(item_name);
				var qty = $(curr_design_meta).parent().next().next().next().find(".qty").val();
				$(curr_design_meta).parent().next().next().next().next().find(".price").val("$"+item_price);
				$(curr_design_meta).parent().next().next().next().next().next().text("$"+parseFloat(item_price*parseInt(qty)).toFixed(2));
				$(curr_design_meta).parent().next().next().next().next().next().next().val("$"+parseFloat(item_price*parseInt(qty)).toFixed(2));

				var append_items = '<tr class="none-display">'+
												'<td colspan="10" class="bg-greenish">'+
                                                    '<div class="text-left ingredients-section">'+
                                                        '<div class="row">'+
                                                            '<div class="col-lg-3">'+
                                                                '<div class="left-image-bx mb-5">'+
                                                                    '<h6 class="heading item-heading">'+item_name+'</h6>'+
                                                                    '<div class="img-dv">'+
                                                                        '<img src="'+curr_img+'" alt="Image">'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</div><!-- col -->'+
                                                            '<div class="col-lg-9">'+
                                                                '<section class="dropdown-click-sec-inner p-2 mb-4" id="ingredient-sec">'+
                                                                    '<div class="heading px-3">'+
                                                                        '<span class="dropdown-click-inner">'+
                                                                            '<i class="fa fa-caret-down" aria-hidden="true"></i>'+
                                                                        '</span>'+
                                                                        '<a href="javascript:void(0);" class="text-black">'+       
                                                                            '<span>Ingredients</span>'+
                                                                        '</a>'+
                                                                        '<span class="float-right">'+
                                                                            '<span class="delete-clk-inner">'+
                                                                                '<i class="fa fa-trash fa-fw"></i>'+
                                                                            '</span>'+
                                                                        '</span>'+
                                                                    '</div>'+
                                                                    '<div class="dropdown-bx">'+
                                                                        '<table class="table table-striped table-list m-0 text-center">'+
                                                                            '<thead>'+
                                                                                '<tr>'+                                 
                                                                                    '<th></th>'+
                                                                                    '<th>Qty</th>'+
                                                                                    '<th>Item Name</th>'+
                                                                                    '<th>Cost</th>'+
                                                                                    '<th>Price</th>'+           
                                                                                    '<th>Tax</th>'+
                                                                                    '<th></th>'+
                                                                                '</tr>'+
																			'</thead><tbody>';
					var append_items_2 = '';
					var append_items_3 = '';
					append_items_2 = append_items_2+'<tr class="ui-sortable-handle tr-inner">'+
						'<td><span><i class="fa fa-list" aria-hidden="true"></i></span></td>'+
						'<td><input type="text" class="ingredient_qty" name="ingredient_qty'+curr_item_id+'[]" value="1"></td>'+
						'<td class="db_item_name" name="dbname'+curr_item_id+'[]">'+item_name+'</td><input type="hidden" name="ingredient_name'+curr_item_id+'[]" value="'+item_name+'"/>'+
						'<td><input type="text" class="ingredient_cost" name="ingredient_cost'+curr_item_id+'[]" value="$'+message.item.default_stem_cost+'"></td>'+
						'<td>'+
							'$'+message.item.default_stem_cost+
						'</td><input type="hidden" name="ingredient_price'+curr_item_id+'[]" value="$'+message.item.default_stem_cost+'"/>'+
						'<td>TP</td><input type="hidden" name="ingredient_tax'+curr_item_id+'[]" value="'+"TP"+'"/>'+           
						'<td><span class="cross-td-inner"><i class="fa fa-times" aria-hidden="true"></i></span>'+
						'</td>'+
					' </tr>';
					append_items_3  = '</tbody><tfoot>'+
											'<tr>'+
												'<td colspan="2">Total Cost: </td>'+
												'<td>$'+message.item.default_stem_cost+'</td><input type="hidden" class="total_ingredient_cost" name="total_ingred_cost'+curr_item_id+'" value="$'+message.item.default_stem_cost+'">'+
												'<td>Markup %</td>'+
												'<td><input type="text" class="ingredient_markup" name="total_ingredient_markup'+curr_item_id+'" value="'+1+'"></td>'+
												'<td>Total Price:</td>'+       
												'<td><input type="text" name="total_ingred_price'+curr_item_id+'" value="$'+message.item.default_stem_cost+'" disabled="disable"></td>'+
											'</tr>'+
									'</tfoot>'+
									'</table>'+
										'</div><!-- dropdown-bx -->'+
										'</section>'+
												'<section class="dropdown-click-sec-inner p-2 mb-4">'+
													'<div class="heading px-3">'+
														'<span class="dropdown-click-inner">'+
															'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
														'</span>'+
														'<a href="javascript:void(0);" class="text-black">'+
															'<span>Description</span>'+
														'</a>'+
														'<span class="float-right">'+
															'<span class="delete-clk-inner">'+
																'<i class="fa fa-trash fa-fw"></i>'+
															'</span>'+
														'</span>'+
													'</div>'+
													'<div class="text">'+
														' <textarea class="form-control" name="ingredient_description'+curr_item_id+'" rows="5">'+message.item.description+'</textarea>'+
													' </div>'+
												'</section>'+
												'<section class="dropdown-click-sec-inner p-2 mb-4">'+
													'<div class="heading px-3">'+
														'<span class="dropdown-click-inner">'+
															'<i class="fa fa-caret-down" aria-hidden="true"></i>'+
														'</span>'+
														'<a href="javascript:void(0);" class="text-black">'+
															' <span>Notes</span>'+
														'</a>'+
														'<span class="float-right">'+
															'<span class="delete-clk-inner">'+
																'<i class="fa fa-trash fa-fw"></i>'+
															'</span>'+
														'</span>'+
													'</div>'+
													'<div class="text">'+
														'<textarea class="form-control" rows="5" name="ingredient_notes'+curr_item_id+'" rows="5">'+message.item.item_type+'</textarea>'+
													'</div>'+
												'</section>'+
											'</div><!-- col -->'+
										'</div><!-- row -->'+
									'</div>'+
								'</td>'+
							'</tr>';	
				append_items = append_items+append_items_2+append_items_3;
				$(".initial_ingredient").remove();
				$(curr_design_meta).parents(".ui-sortable-handle").after(append_items);
			}
		});
	}
	$(".close").click();
});

//Update Item price 
$(document).on("keyup",".qty",function(e){
	var curr = $(this);
	var curr_qty = parseInt(curr.val());
	var curr_design_meta = curr.parent().parent().children(':nth-child(4)').find(".fa-plus");
	if(curr_design_meta.length == 1){
		var price = $(curr).parent().next().find(".price").val();
		price = parseInt(price.replace("$",""));
		$(curr).parent().next().next().text("$"+curr_qty*price);
		$(curr).parent().next().next().next().val("$"+curr_qty*price);
	}
	else{
		var price = $(curr).parent().next().find(".price").val();
		price = parseInt(price.replace("$",""));
		$(curr).parent().next().next().text("$"+curr_qty*price);
		$(curr).parent().next().next().next().val("$"+curr_qty*price);
	}
});

$(document).on("keyup",".price",function(e){
	var curr = $(this);
	var curr_price = parseInt(curr.val().replace("$",""));
	if(isNaN(curr_price)){
		curr.val("$"+0.00);
	}
	else{
		curr.val("$"+curr_price);
	}
	var curr_design_meta = curr.parent().parent().children(':nth-child(4)').find(".fa-plus");
	if(curr_design_meta.length == 1){
		var qty = $(curr).parent().prev().find(".qty").val();
		$(curr).parent().next().text("$"+curr_price*qty);
		$(curr).parent().next().next().val("$"+curr_price*qty);
	}
	else{
		var qty = $(curr).parent().prev().find(".qty").val();
		$(curr).parent().next().text("$"+curr_price*qty);
		$(curr).parent().next().next().val("$"+curr_price*qty);
	}
});

// calculate ingredient
$(document).on("keyup",".ingredient_qty",function(e){
	var curr = $(this);
	curr_price = parseInt(curr.parent().next().next().next().next().text().replace("$",""));
	total_price_obj = curr.parent().parent().parent().next().children().find(".total_ingredient_cost");
	total_price_val = parseInt(total_price_obj.val().replace("$",""));
	var curr_qty = parseInt(curr.val());
	var price = $(curr).parent().next().next().next().find(".ingredient_cost").val();
	price = parseInt(price.replace("$",""));
	$(curr).parent().next().next().next().next().text("$"+curr_qty*price);
	var total_ingred_cost_input = $(curr).parent().next().next().next().next().next();
	total_ingred_cost_input.val("$"+curr_qty*price);
	
	curr_name = total_ingred_cost_input.attr("name");
	curr_val = $("input[name='"+curr_name+"']").map(function(){return $(this).val();}).get();;
	sum = 0;
	$.each(curr_val,function(){sum+=parseFloat(this.replace("$","")) || 0;});
	total_price_obj.val("$"+sum);
	total_price_obj.prev().text("$"+sum);
	total_price_obj.next().next().next().next().children().val("$"+Math.round(sum*$(curr).parent().parent().parent().next().children().find(".ingredient_markup").val())/100);

});

$(document).on("keyup",".ingredient_cost",function(e){
	var curr = $(this);
	curr_price1 = parseInt(curr.parent().next().text().replace("$",""));
	total_price_obj = curr.parent().parent().parent().next().children().find(".total_ingredient_cost");
	total_price_val = parseInt(total_price_obj.val().replace("$",""));
	var curr_price = parseInt(curr.val().replace("$",""));
	if(isNaN(curr_price)){
		curr.val("$"+0.00);
	}
	else{
		curr.val("$"+curr_price);
	}
	var curr_design_meta = curr.parent().parent().children(':nth-child(4)').find(".fa-plus");
	if(curr_design_meta.length == 1){
		var qty = $(curr).parent().prev().prev().prev().find(".ingredient_qty").val();
		$(curr).parent().next().text("$"+curr_price*qty);
		$(curr).parent().next().next().val("$"+curr_price*qty);

		curr_name = $(curr).parent().next().next().attr("name");
		curr_val = $("input[name='"+curr_name+"']").map(function(){return $(this).val();}).get();;
		sum = 0;
		$.each(curr_val,function(){sum+=parseFloat(this.replace("$","")) || 0;});
		total_price_obj.val("$"+sum);
		total_price_obj.prev().text("$"+sum);
		total_price_obj.next().next().next().next().children().val("$"+Math.round(sum*$(curr).parent().parent().parent().next().children().find(".ingredient_markup").val())/100);
		
	}
	else{
		var qty = $(curr).parent().prev().prev().prev().find(".ingredient_qty").val();
		$(curr).parent().next().text("$"+curr_price*qty);
		$(curr).parent().next().next().val("$"+curr_price*qty);
		
		curr_name = $(curr).parent().next().next().attr("name");
		curr_val = $("input[name='"+curr_name+"']").map(function(){return $(this).val();}).get();;
		sum = 0;
		$.each(curr_val,function(){sum+=parseFloat(this.replace("$","")) || 0;});
		total_price_obj.val("$"+sum);
		total_price_obj.prev().text("$"+sum);
		total_price_obj.next().next().next().next().children().val("$"+Math.round(sum*$(curr).parent().parent().parent().next().children().find(".ingredient_markup").val())/100);
	}
});

// Update ingredient total on markup change
$(document).on("keyup",".ingredient_markup",function(e){
	var curr = $(this);
	var markup = parseInt(curr.val());
	var curr_ingred_total = parseInt($(curr).parent().prev().prev().prev().text().replace("$",""));
	if(isNaN(markup)){
		curr.val(0);
	}
	$(curr).parent().next().next().find("input").val("$"+Math.round(curr_ingred_total*(markup))/100);
});


// Load Default Template
$(document).on("click","#load-default-temp",function(e){
	window.open('/worksheet.php',"_self");
});

});// document close



