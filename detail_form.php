<?php
$page = "Event";
if(!isset($_SESSION)) 
{ 
    session_start(); 
}  
if(empty($_SESSION["username"])){
    header("location:users/login.php");
    exit();
}
else{
    $username = $_SESSION["username"];
    $tempid = isset($_GET['ID'])?$_GET['ID']:$_SESSION["ID"];
    $_SESSION["ID"] = $tempid;
    $id = $tempid;
}
include('db.php'); 

?>

<?php
include('header.php');

?> <!--header added-->

<section class="container-fluid mt-3">
    <?php 
    $sql = "SELECT * FROM event_questionare WHERE `event_id`='$id'";
    $result = $con->query($sql);
    if ($result->num_rows > 0) {
        // print_r($result); exit();
        while($row = $result->fetch_assoc()) {
       $_SESSION["groom_bride"] =  $row['bride_name']." & ".$row['groom_name'];
            ?>
            <div class="row">
                <div class="col-sm-4">
                    <div class="bg-white pb-2">
                        <h4 class="text-white p-3" style="background-color:rgb(23,162,184);">Event Details</h4>

                        <form method="POST" action="sub-edit-questionare.php">

                            <div class="mt-3 ml-3 mr-3">
                            <?php /*<div class="md-form">
                                    <label for="form1">Event Name</label>
                                    <input type="text" id="form1" class="form-control" value="<?php echo $row['event_name']; ?>">
                                    </div> */?>

                                    <div class="mt-3">
                                        <div class="md-form">
                                            <select name="status" class="custom-select corner_set">
                                                <option selected><?php echo $row['status']; ?></option>
                                                <option value="1st Proposal" name="1st_proposal">1st Proposal</option>
                                                <option value="Follow Up Needed" name="follow_up_needed">Follow Up Needed</option>
                                                <option value="Proposal Needed" name="proposal_needed">Proposal Needed</option>
                                                <option value="Appointment" name="appointment">Appointment</option>
                                                <option value="Booked Event" name="booked_event">Booked Event</option>
                                                <option value="Closed Event" name="closed_event">Closed Event</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="mt-3">
                                                <div class="md-form">
                                                    <label for="form1">Bride Name</label>
                                                    <input type="text" id="form1" class="form-control corner_set" value="<?php echo $row['bride_name']; ?>"
                                                    name="bride_name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="mt-3">
                                                <div class="md-form">
                                                    <label for="form1">Groom Name</label>
                                                    <input type="text" id="form1" class="form-control corner_set" value="<?php echo $row['groom_name']; ?>"
                                                    name="groom_name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="mt-3">
                                                <div class="md-form">
                                                    <label for="form1">Referred By</label>
                                                    <input type="text" id="form1" name="referred_by" class="form-control corner_set"
                                                    value="<?php echo $row['referred_by']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="mt-3">
                                                <div class="md-form">
                                                    <label for="form1">Event Date</label>
                                                    <input type="date" id="form1" name="event_date" class="form-control corner_set"
                                                    value="<?php echo $row['event_date']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="mt-3">
                                                <div class="md-form">
                                                    <label for="form1">Number of Guests</label>
                                                    <input type="number" id="form1" name="number_guests" class="form-control corner_set"
                                                    value="<?php echo $row['number_guests']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="mt-3">
                                                <div class="md-form">
                                                    <label for="form1">Contact Email</label>
                                                    <input type="email" id="form1" class="form-control corner_set" value="<?php echo $row['contact_email']; ?>"
                                                    name="contact_email">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="mt-3">
                                                <div class="md-form">
                                                    <label for="form1">Contact Phone 1</label>
                                                    <input type="text" id="form1" class="form-control corner_set" value="<?php echo $row['contact_number_first']; ?>"
                                                    name="contact_number_first">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="mt-3">
                                                <div class="md-form">
                                                    <label for="form1">Contact Phone 2</label>
                                                    <input type="text" id="form1" class="form-control corner_set" value="<?php echo $row['contact_number_second']; ?>"
                                                    name="contact_number_second">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mt-3">
                                        <label for="comment">Event Notes</label>
                                        <textarea class="form-control corner_set" name="event_notes_description" rows="5"
                                        id="comment"><?php echo $row['event_notes']; ?></textarea>
                                    </div>
                                    <div class="mt-3">
                                        <div class="md-form">
                                            <label for="form1">Event Budget $</label>
                                            <input type="number" id="form1" class="form-control corner_set"
                                            value="<?php echo $row['event_budget']; ?>" name="event_budget">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="bg-white pb-5">
                                <h4 class="text-white p-3" style="background-color:rgb(23,162,184);">Event Schedule</h4>
                                <p class="border-bottom ml-3 mr-3">Times & Locations</p>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="md-form">
                                                <label for="form1">Ceremony Start Time</label>
                                                <input type="time" id="form1" name="ceremony_start_time" class="form-control corner_set"
                                                value="<?php echo $row['ceremony_start_time']; ?>">
                                            </div>
                                        </div>
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="md-form">
                                                <label for="form1">Cocktails Start Time</label>
                                                <input type="time" id="form1" name="cocktails_start_time" class="form-control corner_set"
                                                value="<?php echo $row['cocktails_start_time']; ?>">
                                            </div>
                                        </div>
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="md-form">
                                                <label for="form1">Reception Start Time</label>
                                                <input type="time" id="form1" name="reception_start_time" class="form-control corner_set"
                                                value="<?php echo $row['reception_start_time']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="md-form">
                                                <label for="form1">Ceremony Location</label>
                                                <input type="text" id="form1" name="ceremony_location" class="form-control corner_set"
                                                value="<?php echo $row['ceremony_location']; ?>">
                                            </div>
                                        </div>
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="md-form">
                                                <label for="form1">Cocktails Location</label>
                                                <input type="text" id="form1" name="cocktails_location" class="form-control corner_set"
                                                value="<?php echo $row['cocktails_location']; ?>">
                                            </div>
                                        </div>
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="md-form">
                                                <label for="form1">Reception Location</label>
                                                <input type="text" id="form1" name="reception_location" class="form-control corner_set"
                                                value="<?php echo $row['reception_location']; ?>">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <p class="border-bottom ml-3 mr-3 mt-5">Important Times</p>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="md-form">
                                                <label for="form1">Setup Available</label>
                                                <input type="time" id="form1" name="setup_available" class="form-control corner_set"
                                                value="<?php echo $row['setup_available']; ?>">
                                            </div>
                                        </div>
                        <!-- <div class="mt-3 ml-3 mr-3">
                                    <div class="md-form">
                                        <label for="form1">Company Arrival</label>
                                        <input type="time" id="form1" class="form-control corner_set">
                                    </div>
                                </div> -->
                            </div>
                            <div class="col-sm-4">
                        <!-- <div class="mt-3 ml-3 mr-3">
                                    <div class="md-form">
                                        <label for="form1">Photographer Start</label>
                                        <input type="time" id="form1" class="form-control corner_set" value="">
                                    </div>
                                </div> -->
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="md-form">
                                        <label for="form1">Event End</label>
                                        <input type="time" id="form1" name="event_end" class="form-control corner_set"
                                        value="<?php echo $row['event_end']; ?>">
                                    </div>
                                    <div>
                                        <input type="hidden" name="id" value="<?php echo $row['event_id']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                        <!-- <div class="mt-3 ml-3 mr-3">
                                    <div class="md-form">
                                        <label for="form1">Strike Begin</label>
                                        <input type="time" id="form1" class="form-control corner_set" value="">
                                    </div>
                                </div>
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="md-form">
                                        <label for="form1">Strike Concludes</label>
                                        <input type="time" id="form1" class="form-control corner_set" value="">
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="bg-white pb-5 mt-4">
                        <h4 class="text-white p-3" style="background-color:rgb(23,162,184);">Delivery Info</h4>
                        <p class="border-bottom ml-3 mr-3">The Bouquet</p>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="md-form">
                                        <label for="form1">Bouquet Delivery</label>
                                        <input type="time" id="form1" name="bouquet_deliverey_time" class="form-control corner_set"
                                        value="<?php echo $row['bouquet_deliverey_time']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="md-form">
                                        <label for="form1">Bouquet Delivery Location</label>
                                        <input type="text" id="form1" name="bouquet_delivery_location" class="form-control corner_set"
                                        value="<?php echo $row['bouquet_delivery_location']; ?>">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="form-group mt-3">
                                        <label for="comment">Bridal bouquet desire type (description)</label>
                                        <textarea class="form-control corner_set" name="bridal_bouquet_desire_type" rows="5"
                                        id="comment"><?php echo $row['bridal_bouquet_desire_type']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="form-group mt-3">
                                        <label for="comment">Bridesmaids bouquets desire type (description)</label>
                                        <textarea class="form-control corner_set" name="bridalmaids_bouquet_desire_type" rows="5"
                                        id="comment"><?php echo $row['bridalmaids_bouquet_desire_type']; ?></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="mt-3 mx-3">
                            <div class="md-form">
                                <label for="form1">How many Bridesmaids bouquets</label>
                                <input type="text" id="form1" class="form-control corner_set" value="<?php echo $row['bridals_maids_bouquets_number']; ?>"
                                name="bridals_maids_bouquets_number">
                            </div>
                        </div>
                        <div class="mt-3 ml-3 mr-3">
                            <div class="form-group mt-3">
                                <label for="comment">Groomsmen boutonnières desire type (description)</label>
                                <textarea class="form-control corner_set" rows="5" name="groomsmen_boutnaries_desire_type"><?php echo $row['groomsmen_boutnaries_desire_type']; ?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="form-group mt-3">
                                        <label for="comment">How many Groomsmen boutonnieres</label>
                                        <input type="text" id="form1" class="form-control corner_set" value="<?php echo $row['groomsmen_boutnaries']; ?>"
                                        name="groomsmen_boutnaries">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="form-group mt-3">
                                        <label for="comment">How Many tables?</label>
                                        <input type="text" id="form1" class="form-control corner_set" value="<?php echo $row['number_tables']; ?>"
                                        name="number_tables">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="mt-3 ml-3 mr-3">
                            <div class="form-group mt-3">
                                <label for="comment">Tables type(round/rectangular)</label>
                                <input type="text" id="form1" class="form-control corner_set" value="<?php echo $row['type_tables']; ?>"
                                name="type_tables">
                            </div>
                        </div>
                        <div class="mt-3 ml-3 mr-3">
                            <div class="form-group mt-3">
                                <label for="comment">Would you like an arch with ﬂorals and greenery? desire type (description)</label>
                                <textarea class="form-control corner_set" rows="5" name="arch_florals_greenery" id="comment"><?php echo $row['arch_florals_greenery']; ?></textarea>
                            </div>
                        </div>
                        <div class="mt-3 ml-3 mr-3">
                            <div class="form-group mt-3">
                                <label for="comment">Arch type</label>
                                <input type="text" id="form1" class="form-control corner_set" value="<?php echo $row['arch_type']; ?>"
                                name="arch_type">
                                <p>Will venue provide arch? or you want it rented from us?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    ?>
</section>
<section class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="text-center pb-4" style="border-top:solid 2px rgb(220,220,220);">
                <button class="btn btn-info pl-4 pr-4 corner_set btn_color" type="submit">
                    <i class="fa fa-save fa-fw"></i>
                    Save
                </button>
            </div>

        </div>
    </div>
</section>
</form>

<script src="js/jquery.auto-complete.min.js"></script>


</body>

</html>