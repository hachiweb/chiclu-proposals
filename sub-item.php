<?php
include 'db.php';
if (isset($_POST['img_upload'])) {
  $name                    = $_FILES['file']['name'];
  $recipe_name             = $_POST['recipe_name'];
  $description             = $_POST['description'];
  $item_name               = $_POST['item_name'];
  $item_type               = $_POST['item_type'];
  $default_stem_cost       = $_POST['default_stem_cost'];
  $stems_per_bunch         = $_POST['stems_per_bunch'];
  $tempcolors              = $_POST['colors'];
  $colors                  = implode(", ", $tempcolors);
  $tempmonths              = $_POST['months'];
  $months                  = implode(", ", $tempmonths);
  $source                  = $_POST['source'];
  $target_dir              = "img/";
  $target_file             = $target_dir . basename($_FILES["file"]["name"]);
  // Select file type
  $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
  // Valid file extensions
  $extensions_arr = array("jpg", "jpeg", "png", "gif", "webp");
  // Check extension
  if (in_array($imageFileType, $extensions_arr)) {
    // Insert record
    $query = "INSERT INTO `item_gallery` (`name`, `item_name`, `item_type`, `default_stem_cost`, `stems_per_bunch`, `colors`, `months`, `recipe_name`, `description`, `source`) VALUE ('" . $name . "', '$item_name', '$item_type', '$default_stem_cost', '$stems_per_bunch', '$colors', '$months', '$recipe_name', '$description', '$source')";
    mysqli_query($con, $query);
    // Upload file
    move_uploaded_file($_FILES['file']['tmp_name'], $target_dir . $name);
  }
  if (isset($_POST["hide_header"])) {
    header("location: item-gallery.php?hide_header=1");
  } else {
    header("location: item-gallery.php");
  }
  
  
}
?>