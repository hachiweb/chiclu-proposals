<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}  
    if(empty($_SESSION["username"])){
        header("location:users/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    }
    include('db.php'); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Proposals ChicLu</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body class="bg-gray">
  <div class="wrapper">
    <header class="main-header bg-white">
      <nav class="navbar navbar-expand-sm top-bar-nav">
        <ul class="navbar-nav flex-wrap">
          <li><a href="index.php">Home</a></li>
          <li class="dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
          Details
            </a>
            <div class="dropdown-menu">
              <a href="questionare.php">Questionare Form</a>
              
            </div>
          </li>
          <li><a href="#">Worksheet</a></li>
          <li class="dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
              Resources
            </a>
            <div class="dropdown-menu">
              <a href="inspiration-gallery.php">Inspiration Gallery</a>
              <a href="recipe-gallery.php">Recipe Gallery</a>
              <a href="item-gallery.php">Item Gallery</a>
            </div>
          </li>
          <li><a href="#">Financials</a></li>
          <li><a href="#">Proposal</a></li>
          <li><a href="#">Documents</a></li>
          <li><a href="#">Costs</a></li>
          <li><a href="users/logout.php">Logout</a></li>
        </ul>        
      </nav>
    </header>

    <main class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="right-sec py-3">

              <div class="p-3 mb-2 bg-info text-white func-dv">
                <div class="row align-items-center">
                  <div class="col-sm-4">
                    <div class="btn-dv">
                      <button id="filter-btn" type="button" class="btn btn-outline-light">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span>Filter / Search</span>
                      </button>
                    </div>
                  </div><!-- col -->

                  <div class="col-sm-4">
                    <h5 class="m-sm-0 font-weight-normal text-sm-center">Showing 856 items.</h5>
                  </div><!-- col -->

                  <div class="col-sm-4 text-sm-right">
                    <div class="btn-dv">
                      <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#newItem">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>New Recipe</span>
                      </button>

                      <button id="table-image-btn" type="button" class="btn btn-outline-light ml-2">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>Table</span>
                      </button>
                    </div>
                  </div><!-- col -->
                </div><!-- row -->
              </div>

              <div id="filter-dv" class="p-3 mb-2 bg-white filter-bx">
              <form action="search.php" method="post" >
                <div class="row">
                  <div class="col-md-4">
                    <div class="entry-txt">
                      <h6 class="heading">Colors</h6>
                      <div class="text mb-4 check-bx">
                        <?php $colorslist = array('White', 'Green', 'Red', 'Purple', 'Blue', 'Magenta','Black','Orange','Yellow','Brown','Bush','Valvet');
                        for($i=0; $i<sizeof($colorslist); $i++){ ?>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" name="colors[]" class="form-check-input" value="<?php echo $colorslist[$i]; ?>"><?php echo $colorslist[$i]; ?>
                            </label>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->

                  <div class="col-md-4">
                    <div class="entry-txt">
                      <h6 class="heading">Months</h6>
                      <div class="text mb-4 check-bx">
                        <?php $monthslist = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                        for($i=1; $i<=sizeof($monthslist); $i++){ ?>
                         <div class="form-check">
                          <label class="form-check-label">
                            <input type="checkbox" name="months[]" class="form-check-input month-filter" value="<?php echo $monthslist[$i] ?>"><?php echo $monthslist[$i] ?>
                          </label>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div><!-- col -->

                <div class="col-md-4">
                  <div class="entry-txt">
                    <h6 class="heading">Categories</h6>
                    <div class="text mb-4 check-bx">
                      <?php
                      $categories = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                        for($i=0; $i<sizeof($categories); $i++){ ?>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" name="categories[]" class="form-check-input" value="<?php echo $categories[$i] ?>"><?php echo $categories[$i] ?>
                            </label>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->
                </div><!-- row -->

                  <div class="filter-form-bx">
                    <div class="row mt-4 align-items-end">
                      <div class="col-md-3">
                       <div class="form-group">
                        <label for="uname">Search By</label>
                        <select name="search_by" id="search_by" class="form-control">
                          <option value="Recipe">Recipe</option>
                          <option value="Item">Item</option>
                        </select>
                      </div>
                    </div><!-- col -->

                    <div class="col-md-6">
                     <div class="form-group">
                      <label for="uname">Search Term:</label>
                      <input name="search_term" type="text" class="form-control">
                    </div>
                  </div><!-- col -->

                  <div class="col-md-3">
                   <div class="form-group">
                    <button class="btn btn-info" type="submit">Search/Filter</button>
                    <button class="btn btn-info" type="Reset">Clear</button>
                  </div>
                </div><!-- col -->
              </div><!-- row -->
            </div>
            </form>
          </div>

          <!-- Image Click Modal -->
          <div class="modal fade custom-modal" id="newItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header bg-info text-white">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Add Recipe</h5>
                  <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-lg-3">
                    <form action="sub-recipe.php" method="post" enctype="multipart/form-data">
                      <div class="image-dv  mb-3">
                        <h6 class="heading">Recipe Image</h6>
                        <div class="img">
                          <img src="img/image.jpg" alt="Image">
                        </div>
                      </div>
                      <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                          </div>
                    </div><!-- col -->
                    <div class="col-lg-9">
                      <div class="entry-txt mb-4">
                        <div class="row">
                          <div class="col-md-8">
                            <h6 class="heading">Recipe Name</h6>
                            <div class="text mb-4">
                              <input type="text" class="form-control" name="recipe_name" >
                            </div>

                            <h6 class="heading">Description</h6>
                            <div class="text mb-4">
                              <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                            </div>
                            <div>
                              <input type="hidden" name="source" value="Recipe">
                            </div>

                            <div class="mt-5">
                              <button class="btn btn-info" type="submit" value="Upload" name="img_upload">Add Ingredient</button>
                            </div>
                          </div><!-- col -->                       

                          <div class="col-md-4">
                            <h6 class="heading">Rates</h6>
                            <div class="text mb-4 after-edit">
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Cost</span>
                                <span>$0.00</span>
                              </div>
                              <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Markup</span>
                                <span><input type="text" class="form-control" name="markup" ></span>
                              </div>
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Price</span>
                                <span><input type="number" class="form-control" name="price" ></span>
                              </div>
                            </div>

                            <h6 class="heading">Category</h6>
                            <div class="text mb-4">
                              <select name="category" id="category" class="form-control">
                              <?php
                              $category1 = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                              for($i=0; $i<sizeof($category1); $i++){ ?>
                                  <option value="<?php echo $category1[$i] ?>"><?php echo $category1[$i] ?></option>
                                  <?php } ?>
                              </select>
                            </div>

                            <h6 class="heading">Styles</h6>
                            <div class="text mb-4 check-bx">
                            <?php
                              $style1 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                              for($i=0; $i<sizeof($style1); $i++){ ?>
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="checkbox" name="styles" class="form-check-input" value="<?php echo $style1[$i] ?>"><?php echo $style1[$i] ?>
                                </label>
                              </div>
                              <?php } ?>
                            </div>

                            <h6 class="heading">Show in Gallery</h6>
                            <div class="text mb-4">
                              <select name="is_active" id="is_active" class="form-control">
                                <option value="Show">Show</option>
                                <option value="Hide">Hide</option>
                              </select>
                            </div>
                          </div><!-- col -->
                        </div><!-- row -->
                      </div>
                      </form>
                    </div><!-- col -->
                    
                  </div><!-- row -->
                </div>
              </div>
            </div>
          </div><!-- modal -->

          <?php
          
          $a = $_GET['colors'];
          $c = $_GET['categories'];
        //   $d = $_POST['search_by'];
          $months = $_GET['months'];
          $e = $_GET['search_term'];
          $get_image = "SELECT * FROM `item_gallery` WHERE colors IN '$a' AND category IN '$c' AND months IN '$months' OR name LIKE '%$e%' OR item_name LIKE '%$e%' OR item_type LIKE '%$e%' OR recipe_name LIKE '%$e%'";  
          $result = mysqli_query($con, $get_image); ?>
          <div id="image-grid-bx" class="image-grid-bx">
            <div class="row row-5">
              <?php while($img = mysqli_fetch_assoc($result)){ ?>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6 col" data-toggle="modal" data-target="#model<?php echo $img['id']; ?>">
                  <div class="image-grid-dv d-flex flex-column justify-content-end">
                    <div class="img">
                      <img src="<?php echo "img/".$img['name']; ?>" alt="Image">
                    </div><!-- img -->
                    <h6 class="title m-0 p-3"><?php echo $img['name']; ?></h6>
                  </div>
                </div><!-- col -->
                <?php } ?>

              </div><!-- row -->
            </div><!-- image-grid-bx -->

          <?php
          $tblsql = "SELECT * FROM `gallery` WHERE source='Recipe'";  
          $tblresult = mysqli_query($con, $tblsql); ?>
          <div id="table-list-bx" class="table-list-bx bg-white d-none">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              <?php while($tbl = mysqli_fetch_assoc($tblresult)){ ?>
                  <tr>
                    <td data-toggle="modal" data-target="#model1">
                      <div class="img max-50">
                        <img class="img-thumbnail" src="<?php echo "img/".$tbl['name']; ?>" alt="Image">
                      </div>
                    </td>
                    <td><?php echo $tbl['recipe_name']; ?></td>                
                    <td class="img max-50"><?php echo $tbl['description']; ?></td>                
                    <td><?php echo $tbl['price']; ?></td>                
                    <td>
                      <span class="delete"><i class="fa fa-times" onclick="imgDelete(<?php echo $tbl['id']; ?>)" aria-hidden="true"></i></span>
                    </td>                
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div><!-- table-list-bx -->


          <!-- Image Click Modal -->
            <?php 
            $resource_detail_sql =  "SELECT * FROM `gallery` WHERE source='Recipe'";  
            $resource_detail_res = mysqli_query($con, $resource_detail_sql);
            while($row = mysqli_fetch_assoc($resource_detail_res)){ ?>
              <div class="modal fade custom-modal" id="model<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-info text-white flex-wrap">
                      <h5 class="modal-title" id="exampleModalCenterTitle"><?php echo $row['recipe_name']; ?></h5>

                      <button id="edit-btn<?php echo $row['id']; ?>" type="button" class="btn btn-outline-light ml-sm-auto editbtn">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>Edit</span>
                      </button>
                      <button type="button" class="btn btn-outline-light ml-2" onclick="imgDelete(<?php echo $row['id']; ?>)">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span>Delete</span>
                      </button>
                      <script>
                        function imgDelete(id) {
                          if(confirm("Are you sure you want to Delete?")){
                            del="delete.php?id="+id+"&source=Recipe";
                            window.location.href = del;
                          }
                        
                        }
                        </script>
                      <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div id="before-edit" class="row before-edit">
                        <div class="col-lg-4 col-md-3">
                          <div class="image-dv mb-5">
                            <h6 class="heading">Recipe Image</h6>
                            <div class="img">
                              <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                            </div>
                          </div>

                          <div class="image-dv image-dv-thumb">
                            <h6 class="heading">Ingredient Images</h6>
                            <div class="row row-5">
                            <?php
                            $id =  $row['id'];
                            $match = $row['recipe_name'];
                            $ingsql = "SELECT * FROM `item_gallery` WHERE recipe_name='$match'";
                            $ingresult = mysqli_query($con, $ingsql);
                            while($ing = mysqli_fetch_assoc($ingresult)){ ?>
                              <div class="col-4">
                                <div class="img"><img src="<?php echo "img/".$ing['name']; ?>" alt="Image"></div>
                              </div>
                            <?php } ?>
                            </div>
                          </div>
                        </div><!-- col -->
                        <div class="col-lg-8 col-md-9">
                          <div class="entry-txt mb-4">
                            <div class="row">
                              <div class="col-md-7">
                                <h6 class="heading">Description</h6>
                                <div class="text mih-200 mb-4">
                                  <p><?php echo $row['description']; ?></p>
                                </div>

                                <h6 class="heading">Ingredients</h6>
                                <div class="text">
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <span>Item Name</span>
                                    <span>Qty.</span>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <span>Avg Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <span>Avg Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                </div>
                              </div><!-- col -->                       

                              <div class="col-md-5">
                                <h6 class="heading">Rates</h6>
                                <div class="text mb-4">
                                  <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Markup</span>
                                    <span><?php echo $row['markup']; ?></span>
                                  </div>
                                  <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Price</span>
                                    <span><?php echo $row['price']; ?></span>
                                  </div>
                                </div>

                                <h6 class="heading">Category</h6>
                                <div class="text mb-4"><?php echo $row['category']; ?></div>

                                <h6 class="heading">Styles</h6>
                                <div class="text mb-4"><?php echo $row['styles']; ?></div>

                                <h6 class="heading">Colors</h6>
                                <div class="text mb-4">
                                  Green, Silver, Brown, Neutral, White, Pink, Orange
                                </div>

                                <h6 class="heading">Months</h6>
                                <div class="text mb-4">
                                  May, June, July, August, September, October, November, December
                                </div>
                              </div><!-- col -->
                            </div><!-- row -->
                          </div>

                        </div><!-- col -->
                      </div><!-- row -->

                      <div id="after-edit" class="row after-edit">
                        <div class="col-lg-4 col-md-3">
                        <form action="sub-edit.php" method="post" enctype="multipart/form-data">
                          <div class="image-dv mb-5">
                            <h6 class="heading">Recipe Image</h6>
                            <div class="img">
                              <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                            </div>
                            <div><input type="hidden" name="source" value="Recipe"></div>
                          </div>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                          </div>

                          <div class="image-dv">
                            <h6 class="heading">Ingredient Images</h6>
                            <div class="row row-5">
                            <?php
                            $id =  $row['id'];
                            $ingsql2 = "SELECT * FROM `gallery` WHERE source='Recipe' AND parent_id='$id'";
                            $ingresult2 = mysqli_query($con, $ingsql2);
                            while($ing2 = mysqli_fetch_assoc($ingresult2)){ ?>
                              <div class="col-4">
                                <img src="<?php echo "img/".$ing2['name']; ?>" alt="Image">
                              </div>
                            <?php } ?>
                            </div>
                          </div>
                        </div><!-- col -->
                        
                        <div class="col-lg-8 col-md-9">
                          <div class="entry-txt mb-4">
                            <div class="row">
                              <div class="col-md-8">
                                <h6 class="heading">Recipe Name</h6>
                                <div class="text mb-4">
                                  <input type="text" class="form-control" value="<?php echo $row['recipe_name']; ?>" name="recipe_name">
                                </div>
                                <div><input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>"></div>

                                <h6 class="heading">Description</h6>
                                <div class="text mb-4">
                                  <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                                </div>

                                <div class="text mb-5 after-edit">
                                  <div class="alert flex-wrap flex-row alert-dark d-flex justify-content-between mb-2">
                                    <div>Ingredients</div>
                                    <div>Show Deleted</div>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <div>Item Name</div>
                                    <div>Qty.</div>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_one']; ?>" name="avg_cost_one" id="avg_cost_one"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_two']; ?>" name="avg_cost_two" id="avg_cost_two"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_three']; ?>" name="avg_cost_three" id="avg_cost_three"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                </div>

                                <div class="my-5">
                                  <button class="btn btn-info" type="submit" value="Upload" name="img_upload">Add Ingredient</button>
                                </div>
                              </div><!-- col -->                       

                              <div class="col-md-4">
                                <h6 class="heading">Rates</h6>
                                <div class="text mb-4 after-edit">
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Markup</span>
                                    <span><input type="text" value="<?php echo $row['markup']; ?>" name="markup" class="form-control"></span>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Price</span>
                                    <span><input type="number" value="<?php echo $row['price']; ?>" name="price" class="form-control"></span>
                                  </div>
                                </div>

                                <h6 class="heading">Category</h6>
                                <div class="text mb-4">
                                <select name="category" id="category" class="form-control">
                                  <?php
                                  $category2 = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                                  for($i=0; $i<sizeof($category2); $i++){ ?>
                                  <option value="<?php echo $category2[$i] ?>"><?php echo $category2[$i] ?></option>
                                  <?php } ?>
                              </select>
                                </div>

                                <h6 class="heading">Styles</h6>
                                <div class="text mb-4 check-bx">
                                <?php
                                  $style2 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                                  for($i=0; $i<sizeof($style2); $i++){ ?>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="checkbox" name="styles" class="form-check-input" value="<?php echo $style2[$i] ?>"><?php echo $style2[$i] ?>
                                    </label>
                                  </div>
                                  <?php } ?>
                                </div>

                                <h6 class="heading">Show in Gallery</h6>
                                <div class="text mb-4">
                                  <select id="is_active" value="<?php echo $row['is_active']; ?>" name="is_active" class="form-control">
                                    <option value="Show">Show</option>
                                    <option value="Hide">Hide</option>
                                  </select>
                                </div>
                              </div><!-- col -->
                            </div><!-- row -->
                          </div>

                        </div><!-- col -->
                        </form>
                      </div><!-- row -->
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>


          </div>
        </div><!-- col -->
      </div><!-- row -->
    </div><!-- container-fluid -->
  </main>
</div><!-- wrapper -->

<script src="js/custom.js"></script>
</body>
</html><!DOCTYPE html>
<html lang="en">
<head>
  <title>Proposals ChicLu</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body class="bg-gray">
  <div class="wrapper">
    <header class="main-header bg-white">
      <nav class="navbar navbar-expand-sm top-bar-nav">
        <ul class="navbar-nav flex-wrap">
          <li><a href="index.php">Home</a></li>
          <li><a href="#">Details</a></li>
          <li><a href="#">Worksheet</a></li>
          <li class="dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
              Resources
            </a>
            <div class="dropdown-menu">
              <a href="inspiration-gallery.php">Inspiration Gallery</a>
              <a href="recipe-gallery.php">Recipe Gallery</a>
              <a href="item-gallery.php">Item Gallery</a>
            </div>
          </li>
          <li><a href="#">Financials</a></li>
          <li><a href="#">Proposal</a></li>
          <li><a href="#">Documents</a></li>
          <li><a href="#">Costs</a></li>
        </ul>        
      </nav>
    </header>

    <main class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="right-sec py-3">

              <div class="p-3 mb-2 bg-info text-white func-dv">
                <div class="row align-items-center">
                  <div class="col-sm-4">
                    <div class="btn-dv">
                      <button id="filter-btn" type="button" class="btn btn-outline-light">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span>Filter / Search</span>
                      </button>
                    </div>
                  </div><!-- col -->

                  <div class="col-sm-4">
                    <h5 class="m-sm-0 font-weight-normal text-sm-center">Showing 856 items.</h5>
                  </div><!-- col -->

                  <div class="col-sm-4 text-sm-right">
                    <div class="btn-dv">
                      <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#newItem">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>New Recipe</span>
                      </button>

                      <button id="table-image-btn" type="button" class="btn btn-outline-light ml-2">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>Table</span>
                      </button>
                    </div>
                  </div><!-- col -->
                </div><!-- row -->
              </div>

              <div id="filter-dv" class="p-3 mb-2 bg-white filter-bx">
              <form action="search.php" method="post" >
                <div class="row">
                  <div class="col-md-4">
                    <div class="entry-txt">
                      <h6 class="heading">Colors</h6>
                      <div class="text mb-4 check-bx">
                        <?php $colorslist = array('White', 'Green', 'Red', 'Purple', 'Blue', 'Magenta','Black','Orange','Yellow','Brown','Bush','Valvet');
                        for($i=0; $i<sizeof($colorslist); $i++){ ?>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" name="colors[]" class="form-check-input" value="<?php echo $colorslist[$i]; ?>"><?php echo $colorslist[$i]; ?>
                            </label>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->

                  <div class="col-md-4">
                    <div class="entry-txt">
                      <h6 class="heading">Months</h6>
                      <div class="text mb-4 check-bx">
                        <?php $monthslist = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                        for($i=1; $i<=sizeof($monthslist); $i++){ ?>
                         <div class="form-check">
                          <label class="form-check-label">
                            <input type="checkbox" name="months[]" class="form-check-input month-filter" value="<?php echo $monthslist[$i] ?>"><?php echo $monthslist[$i] ?>
                          </label>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div><!-- col -->

                <div class="col-md-4">
                  <div class="entry-txt">
                    <h6 class="heading">Categories</h6>
                    <div class="text mb-4 check-bx">
                      <?php
                      $categories = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                        for($i=0; $i<sizeof($categories); $i++){ ?>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" name="categories[]" class="form-check-input" value="<?php echo $categories[$i] ?>"><?php echo $categories[$i] ?>
                            </label>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->
                </div><!-- row -->

                  <div class="filter-form-bx">
                    <div class="row mt-4 align-items-end">
                      <div class="col-md-3">
                       <div class="form-group">
                        <label for="uname">Search By</label>
                        <select name="search_by" id="search_by" class="form-control">
                          <option value="Recipe">Recipe</option>
                          <option value="Item">Item</option>
                        </select>
                      </div>
                    </div><!-- col -->

                    <div class="col-md-6">
                     <div class="form-group">
                      <label for="uname">Search Term:</label>
                      <input name="search_term" type="text" class="form-control">
                    </div>
                  </div><!-- col -->

                  <div class="col-md-3">
                   <div class="form-group">
                    <button class="btn btn-info" type="submit">Search/Filter</button>
                    <button class="btn btn-info" type="Reset">Clear</button>
                  </div>
                </div><!-- col -->
              </div><!-- row -->
            </div>
            </form>
          </div>

          <!-- Image Click Modal -->
          <div class="modal fade custom-modal" id="newItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header bg-info text-white">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Add Recipe</h5>
                  <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-lg-3">
                    <form action="sub-recipe.php" method="post" enctype="multipart/form-data">
                      <div class="image-dv  mb-3">
                        <h6 class="heading">Recipe Image</h6>
                        <div class="img">
                          <img src="img/image.jpg" alt="Image">
                        </div>
                      </div>
                      <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                          </div>
                    </div><!-- col -->
                    <div class="col-lg-9">
                      <div class="entry-txt mb-4">
                        <div class="row">
                          <div class="col-md-8">
                            <h6 class="heading">Recipe Name</h6>
                            <div class="text mb-4">
                              <input type="text" class="form-control" name="recipe_name" >
                            </div>

                            <h6 class="heading">Description</h6>
                            <div class="text mb-4">
                              <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                            </div>
                            <div>
                              <input type="hidden" name="source" value="Recipe">
                            </div>

                            <div class="mt-5">
                              <button class="btn btn-info" type="submit" value="Upload" name="img_upload">Add Ingredient</button>
                            </div>
                          </div><!-- col -->                       

                          <div class="col-md-4">
                            <h6 class="heading">Rates</h6>
                            <div class="text mb-4 after-edit">
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Cost</span>
                                <span>$0.00</span>
                              </div>
                              <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Markup</span>
                                <span><input type="text" class="form-control" name="markup" ></span>
                              </div>
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Price</span>
                                <span><input type="number" class="form-control" name="price" ></span>
                              </div>
                            </div>

                            <h6 class="heading">Category</h6>
                            <div class="text mb-4">
                              <select name="category" id="category" class="form-control">
                              <?php
                              $category1 = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                              for($i=0; $i<sizeof($category1); $i++){ ?>
                                  <option value="<?php echo $category1[$i] ?>"><?php echo $category1[$i] ?></option>
                                  <?php } ?>
                              </select>
                            </div>

                            <h6 class="heading">Styles</h6>
                            <div class="text mb-4 check-bx">
                            <?php
                              $style1 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                              for($i=0; $i<sizeof($style1); $i++){ ?>
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="checkbox" name="styles" class="form-check-input" value="<?php echo $style1[$i] ?>"><?php echo $style1[$i] ?>
                                </label>
                              </div>
                              <?php } ?>
                            </div>

                            <h6 class="heading">Show in Gallery</h6>
                            <div class="text mb-4">
                              <select name="is_active" id="is_active" class="form-control">
                                <option value="Show">Show</option>
                                <option value="Hide">Hide</option>
                              </select>
                            </div>
                          </div><!-- col -->
                        </div><!-- row -->
                      </div>
                      </form>
                    </div><!-- col -->
                    
                  </div><!-- row -->
                </div>
              </div>
            </div>
          </div><!-- modal -->

          <?php
          
          $a = $_GET['colors'];
          $c = $_GET['categories'];
        //   $d = $_POST['search_by'];
          $months = $_GET['months'];
          $e = $_GET['search_term'];
          $get_image = "SELECT * FROM `item_gallery` WHERE colors IN '$a' AND category IN '$c' AND months IN '$months' OR name LIKE '%$e%' OR item_name LIKE '%$e%' OR item_type LIKE '%$e%' OR recipe_name LIKE '%$e%'";  
          $result = mysqli_query($con, $get_image); ?>
          <div id="image-grid-bx" class="image-grid-bx">
            <div class="row row-5">
              <?php while($img = mysqli_fetch_assoc($result)){ ?>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6 col" data-toggle="modal" data-target="#model<?php echo $img['id']; ?>">
                  <div class="image-grid-dv d-flex flex-column justify-content-end">
                    <div class="img">
                      <img src="<?php echo "img/".$img['name']; ?>" alt="Image">
                    </div><!-- img -->
                    <h6 class="title m-0 p-3"><?php echo $img['name']; ?></h6>
                  </div>
                </div><!-- col -->
                <?php } ?>

              </div><!-- row -->
            </div><!-- image-grid-bx -->

          <?php
          $tblsql = "SELECT * FROM `gallery` WHERE source='Recipe'";  
          $tblresult = mysqli_query($con, $tblsql); ?>
          <div id="table-list-bx" class="table-list-bx bg-white d-none">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              <?php while($tbl = mysqli_fetch_assoc($tblresult)){ ?>
                  <tr>
                    <td data-toggle="modal" data-target="#model1">
                      <div class="img max-50">
                        <img class="img-thumbnail" src="<?php echo "img/".$tbl['name']; ?>" alt="Image">
                      </div>
                    </td>
                    <td><?php echo $tbl['recipe_name']; ?></td>                
                    <td class="img max-50"><?php echo $tbl['description']; ?></td>                
                    <td><?php echo $tbl['price']; ?></td>                
                    <td>
                      <span class="delete"><i class="fa fa-times" onclick="imgDelete(<?php echo $tbl['id']; ?>)" aria-hidden="true"></i></span>
                    </td>                
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div><!-- table-list-bx -->


          <!-- Image Click Modal -->
            <?php 
            $resource_detail_sql =  "SELECT * FROM `gallery` WHERE source='Recipe'";  
            $resource_detail_res = mysqli_query($con, $resource_detail_sql);
            while($row = mysqli_fetch_assoc($resource_detail_res)){ ?>
              <div class="modal fade custom-modal" id="model<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-info text-white flex-wrap">
                      <h5 class="modal-title" id="exampleModalCenterTitle"><?php echo $row['recipe_name']; ?></h5>

                      <button id="edit-btn<?php echo $row['id']; ?>" type="button" class="btn btn-outline-light ml-sm-auto editbtn">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>Edit</span>
                      </button>
                      <button type="button" class="btn btn-outline-light ml-2" onclick="imgDelete(<?php echo $row['id']; ?>)">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span>Delete</span>
                      </button>
                      <script>
                        function imgDelete(id) {
                          if(confirm("Are you sure you want to Delete?")){
                            del="delete.php?id="+id+"&source=Recipe";
                            window.location.href = del;
                          }
                        
                        }
                        </script>
                      <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div id="before-edit" class="row before-edit">
                        <div class="col-lg-4 col-md-3">
                          <div class="image-dv mb-5">
                            <h6 class="heading">Recipe Image</h6>
                            <div class="img">
                              <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                            </div>
                          </div>

                          <div class="image-dv image-dv-thumb">
                            <h6 class="heading">Ingredient Images</h6>
                            <div class="row row-5">
                            <?php
                            $id =  $row['id'];
                            $match = $row['recipe_name'];
                            $ingsql = "SELECT * FROM `item_gallery` WHERE recipe_name='$match'";
                            $ingresult = mysqli_query($con, $ingsql);
                            while($ing = mysqli_fetch_assoc($ingresult)){ ?>
                              <div class="col-4">
                                <div class="img"><img src="<?php echo "img/".$ing['name']; ?>" alt="Image"></div>
                              </div>
                            <?php } ?>
                            </div>
                          </div>
                        </div><!-- col -->
                        <div class="col-lg-8 col-md-9">
                          <div class="entry-txt mb-4">
                            <div class="row">
                              <div class="col-md-7">
                                <h6 class="heading">Description</h6>
                                <div class="text mih-200 mb-4">
                                  <p><?php echo $row['description']; ?></p>
                                </div>

                                <h6 class="heading">Ingredients</h6>
                                <div class="text">
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <span>Item Name</span>
                                    <span>Qty.</span>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <span>Avg Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <span>Avg Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                </div>
                              </div><!-- col -->                       

                              <div class="col-md-5">
                                <h6 class="heading">Rates</h6>
                                <div class="text mb-4">
                                  <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Markup</span>
                                    <span><?php echo $row['markup']; ?></span>
                                  </div>
                                  <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Price</span>
                                    <span><?php echo $row['price']; ?></span>
                                  </div>
                                </div>

                                <h6 class="heading">Category</h6>
                                <div class="text mb-4"><?php echo $row['category']; ?></div>

                                <h6 class="heading">Styles</h6>
                                <div class="text mb-4"><?php echo $row['styles']; ?></div>

                                <h6 class="heading">Colors</h6>
                                <div class="text mb-4">
                                  Green, Silver, Brown, Neutral, White, Pink, Orange
                                </div>

                                <h6 class="heading">Months</h6>
                                <div class="text mb-4">
                                  May, June, July, August, September, October, November, December
                                </div>
                              </div><!-- col -->
                            </div><!-- row -->
                          </div>

                        </div><!-- col -->
                      </div><!-- row -->

                      <div id="after-edit" class="row after-edit">
                        <div class="col-lg-4 col-md-3">
                        <form action="sub-edit.php" method="post" enctype="multipart/form-data">
                          <div class="image-dv mb-5">
                            <h6 class="heading">Recipe Image</h6>
                            <div class="img">
                              <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                            </div>
                            <div><input type="hidden" name="source" value="Recipe"></div>
                          </div>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                          </div>

                          <div class="image-dv">
                            <h6 class="heading">Ingredient Images</h6>
                            <div class="row row-5">
                            <?php
                            $id =  $row['id'];
                            $ingsql2 = "SELECT * FROM `gallery` WHERE source='Recipe' AND parent_id='$id'";
                            $ingresult2 = mysqli_query($con, $ingsql2);
                            while($ing2 = mysqli_fetch_assoc($ingresult2)){ ?>
                              <div class="col-4">
                                <img src="<?php echo "img/".$ing2['name']; ?>" alt="Image">
                              </div>
                            <?php } ?>
                            </div>
                          </div>
                        </div><!-- col -->
                        
                        <div class="col-lg-8 col-md-9">
                          <div class="entry-txt mb-4">
                            <div class="row">
                              <div class="col-md-8">
                                <h6 class="heading">Recipe Name</h6>
                                <div class="text mb-4">
                                  <input type="text" class="form-control" value="<?php echo $row['recipe_name']; ?>" name="recipe_name">
                                </div>
                                <div><input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>"></div>

                                <h6 class="heading">Description</h6>
                                <div class="text mb-4">
                                  <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                                </div>

                                <div class="text mb-5 after-edit">
                                  <div class="alert flex-wrap flex-row alert-dark d-flex justify-content-between mb-2">
                                    <div>Ingredients</div>
                                    <div>Show Deleted</div>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <div>Item Name</div>
                                    <div>Qty.</div>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_one']; ?>" name="avg_cost_one" id="avg_cost_one"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_two']; ?>" name="avg_cost_two" id="avg_cost_two"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_three']; ?>" name="avg_cost_three" id="avg_cost_three"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                </div>

                                <div class="my-5">
                                  <button class="btn btn-info" type="submit" value="Upload" name="img_upload">Add Ingredient</button>
                                </div>
                              </div><!-- col -->                       

                              <div class="col-md-4">
                                <h6 class="heading">Rates</h6>
                                <div class="text mb-4 after-edit">
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Markup</span>
                                    <span><input type="text" value="<?php echo $row['markup']; ?>" name="markup" class="form-control"></span>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Price</span>
                                    <span><input type="number" value="<?php echo $row['price']; ?>" name="price" class="form-control"></span>
                                  </div>
                                </div>

                                <h6 class="heading">Category</h6>
                                <div class="text mb-4">
                                <select name="category" id="category" class="form-control">
                                  <?php
                                  $category2 = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                                  for($i=0; $i<sizeof($category2); $i++){ ?>
                                  <option value="<?php echo $category2[$i] ?>"><?php echo $category2[$i] ?></option>
                                  <?php } ?>
                              </select>
                                </div>

                                <h6 class="heading">Styles</h6>
                                <div class="text mb-4 check-bx">
                                <?php
                                  $style2 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                                  for($i=0; $i<sizeof($style2); $i++){ ?>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="checkbox" name="styles" class="form-check-input" value="<?php echo $style2[$i] ?>"><?php echo $style2[$i] ?>
                                    </label>
                                  </div>
                                  <?php } ?>
                                </div>

                                <h6 class="heading">Show in Gallery</h6>
                                <div class="text mb-4">
                                  <select id="is_active" value="<?php echo $row['is_active']; ?>" name="is_active" class="form-control">
                                    <option value="Show">Show</option>
                                    <option value="Hide">Hide</option>
                                  </select>
                                </div>
                              </div><!-- col -->
                            </div><!-- row -->
                          </div>

                        </div><!-- col -->
                        </form>
                      </div><!-- row -->
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>


          </div>
        </div><!-- col -->
      </div><!-- row -->
    </div><!-- container-fluid -->
  </main>
</div><!-- wrapper -->

<script src="js/custom.js"></script>
</body>
</html><!DOCTYPE html>
<html lang="en">
<head>
  <title>Proposals ChicLu</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body class="bg-gray">
  <div class="wrapper">
    <header class="main-header bg-white">
      <nav class="navbar navbar-expand-sm top-bar-nav">
        <ul class="navbar-nav flex-wrap">
          <li><a href="index.php">Home</a></li>
          <li><a href="#">Details</a></li>
          <li><a href="#">Worksheet</a></li>
          <li class="dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
              Resources
            </a>
            <div class="dropdown-menu">
              <a href="inspiration-gallery.php">Inspiration Gallery</a>
              <a href="recipe-gallery.php">Recipe Gallery</a>
              <a href="item-gallery.php">Item Gallery</a>
            </div>
          </li>
          <li><a href="#">Financials</a></li>
          <li><a href="#">Proposal</a></li>
          <li><a href="#">Documents</a></li>
          <li><a href="#">Costs</a></li>
        </ul>        
      </nav>
    </header>

    <main class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="right-sec py-3">

              <div class="p-3 mb-2 bg-info text-white func-dv">
                <div class="row align-items-center">
                  <div class="col-sm-4">
                    <div class="btn-dv">
                      <button id="filter-btn" type="button" class="btn btn-outline-light">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span>Filter / Search</span>
                      </button>
                    </div>
                  </div><!-- col -->

                  <div class="col-sm-4">
                    <h5 class="m-sm-0 font-weight-normal text-sm-center">Showing 856 items.</h5>
                  </div><!-- col -->

                  <div class="col-sm-4 text-sm-right">
                    <div class="btn-dv">
                      <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#newItem">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>New Recipe</span>
                      </button>

                      <button id="table-image-btn" type="button" class="btn btn-outline-light ml-2">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>Table</span>
                      </button>
                    </div>
                  </div><!-- col -->
                </div><!-- row -->
              </div>

              <div id="filter-dv" class="p-3 mb-2 bg-white filter-bx">
              <form action="search.php" method="post" >
                <div class="row">
                  <div class="col-md-4">
                    <div class="entry-txt">
                      <h6 class="heading">Colors</h6>
                      <div class="text mb-4 check-bx">
                        <?php $colorslist = array('White', 'Green', 'Red', 'Purple', 'Blue', 'Magenta','Black','Orange','Yellow','Brown','Bush','Valvet');
                        for($i=0; $i<sizeof($colorslist); $i++){ ?>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" name="colors[]" class="form-check-input" value="<?php echo $colorslist[$i]; ?>"><?php echo $colorslist[$i]; ?>
                            </label>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->

                  <div class="col-md-4">
                    <div class="entry-txt">
                      <h6 class="heading">Months</h6>
                      <div class="text mb-4 check-bx">
                        <?php $monthslist = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                        for($i=1; $i<=sizeof($monthslist); $i++){ ?>
                         <div class="form-check">
                          <label class="form-check-label">
                            <input type="checkbox" name="months[]" class="form-check-input month-filter" value="<?php echo $monthslist[$i] ?>"><?php echo $monthslist[$i] ?>
                          </label>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div><!-- col -->

                <div class="col-md-4">
                  <div class="entry-txt">
                    <h6 class="heading">Categories</h6>
                    <div class="text mb-4 check-bx">
                      <?php
                      $categories = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                        for($i=0; $i<sizeof($categories); $i++){ ?>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" name="categories[]" class="form-check-input" value="<?php echo $categories[$i] ?>"><?php echo $categories[$i] ?>
                            </label>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->
                </div><!-- row -->

                  <div class="filter-form-bx">
                    <div class="row mt-4 align-items-end">
                      <div class="col-md-3">
                       <div class="form-group">
                        <label for="uname">Search By</label>
                        <select name="search_by" id="search_by" class="form-control">
                          <option value="Recipe">Recipe</option>
                          <option value="Item">Item</option>
                        </select>
                      </div>
                    </div><!-- col -->

                    <div class="col-md-6">
                     <div class="form-group">
                      <label for="uname">Search Term:</label>
                      <input name="search_term" type="text" class="form-control">
                    </div>
                  </div><!-- col -->

                  <div class="col-md-3">
                   <div class="form-group">
                    <button class="btn btn-info" type="submit">Search/Filter</button>
                    <button class="btn btn-info" type="Reset">Clear</button>
                  </div>
                </div><!-- col -->
              </div><!-- row -->
            </div>
            </form>
          </div>

          <!-- Image Click Modal -->
          <div class="modal fade custom-modal" id="newItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header bg-info text-white">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Add Recipe</h5>
                  <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-lg-3">
                    <form action="sub-recipe.php" method="post" enctype="multipart/form-data">
                      <div class="image-dv  mb-3">
                        <h6 class="heading">Recipe Image</h6>
                        <div class="img">
                          <img src="img/image.jpg" alt="Image">
                        </div>
                      </div>
                      <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                          </div>
                    </div><!-- col -->
                    <div class="col-lg-9">
                      <div class="entry-txt mb-4">
                        <div class="row">
                          <div class="col-md-8">
                            <h6 class="heading">Recipe Name</h6>
                            <div class="text mb-4">
                              <input type="text" class="form-control" name="recipe_name" >
                            </div>

                            <h6 class="heading">Description</h6>
                            <div class="text mb-4">
                              <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                            </div>
                            <div>
                              <input type="hidden" name="source" value="Recipe">
                            </div>

                            <div class="mt-5">
                              <button class="btn btn-info" type="submit" value="Upload" name="img_upload">Add Ingredient</button>
                            </div>
                          </div><!-- col -->                       

                          <div class="col-md-4">
                            <h6 class="heading">Rates</h6>
                            <div class="text mb-4 after-edit">
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Cost</span>
                                <span>$0.00</span>
                              </div>
                              <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Markup</span>
                                <span><input type="text" class="form-control" name="markup" ></span>
                              </div>
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Price</span>
                                <span><input type="number" class="form-control" name="price" ></span>
                              </div>
                            </div>

                            <h6 class="heading">Category</h6>
                            <div class="text mb-4">
                              <select name="category" id="category" class="form-control">
                              <?php
                              $category1 = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                              for($i=0; $i<sizeof($category1); $i++){ ?>
                                  <option value="<?php echo $category1[$i] ?>"><?php echo $category1[$i] ?></option>
                                  <?php } ?>
                              </select>
                            </div>

                            <h6 class="heading">Styles</h6>
                            <div class="text mb-4 check-bx">
                            <?php
                              $style1 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                              for($i=0; $i<sizeof($style1); $i++){ ?>
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="checkbox" name="styles" class="form-check-input" value="<?php echo $style1[$i] ?>"><?php echo $style1[$i] ?>
                                </label>
                              </div>
                              <?php } ?>
                            </div>

                            <h6 class="heading">Show in Gallery</h6>
                            <div class="text mb-4">
                              <select name="is_active" id="is_active" class="form-control">
                                <option value="Show">Show</option>
                                <option value="Hide">Hide</option>
                              </select>
                            </div>
                          </div><!-- col -->
                        </div><!-- row -->
                      </div>
                      </form>
                    </div><!-- col -->
                    
                  </div><!-- row -->
                </div>
              </div>
            </div>
          </div><!-- modal -->

          <?php
          
          $a = $_GET['colors'];
          $c = $_GET['categories'];
        //   $d = $_POST['search_by'];
          $months = $_GET['months'];
          $e = $_GET['search_term'];
          $get_image = "SELECT * FROM `item_gallery` WHERE colors IN '$a' AND category IN '$c' AND months IN '$months' OR name LIKE '%$e%' OR item_name LIKE '%$e%' OR item_type LIKE '%$e%' OR recipe_name LIKE '%$e%'";  
          $result = mysqli_query($con, $get_image); ?>
          <div id="image-grid-bx" class="image-grid-bx">
            <div class="row row-5">
              <?php while($img = mysqli_fetch_assoc($result)){ ?>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6 col" data-toggle="modal" data-target="#model<?php echo $img['id']; ?>">
                  <div class="image-grid-dv d-flex flex-column justify-content-end">
                    <div class="img">
                      <img src="<?php echo "img/".$img['name']; ?>" alt="Image">
                    </div><!-- img -->
                    <h6 class="title m-0 p-3"><?php echo $img['name']; ?></h6>
                  </div>
                </div><!-- col -->
                <?php } ?>

              </div><!-- row -->
            </div><!-- image-grid-bx -->

          <?php
          $tblsql = "SELECT * FROM `gallery` WHERE source='Recipe'";  
          $tblresult = mysqli_query($con, $tblsql); ?>
          <div id="table-list-bx" class="table-list-bx bg-white d-none">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              <?php while($tbl = mysqli_fetch_assoc($tblresult)){ ?>
                  <tr>
                    <td data-toggle="modal" data-target="#model1">
                      <div class="img max-50">
                        <img class="img-thumbnail" src="<?php echo "img/".$tbl['name']; ?>" alt="Image">
                      </div>
                    </td>
                    <td><?php echo $tbl['recipe_name']; ?></td>                
                    <td class="img max-50"><?php echo $tbl['description']; ?></td>                
                    <td><?php echo $tbl['price']; ?></td>                
                    <td>
                      <span class="delete"><i class="fa fa-times" onclick="imgDelete(<?php echo $tbl['id']; ?>)" aria-hidden="true"></i></span>
                    </td>                
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div><!-- table-list-bx -->


          <!-- Image Click Modal -->
            <?php 
            $resource_detail_sql =  "SELECT * FROM `gallery` WHERE source='Recipe'";  
            $resource_detail_res = mysqli_query($con, $resource_detail_sql);
            while($row = mysqli_fetch_assoc($resource_detail_res)){ ?>
              <div class="modal fade custom-modal" id="model<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-info text-white flex-wrap">
                      <h5 class="modal-title" id="exampleModalCenterTitle"><?php echo $row['recipe_name']; ?></h5>

                      <button id="edit-btn<?php echo $row['id']; ?>" type="button" class="btn btn-outline-light ml-sm-auto editbtn">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>Edit</span>
                      </button>
                      <button type="button" class="btn btn-outline-light ml-2" onclick="imgDelete(<?php echo $row['id']; ?>)">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span>Delete</span>
                      </button>
                      <script>
                        function imgDelete(id) {
                          if(confirm("Are you sure you want to Delete?")){
                            del="delete.php?id="+id+"&source=Recipe";
                            window.location.href = del;
                          }
                        
                        }
                        </script>
                      <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div id="before-edit" class="row before-edit">
                        <div class="col-lg-4 col-md-3">
                          <div class="image-dv mb-5">
                            <h6 class="heading">Recipe Image</h6>
                            <div class="img">
                              <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                            </div>
                          </div>

                          <div class="image-dv image-dv-thumb">
                            <h6 class="heading">Ingredient Images</h6>
                            <div class="row row-5">
                            <?php
                            $id =  $row['id'];
                            $match = $row['recipe_name'];
                            $ingsql = "SELECT * FROM `item_gallery` WHERE recipe_name='$match'";
                            $ingresult = mysqli_query($con, $ingsql);
                            while($ing = mysqli_fetch_assoc($ingresult)){ ?>
                              <div class="col-4">
                                <div class="img"><img src="<?php echo "img/".$ing['name']; ?>" alt="Image"></div>
                              </div>
                            <?php } ?>
                            </div>
                          </div>
                        </div><!-- col -->
                        <div class="col-lg-8 col-md-9">
                          <div class="entry-txt mb-4">
                            <div class="row">
                              <div class="col-md-7">
                                <h6 class="heading">Description</h6>
                                <div class="text mih-200 mb-4">
                                  <p><?php echo $row['description']; ?></p>
                                </div>

                                <h6 class="heading">Ingredients</h6>
                                <div class="text">
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <span>Item Name</span>
                                    <span>Qty.</span>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <span>Avg Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <span>Avg Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                </div>
                              </div><!-- col -->                       

                              <div class="col-md-5">
                                <h6 class="heading">Rates</h6>
                                <div class="text mb-4">
                                  <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Markup</span>
                                    <span><?php echo $row['markup']; ?></span>
                                  </div>
                                  <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Price</span>
                                    <span><?php echo $row['price']; ?></span>
                                  </div>
                                </div>

                                <h6 class="heading">Category</h6>
                                <div class="text mb-4"><?php echo $row['category']; ?></div>

                                <h6 class="heading">Styles</h6>
                                <div class="text mb-4"><?php echo $row['styles']; ?></div>

                                <h6 class="heading">Colors</h6>
                                <div class="text mb-4">
                                  Green, Silver, Brown, Neutral, White, Pink, Orange
                                </div>

                                <h6 class="heading">Months</h6>
                                <div class="text mb-4">
                                  May, June, July, August, September, October, November, December
                                </div>
                              </div><!-- col -->
                            </div><!-- row -->
                          </div>

                        </div><!-- col -->
                      </div><!-- row -->

                      <div id="after-edit" class="row after-edit">
                        <div class="col-lg-4 col-md-3">
                        <form action="sub-edit.php" method="post" enctype="multipart/form-data">
                          <div class="image-dv mb-5">
                            <h6 class="heading">Recipe Image</h6>
                            <div class="img">
                              <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                            </div>
                            <div><input type="hidden" name="source" value="Recipe"></div>
                          </div>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                          </div>

                          <div class="image-dv">
                            <h6 class="heading">Ingredient Images</h6>
                            <div class="row row-5">
                            <?php
                            $id =  $row['id'];
                            $ingsql2 = "SELECT * FROM `gallery` WHERE source='Recipe' AND parent_id='$id'";
                            $ingresult2 = mysqli_query($con, $ingsql2);
                            while($ing2 = mysqli_fetch_assoc($ingresult2)){ ?>
                              <div class="col-4">
                                <img src="<?php echo "img/".$ing2['name']; ?>" alt="Image">
                              </div>
                            <?php } ?>
                            </div>
                          </div>
                        </div><!-- col -->
                        
                        <div class="col-lg-8 col-md-9">
                          <div class="entry-txt mb-4">
                            <div class="row">
                              <div class="col-md-8">
                                <h6 class="heading">Recipe Name</h6>
                                <div class="text mb-4">
                                  <input type="text" class="form-control" value="<?php echo $row['recipe_name']; ?>" name="recipe_name">
                                </div>
                                <div><input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>"></div>

                                <h6 class="heading">Description</h6>
                                <div class="text mb-4">
                                  <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                                </div>

                                <div class="text mb-5 after-edit">
                                  <div class="alert flex-wrap flex-row alert-dark d-flex justify-content-between mb-2">
                                    <div>Ingredients</div>
                                    <div>Show Deleted</div>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <div>Item Name</div>
                                    <div>Qty.</div>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_one']; ?>" name="avg_cost_one" id="avg_cost_one"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_two']; ?>" name="avg_cost_two" id="avg_cost_two"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_three']; ?>" name="avg_cost_three" id="avg_cost_three"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                </div>

                                <div class="my-5">
                                  <button class="btn btn-info" type="submit" value="Upload" name="img_upload">Add Ingredient</button>
                                </div>
                              </div><!-- col -->                       

                              <div class="col-md-4">
                                <h6 class="heading">Rates</h6>
                                <div class="text mb-4 after-edit">
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Markup</span>
                                    <span><input type="text" value="<?php echo $row['markup']; ?>" name="markup" class="form-control"></span>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Price</span>
                                    <span><input type="number" value="<?php echo $row['price']; ?>" name="price" class="form-control"></span>
                                  </div>
                                </div>

                                <h6 class="heading">Category</h6>
                                <div class="text mb-4">
                                <select name="category" id="category" class="form-control">
                                  <?php
                                  $category2 = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                                  for($i=0; $i<sizeof($category2); $i++){ ?>
                                  <option value="<?php echo $category2[$i] ?>"><?php echo $category2[$i] ?></option>
                                  <?php } ?>
                              </select>
                                </div>

                                <h6 class="heading">Styles</h6>
                                <div class="text mb-4 check-bx">
                                <?php
                                  $style2 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                                  for($i=0; $i<sizeof($style2); $i++){ ?>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="checkbox" name="styles" class="form-check-input" value="<?php echo $style2[$i] ?>"><?php echo $style2[$i] ?>
                                    </label>
                                  </div>
                                  <?php } ?>
                                </div>

                                <h6 class="heading">Show in Gallery</h6>
                                <div class="text mb-4">
                                  <select id="is_active" value="<?php echo $row['is_active']; ?>" name="is_active" class="form-control">
                                    <option value="Show">Show</option>
                                    <option value="Hide">Hide</option>
                                  </select>
                                </div>
                              </div><!-- col -->
                            </div><!-- row -->
                          </div>

                        </div><!-- col -->
                        </form>
                      </div><!-- row -->
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>


          </div>
        </div><!-- col -->
      </div><!-- row -->
    </div><!-- container-fluid -->
  </main>
</div><!-- wrapper -->

<script src="js/custom.js"></script>
</body>
</html><!DOCTYPE html>
<html lang="en">
<head>
  <title>Proposals ChicLu</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body class="bg-gray">
  <div class="wrapper">
    <header class="main-header bg-white">
      <nav class="navbar navbar-expand-sm top-bar-nav">
        <ul class="navbar-nav flex-wrap">
          <li><a href="index.php">Home</a></li>
          <li><a href="#">Details</a></li>
          <li><a href="#">Worksheet</a></li>
          <li class="dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
              Resources
            </a>
            <div class="dropdown-menu">
              <a href="inspiration-gallery.php">Inspiration Gallery</a>
              <a href="recipe-gallery.php">Recipe Gallery</a>
              <a href="item-gallery.php">Item Gallery</a>
            </div>
          </li>
          <li><a href="#">Financials</a></li>
          <li><a href="#">Proposal</a></li>
          <li><a href="#">Documents</a></li>
          <li><a href="#">Costs</a></li>
        </ul>        
      </nav>
    </header>

    <main class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="right-sec py-3">

              <div class="p-3 mb-2 bg-info text-white func-dv">
                <div class="row align-items-center">
                  <div class="col-sm-4">
                    <div class="btn-dv">
                      <button id="filter-btn" type="button" class="btn btn-outline-light">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span>Filter / Search</span>
                      </button>
                    </div>
                  </div><!-- col -->

                  <div class="col-sm-4">
                    <h5 class="m-sm-0 font-weight-normal text-sm-center">Showing 856 items.</h5>
                  </div><!-- col -->

                  <div class="col-sm-4 text-sm-right">
                    <div class="btn-dv">
                      <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#newItem">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>New Recipe</span>
                      </button>

                      <button id="table-image-btn" type="button" class="btn btn-outline-light ml-2">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>Table</span>
                      </button>
                    </div>
                  </div><!-- col -->
                </div><!-- row -->
              </div>

              <div id="filter-dv" class="p-3 mb-2 bg-white filter-bx">
              <form action="search.php" method="post" >
                <div class="row">
                  <div class="col-md-4">
                    <div class="entry-txt">
                      <h6 class="heading">Colors</h6>
                      <div class="text mb-4 check-bx">
                        <?php $colorslist = array('White', 'Green', 'Red', 'Purple', 'Blue', 'Magenta','Black','Orange','Yellow','Brown','Bush','Valvet');
                        for($i=0; $i<sizeof($colorslist); $i++){ ?>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" name="colors[]" class="form-check-input" value="<?php echo $colorslist[$i]; ?>"><?php echo $colorslist[$i]; ?>
                            </label>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->

                  <div class="col-md-4">
                    <div class="entry-txt">
                      <h6 class="heading">Months</h6>
                      <div class="text mb-4 check-bx">
                        <?php $monthslist = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                        for($i=1; $i<=sizeof($monthslist); $i++){ ?>
                         <div class="form-check">
                          <label class="form-check-label">
                            <input type="checkbox" name="months[]" class="form-check-input month-filter" value="<?php echo $monthslist[$i] ?>"><?php echo $monthslist[$i] ?>
                          </label>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div><!-- col -->

                <div class="col-md-4">
                  <div class="entry-txt">
                    <h6 class="heading">Categories</h6>
                    <div class="text mb-4 check-bx">
                      <?php
                      $categories = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                        for($i=0; $i<sizeof($categories); $i++){ ?>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" name="categories[]" class="form-check-input" value="<?php echo $categories[$i] ?>"><?php echo $categories[$i] ?>
                            </label>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->
                </div><!-- row -->

                  <div class="filter-form-bx">
                    <div class="row mt-4 align-items-end">
                      <div class="col-md-3">
                       <div class="form-group">
                        <label for="uname">Search By</label>
                        <select name="search_by" id="search_by" class="form-control">
                          <option value="Recipe">Recipe</option>
                          <option value="Item">Item</option>
                        </select>
                      </div>
                    </div><!-- col -->

                    <div class="col-md-6">
                     <div class="form-group">
                      <label for="uname">Search Term:</label>
                      <input name="search_term" type="text" class="form-control">
                    </div>
                  </div><!-- col -->

                  <div class="col-md-3">
                   <div class="form-group">
                    <button class="btn btn-info" type="submit">Search/Filter</button>
                    <button class="btn btn-info" type="Reset">Clear</button>
                  </div>
                </div><!-- col -->
              </div><!-- row -->
            </div>
            </form>
          </div>

          <!-- Image Click Modal -->
          <div class="modal fade custom-modal" id="newItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header bg-info text-white">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Add Recipe</h5>
                  <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-lg-3">
                    <form action="sub-recipe.php" method="post" enctype="multipart/form-data">
                      <div class="image-dv  mb-3">
                        <h6 class="heading">Recipe Image</h6>
                        <div class="img">
                          <img src="img/image.jpg" alt="Image">
                        </div>
                      </div>
                      <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                          </div>
                    </div><!-- col -->
                    <div class="col-lg-9">
                      <div class="entry-txt mb-4">
                        <div class="row">
                          <div class="col-md-8">
                            <h6 class="heading">Recipe Name</h6>
                            <div class="text mb-4">
                              <input type="text" class="form-control" name="recipe_name" >
                            </div>

                            <h6 class="heading">Description</h6>
                            <div class="text mb-4">
                              <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                            </div>
                            <div>
                              <input type="hidden" name="source" value="Recipe">
                            </div>

                            <div class="mt-5">
                              <button class="btn btn-info" type="submit" value="Upload" name="img_upload">Add Ingredient</button>
                            </div>
                          </div><!-- col -->                       

                          <div class="col-md-4">
                            <h6 class="heading">Rates</h6>
                            <div class="text mb-4 after-edit">
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Cost</span>
                                <span>$0.00</span>
                              </div>
                              <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Markup</span>
                                <span><input type="text" class="form-control" name="markup" ></span>
                              </div>
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Price</span>
                                <span><input type="number" class="form-control" name="price" ></span>
                              </div>
                            </div>

                            <h6 class="heading">Category</h6>
                            <div class="text mb-4">
                              <select name="category" id="category" class="form-control">
                              <?php
                              $category1 = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                              for($i=0; $i<sizeof($category1); $i++){ ?>
                                  <option value="<?php echo $category1[$i] ?>"><?php echo $category1[$i] ?></option>
                                  <?php } ?>
                              </select>
                            </div>

                            <h6 class="heading">Styles</h6>
                            <div class="text mb-4 check-bx">
                            <?php
                              $style1 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                              for($i=0; $i<sizeof($style1); $i++){ ?>
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="checkbox" name="styles" class="form-check-input" value="<?php echo $style1[$i] ?>"><?php echo $style1[$i] ?>
                                </label>
                              </div>
                              <?php } ?>
                            </div>

                            <h6 class="heading">Show in Gallery</h6>
                            <div class="text mb-4">
                              <select name="is_active" id="is_active" class="form-control">
                                <option value="Show">Show</option>
                                <option value="Hide">Hide</option>
                              </select>
                            </div>
                          </div><!-- col -->
                        </div><!-- row -->
                      </div>
                      </form>
                    </div><!-- col -->
                    
                  </div><!-- row -->
                </div>
              </div>
            </div>
          </div><!-- modal -->

          <?php
          
          $a = $_GET['colors'];
          $c = $_GET['categories'];
        //   $d = $_POST['search_by'];
          $months = $_GET['months'];
          $e = $_GET['search_term'];
          $get_image = "SELECT * FROM `item_gallery` WHERE colors IN '$a' AND category IN '$c' AND months IN '$months' OR name LIKE '%$e%' OR item_name LIKE '%$e%' OR item_type LIKE '%$e%' OR recipe_name LIKE '%$e%'";  
          $result = mysqli_query($con, $get_image); ?>
          <div id="image-grid-bx" class="image-grid-bx">
            <div class="row row-5">
              <?php while($img = mysqli_fetch_assoc($result)){ ?>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6 col" data-toggle="modal" data-target="#model<?php echo $img['id']; ?>">
                  <div class="image-grid-dv d-flex flex-column justify-content-end">
                    <div class="img">
                      <img src="<?php echo "img/".$img['name']; ?>" alt="Image">
                    </div><!-- img -->
                    <h6 class="title m-0 p-3"><?php echo $img['name']; ?></h6>
                  </div>
                </div><!-- col -->
                <?php } ?>

              </div><!-- row -->
            </div><!-- image-grid-bx -->

          <?php
          $tblsql = "SELECT * FROM `gallery` WHERE source='Recipe'";  
          $tblresult = mysqli_query($con, $tblsql); ?>
          <div id="table-list-bx" class="table-list-bx bg-white d-none">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              <?php while($tbl = mysqli_fetch_assoc($tblresult)){ ?>
                  <tr>
                    <td data-toggle="modal" data-target="#model1">
                      <div class="img max-50">
                        <img class="img-thumbnail" src="<?php echo "img/".$tbl['name']; ?>" alt="Image">
                      </div>
                    </td>
                    <td><?php echo $tbl['recipe_name']; ?></td>                
                    <td class="img max-50"><?php echo $tbl['description']; ?></td>                
                    <td><?php echo $tbl['price']; ?></td>                
                    <td>
                      <span class="delete"><i class="fa fa-times" onclick="imgDelete(<?php echo $tbl['id']; ?>)" aria-hidden="true"></i></span>
                    </td>                
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div><!-- table-list-bx -->


          <!-- Image Click Modal -->
            <?php 
            $resource_detail_sql =  "SELECT * FROM `gallery` WHERE source='Recipe'";  
            $resource_detail_res = mysqli_query($con, $resource_detail_sql);
            while($row = mysqli_fetch_assoc($resource_detail_res)){ ?>
              <div class="modal fade custom-modal" id="model<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-info text-white flex-wrap">
                      <h5 class="modal-title" id="exampleModalCenterTitle"><?php echo $row['recipe_name']; ?></h5>

                      <button id="edit-btn<?php echo $row['id']; ?>" type="button" class="btn btn-outline-light ml-sm-auto editbtn">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>Edit</span>
                      </button>
                      <button type="button" class="btn btn-outline-light ml-2" onclick="imgDelete(<?php echo $row['id']; ?>)">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span>Delete</span>
                      </button>
                      <script>
                        function imgDelete(id) {
                          if(confirm("Are you sure you want to Delete?")){
                            del="delete.php?id="+id+"&source=Recipe";
                            window.location.href = del;
                          }
                        
                        }
                        </script>
                      <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div id="before-edit" class="row before-edit">
                        <div class="col-lg-4 col-md-3">
                          <div class="image-dv mb-5">
                            <h6 class="heading">Recipe Image</h6>
                            <div class="img">
                              <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                            </div>
                          </div>

                          <div class="image-dv image-dv-thumb">
                            <h6 class="heading">Ingredient Images</h6>
                            <div class="row row-5">
                            <?php
                            $id =  $row['id'];
                            $match = $row['recipe_name'];
                            $ingsql = "SELECT * FROM `item_gallery` WHERE recipe_name='$match'";
                            $ingresult = mysqli_query($con, $ingsql);
                            while($ing = mysqli_fetch_assoc($ingresult)){ ?>
                              <div class="col-4">
                                <div class="img"><img src="<?php echo "img/".$ing['name']; ?>" alt="Image"></div>
                              </div>
                            <?php } ?>
                            </div>
                          </div>
                        </div><!-- col -->
                        <div class="col-lg-8 col-md-9">
                          <div class="entry-txt mb-4">
                            <div class="row">
                              <div class="col-md-7">
                                <h6 class="heading">Description</h6>
                                <div class="text mih-200 mb-4">
                                  <p><?php echo $row['description']; ?></p>
                                </div>

                                <h6 class="heading">Ingredients</h6>
                                <div class="text">
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <span>Item Name</span>
                                    <span>Qty.</span>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <span>Avg Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <span>Avg Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                </div>
                              </div><!-- col -->                       

                              <div class="col-md-5">
                                <h6 class="heading">Rates</h6>
                                <div class="text mb-4">
                                  <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Markup</span>
                                    <span><?php echo $row['markup']; ?></span>
                                  </div>
                                  <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Price</span>
                                    <span><?php echo $row['price']; ?></span>
                                  </div>
                                </div>

                                <h6 class="heading">Category</h6>
                                <div class="text mb-4"><?php echo $row['category']; ?></div>

                                <h6 class="heading">Styles</h6>
                                <div class="text mb-4"><?php echo $row['styles']; ?></div>

                                <h6 class="heading">Colors</h6>
                                <div class="text mb-4">
                                  Green, Silver, Brown, Neutral, White, Pink, Orange
                                </div>

                                <h6 class="heading">Months</h6>
                                <div class="text mb-4">
                                  May, June, July, August, September, October, November, December
                                </div>
                              </div><!-- col -->
                            </div><!-- row -->
                          </div>

                        </div><!-- col -->
                      </div><!-- row -->

                      <div id="after-edit" class="row after-edit">
                        <div class="col-lg-4 col-md-3">
                        <form action="sub-edit.php" method="post" enctype="multipart/form-data">
                          <div class="image-dv mb-5">
                            <h6 class="heading">Recipe Image</h6>
                            <div class="img">
                              <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                            </div>
                            <div><input type="hidden" name="source" value="Recipe"></div>
                          </div>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                          </div>

                          <div class="image-dv">
                            <h6 class="heading">Ingredient Images</h6>
                            <div class="row row-5">
                            <?php
                            $id =  $row['id'];
                            $ingsql2 = "SELECT * FROM `gallery` WHERE source='Recipe' AND parent_id='$id'";
                            $ingresult2 = mysqli_query($con, $ingsql2);
                            while($ing2 = mysqli_fetch_assoc($ingresult2)){ ?>
                              <div class="col-4">
                                <img src="<?php echo "img/".$ing2['name']; ?>" alt="Image">
                              </div>
                            <?php } ?>
                            </div>
                          </div>
                        </div><!-- col -->
                        
                        <div class="col-lg-8 col-md-9">
                          <div class="entry-txt mb-4">
                            <div class="row">
                              <div class="col-md-8">
                                <h6 class="heading">Recipe Name</h6>
                                <div class="text mb-4">
                                  <input type="text" class="form-control" value="<?php echo $row['recipe_name']; ?>" name="recipe_name">
                                </div>
                                <div><input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>"></div>

                                <h6 class="heading">Description</h6>
                                <div class="text mb-4">
                                  <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                                </div>

                                <div class="text mb-5 after-edit">
                                  <div class="alert flex-wrap flex-row alert-dark d-flex justify-content-between mb-2">
                                    <div>Ingredients</div>
                                    <div>Show Deleted</div>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <div>Item Name</div>
                                    <div>Qty.</div>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_one']; ?>" name="avg_cost_one" id="avg_cost_one"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_two']; ?>" name="avg_cost_two" id="avg_cost_two"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                                    <div>Avg Cost</div>
                                    <div>
                                      <span><input class="form-control" type="number" value="<?php echo $row['avg_cost_three']; ?>" name="avg_cost_three" id="avg_cost_three"></span>
                                      <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </div>
                                  </div>
                                </div>

                                <div class="my-5">
                                  <button class="btn btn-info" type="submit" value="Upload" name="img_upload">Add Ingredient</button>
                                </div>
                              </div><!-- col -->                       

                              <div class="col-md-4">
                                <h6 class="heading">Rates</h6>
                                <div class="text mb-4 after-edit">
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Cost</span>
                                    <span>$0.00</span>
                                  </div>
                                  <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Markup</span>
                                    <span><input type="text" value="<?php echo $row['markup']; ?>" name="markup" class="form-control"></span>
                                  </div>
                                  <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                    <span class="mr-3">Price</span>
                                    <span><input type="number" value="<?php echo $row['price']; ?>" name="price" class="form-control"></span>
                                  </div>
                                </div>

                                <h6 class="heading">Category</h6>
                                <div class="text mb-4">
                                <select name="category" id="category" class="form-control">
                                  <?php
                                  $category2 = array('Aisle Décor', 'Altar Pieces', 'Bouquets', 'Boutonnieres', 'Buffet Pieces', 'Cakes', 'Chairs', 'Chargers', 'Cocktail Tables', 'Corsages', 'Dance Floor', 'Dessert Tables', 'Drapery', 'Entrance Pieces', 'Entry Flowers', 'Escort Card Table', 'Focal Pieces', 'Furniture', 'Head Table', 'Lighting', 'Linens', 'Reception Tables', 'Rose Petals Decor', 'Services', 'Sweetheart Table', 'Tables', 'Utensils, Glassware & Dinnerware');
                                  for($i=0; $i<sizeof($category2); $i++){ ?>
                                  <option value="<?php echo $category2[$i] ?>"><?php echo $category2[$i] ?></option>
                                  <?php } ?>
                              </select>
                                </div>

                                <h6 class="heading">Styles</h6>
                                <div class="text mb-4 check-bx">
                                <?php
                                  $style2 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                                  for($i=0; $i<sizeof($style2); $i++){ ?>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="checkbox" name="styles" class="form-check-input" value="<?php echo $style2[$i] ?>"><?php echo $style2[$i] ?>
                                    </label>
                                  </div>
                                  <?php } ?>
                                </div>

                                <h6 class="heading">Show in Gallery</h6>
                                <div class="text mb-4">
                                  <select id="is_active" value="<?php echo $row['is_active']; ?>" name="is_active" class="form-control">
                                    <option value="Show">Show</option>
                                    <option value="Hide">Hide</option>
                                  </select>
                                </div>
                              </div><!-- col -->
                            </div><!-- row -->
                          </div>

                        </div><!-- col -->
                        </form>
                      </div><!-- row -->
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>


          </div>
        </div><!-- col -->
      </div><!-- row -->
    </div><!-- container-fluid -->
  </main>
</div><!-- wrapper -->

<script src="js/custom.js"></script>
</body>
</html>