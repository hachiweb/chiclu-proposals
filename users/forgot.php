<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
if(isset($_SESSION['username'])) {
header('location:../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Proposals ChicLu</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="stylesheet" href="../css/jquery.auto-complete.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="jquery-3.2.1.min.js" type="text/javascript"></script>

</head>

<body class="bg-gray">
<section class="container">
<div class="row mt-4">
<div class="col-sm-4"></div>
<div class="col-sm-4 pb-5">
<div class="bg-white text-center" style="border:2px solid rgb(220,220,220)">
<img src="../img/logo.png" alt="img not found">
<div id="message"></div>

<div id="get_email">

<div class="form-group ml-5 mr-5 mt-5">
<input type="text" class="form-control" name="username" id="username" placeholder="E-Mail Address">
</div>

<div class="text-center mt-4">
<button type="submit" name="submit" id="submit" style="background-color:rgb(0,158,193);color:white;" class="btn">Reset</button>
</div>

</div>

<div id="get_OTP" style="display:none;">

<div class="form-group ml-5 mr-5 mt-5">
<input type="number" class="form-control" name="OTP" id="OTP" placeholder="Enter OTP number ">
</div>

<div class="form-group ml-5 mr-5 mt-5">
<input type="password" class="form-control" name="password" id="password" placeholder="Enter New password ">
</div>

<div class="form-group ml-5 mr-5 mt-5">
<input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm password ">
</div>

<div class="text-center mt-4">
<button type="submit" name="reset" id="reset" style="background-color:rgb(0,158,193);color:white;" class="btn">Reset</button>
</div>

</div>

<div class="text-center mt-4 mb-5">
<span><a href="login.php" style="color:rgb(0,158,193)">Log IN </a></span>
<span><a href="sign_up.php" style="color:rgb(0,158,193)">| Sign UP</a></span>
</div>
</div>


</div>
<div class="col-sm-4"></div>



</div>


</section>
<script type="text/javascript">
    $(document).ready(function(){

        $("#submit").click(function(){
            var username = $("#username").val().trim();
        

            if( username != ""){
                $.ajax({
                    url:'process_forgot.php',
                    type:'POST',
                    data:{username:username,action: "generate_OTP"},
                    success:function(response){
                        var msg = "";
                        if(response == 1){
                            $("#get_email").hide();
                            $("#get_OTP").show();
                        }else{
                            msg = "Sorry, This email was incorrect.";
                        }
                        $("#message").html(msg);
                    }
                });
            }
        });

        $("#reset").click(function(){
            var OTP = $("#OTP").val().trim();
            var pswd = $("#password").val().trim();
            var cf_pswd = $("#confirm_password").val().trim();
            if (pswd != cf_pswd) {
                $("#message").html("Confirm Password Not Match!");
            }
            if( OTP != "" && pswd != "" && cf_pswd != "" && pswd == cf_pswd){
                $.ajax({
                    url:'process_forgot.php',
                    type:'POST',
                    data:{OTP:OTP,pswd:pswd,cf_pswd:cf_pswd,action: "verify_OTP"},
                    success:function(response){
                        var msg = "";
                        if(response == 1){
                            window.location = "login.php"
                        }else{
                            msg = "OTP Not Match!";
                        }
                        $("#message").html(msg);
                    }
                });
            }
        });

    });
</script>
</body>
</html>