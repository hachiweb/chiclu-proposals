<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
if(isset($_SESSION['username'])) {
header('location:../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Proposals ChicLu</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="stylesheet" href="../css/jquery.auto-complete.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="jquery-3.2.1.min.js" type="text/javascript"></script>

</head>

<body class="bg-gray">
<section class="container">
<div class="row mt-4">
<div class="col-sm-4"></div>
<div class="col-sm-4 pb-5">
<div class="bg-white text-center" style="border:2px solid rgb(220,220,220)">
<img src="../img/logo.png" alt="img not found">
<div id="message"></div>
<div class="form-group ml-5 mr-5 mt-5">
<input type="text" class="form-control" name="username" id="username" placeholder="E-Mail Address">
</div>
<div class="form-group  ml-5 mr-5">
<input type="password" class="form-control" name="password" id="password" placeholder="Password">
</div>
<div class="text-center mt-4">
<button type="submit" name="submit" id="submit" style="background-color:rgb(0,158,193);color:white;" class="btn">Sign In</button>
</div>
<div class="text-center mt-4 mb-5">
<span><a href="sign_up.php" style="color:rgb(0,158,193)">Sign Up </a></span>
<span><a href="forgot.php" style="color:rgb(0,158,193)">| Forgot password?</a></span>
</div>
</div>


</div>
<div class="col-sm-4"></div>



</div>


</section>
<script type="text/javascript">
    $(document).ready(function(){

        $("#submit").click(function(){
            var username = $("#username").val().trim();
            var password = $("#password").val().trim();

            if( username != "" && password != "" ){
                $.ajax({
                    url:'process_login.php',
                    type:'POST',
                    data:{username:username,password:password},
                    success:function(response){
                        var msg = "";
                        if(response == 1){
                            window.location = "../index.php";
                        }else{
                            msg = "<span class='alert alert-danger'> Sorry, your login was incorrect.</span>";
                        }
                        $("#message").html(msg);
                    }
                });
            }
        });

    });
</script>
</body>
</html>