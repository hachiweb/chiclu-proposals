<!DOCTYPE html>
<html lang="en">

<head>
    <title>sign_up form</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/custom.css">
    <!-- <link rel="stylesheet" href="css/jquery.auto-complete.css"> -->


    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
    <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
    <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
</head>

<body class="bg-gray">
    <section class="container">
        <div class="row mt-4">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 pb-5">
                <div class=" text-center" style="border:2px solid rgb(220,220,220);">
                    <form action="process_login.php" method="post">
                        <img src="../img/logo.png" alt="img not found">
                        <div class="text-center mt-4">
                            <h5>Your Information</h5>

                        </div>
                        <div class="form-group ml-5 mr-5 mt-5">

                            <input type="text" class="form-control" name="username" placeholder="First Name">
                        </div>
                        <div class="form-group  ml-5 mr-5">

                            <input type="password" class="form-control" name="password" placeholder="Last Name">
                        </div>
                        <div class="form-group  ml-5 mr-5">

                            <input type="password" class="form-control" name="password" placeholder="E-Mail-Address">
                        </div>
                        <div class="text-center mt-4">
                            <h5>Choose a Password</h5>

                        </div>
                        <div class="form-group  ml-5 mr-5">

                            <input type="password" class="form-control" name="password" placeholder="Enter a Password">
                        </div>
                        <div class="form-group  ml-5 mr-5">

                            <input type="password" class="form-control" name="password"
                                placeholder="Re-Enter a Password">
                        </div>
                        <div class="text-center mt-5 mb-5">
                            
                                <button id="" style="width:200px;" type="button" class="btn " style="background-color:rgb(0,158,193); color:white;">
                                    <span class="text-white">Sign-Up</span>

                                </button>
                           
                        </div>
                        <div class="text-center mt-5 mb-5">
                          <a href="login.php" class="text-success">Back to Login</a>  
                            
                        

                          
                       
                    </div>
                    </form>
                </div>


            </div>
            <div class="col-sm-3"></div>



        </div>


    </section>
</body>

</html>