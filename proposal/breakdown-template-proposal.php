<section class="bg-white px-4 p-3 template-sec breakdown-template-sec">
    <header class="temp-header mb-3">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="logo-dv">
                    <img src="img/logo.jpg" alt="Logo">
                </div>
            </div><!-- col -->

            <div class="col-md-6">
                <div class="text-dv text-right text-uppercase font-weight-light">
                    <div class="name h5">JAQUELYN BURKE</div>
                    <div class="email small">allenmrbin@gmail.com</div>
                </div>
            </div><!-- col -->
        </div><!-- row -->
    </header>

    <section class="temp-content-sec breakdown-content-sec">
        <h1 class="text-center mb-5 heading text-uppercase" id="">Conceptual Design</h1>

        <div class="temp-table breakdown-table-bx">
            <h4 class="heading text-uppercase d-inline-block">STAFFING / LABOR</h4>

            <table>
                <thead>
                    <tr>
                        <th>Staff Title</th>
                        <th>Count</th>
                        <th>Hours</th>
                        <th>Rate</th>
                        <th>Taxed</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Event Supervisor Test</td>
                        <td>1</td>
                        <td>3</td>
                        <td>$60.00</td>
                        <td>Y</td>
                        <td>$180.00</td>
                    </tr>
                    <tr>
                        <td>Event Staff Test</td>
                        <td>1</td>
                        <td>3</td>
                        <td>$30.00</td>
                        <td>Y</td>
                        <td>$90.00</td>
                    </tr>
                    <tr>
                        <td>Setup Team</td>
                        <td>1</td>
                        <td>3</td>
                        <td>$15.00</td>
                        <td>Y</td>
                        <td>$45.00</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="text-right" colspan="5"><strong>Total</strong></th>
                        <th>$315.00</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- breakdown-table-bx -->

        <div class="temp-table breakdown-table-bx mt-5">
            <h4 class="heading text-uppercase d-inline-block">STAFFING / LABOR</h4>

            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Subtotal</th>
                        <th></th>
                        <th>Fees</th>
                        <th></th>
                        <th>Discounts</th>
                        <th></th>
                        <th>Taxes</th>
                        <th></th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><strong>Products</strong></td>
                        <td>$187.80</td>
                        <td><strong>+</strong></td>
                        <td>$0.00</td>
                        <td><strong>-</strong></td>
                        <td>$0.00</td>
                        <td><strong>+</strong></td>
                        <td>$12.21</td>
                        <td><strong>=</strong></td>
                        <td>$200.01</td>
                    </tr>
                    <tr>
                        <td><strong>Services</strong></td>
                        <td>$0.00</td>
                        <td><strong>+</strong></td>
                        <td>$0.00</td>
                        <td><strong>-</strong></td>
                        <td>$0.00</td>
                        <td><strong>+</strong></td>
                        <td>$0.00</td>
                        <td><strong>=</strong></td>
                        <td>$0.00</td>
                    </tr>
                    <tr>
                        <td><strong>Labor</strong></td>
                        <td>$315.00</td>
                        <td><strong>+</strong></td>
                        <td>$0.00</td>
                        <td><strong>-</strong></td>
                        <td>$0.00</td>
                        <td><strong>+</strong></td>
                        <td>$20.48</td>
                        <td><strong>=</strong></td>
                        <td>$335.48</td>
                    </tr>
                    <tr>
                        <td><strong>Total</strong></td>
                        <td>$502.80</td>
                        <td><strong>+</strong></td>
                        <td>$0.00</td>
                        <td><strong>-</strong></td>
                        <td>$0.00</td>
                        <td><strong>+</strong></td>
                        <td>$32.69</td>
                        <td><strong>=</strong></td>
                        <td>$535.49</td>
                    </tr>
                    <tfoot>
                        <tr>
                            <th class="text-right pad" colspan="9"><strong>Grand Total</strong></th>
                            <th class="text-right pad">$535.49</th>
                        </tr>
                    </tfoot>
                </tbody>
            </table>
        </div><!-- breakdown-table-bx -->

    </section><!-- temp-content-sec -->
</section><!-- template-sec -->