<section class="bg-white px-4 p-3 template-sec concept-template-sec">
    <section class="temp-content-sec cover-content-sec">
        <div class="concept-edit-info">
            <div class="content-dv ml-auto mr-3">
                <div class="txt-bx text-uppercase text-center mb-4">
                    <h3 id="concept-input-1" class="m-0">Conceptual Design</h3>
                </div>

                <div class="txt-bx text-uppercase mb-3">
                    <h4 id="concept-input-2">Overall Feeling:</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 mb-4">
                <div class="concept-left">
                    <h4 class="heading">EVENT INSPIRATION</h4>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div class="image-full h-100">
                                <img src="img/scabiosa white.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                        <div class="col-md-6 mb-3">
                            <div class="image-full h-100">
                                <img src="img/scabiosa white.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                        <div class="col-md-6 mb-3">
                            <div class="image-full h-100">
                                <img src="img/scabiosa white.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                        <div class="col-md-6 mb-3">
                            <div class="image-full h-100">
                                <img src="img/scabiosa white.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                    </div><!-- row -->
                </div><!-- concept-left -->
            </div><!-- col -->

            <div class="col-md-4 mb-4">
                <div class="concept-right">
                    <h4 class="heading">FLORAL INSPIRATION</h4>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div class="image-full">
                                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                        <div class="col-md-6 mb-3">
                            <div class="image-full">
                                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                        <div class="col-md-6 mb-3">
                            <div class="image-full">
                                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                        <div class="col-md-6 mb-3">
                            <div class="image-full">
                                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                        <div class="col-md-6 mb-3">
                            <div class="image-full">
                                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                        <div class="col-md-6 mb-3">
                            <div class="image-full">
                                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
                            </div>
                        </div><!-- col -->
                    </div><!-- row -->
                </div>
            </div><!-- col -->
        </div><!-- row -->
    </section><!-- temp-content-sec -->
        </section><!-- template-sec -->