<section class="bg-white px-4 p-3 template-sec cover-template-sec">
    <header class="temp-header mb-3">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="logo-dv">
                    <img src="img/logo.jpg" alt="Logo">
                </div>
            </div><!-- col -->

            <div class="col-md-6">
                <div class="text-dv text-right text-uppercase font-weight-light">
                    <div class="name h5">JAQUELYN BURKE</div>
                    <div class="email small">allenmrbin@gmail.com</div>
                </div>
            </div><!-- col -->
        </div><!-- row -->
    </header>

    <section class="temp-content-sec cover-content-sec">
        <div class="row">
            <div class="col-md-8 mb-4">
                <div class="image-full h-100">
                    <img src="img/scabiosa white.jpg" alt="Image">
                </div>
            </div><!-- col -->

            <div class="col-md-4 mb-4">
                <div class="image-full h-100">
                    <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
                </div>
            </div><!-- col -->
        </div><!-- row -->

        <div class="temp-edit-info cover-edit-info">
            <div class="content-dv ml-auto mr-3">
                <div class="txt-bx text-uppercase text-center">
                    <h3 id="text-input-1" class="m-0"></h3>
                    <h3>&</h3>
                    <h3 id="text-input-2" class="m-0"></h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 mb-4">
                <div class="image-full h-100">
                    <img src="img/scabiosa white.jpg" alt="Image">
                </div>
            </div><!-- col -->
            
            <div class="col-md-4 mb-4">
                <div class="image-full h-100">
                    <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
                </div>
            </div><!-- col -->
        </div><!-- row -->
    </section><!-- temp-content-sec -->
        </section><!-- template-sec -->