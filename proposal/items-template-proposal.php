<section class="bg-white px-4 p-3 template-sec items-template-sec">
    <header class="temp-header mb-3">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="logo-dv">
                    <img src="img/logo.jpg" alt="Logo">
                </div>
            </div><!-- col -->

            <div class="col-md-6">
                <div class="text-dv text-right text-uppercase font-weight-light">
                    <div class="name h5">JAQUELYN BURKE</div>
                    <div class="email small">allenmrbin@gmail.com</div>
                </div>
            </div><!-- col -->
        </div><!-- row -->
    </header>

    <section class="temp-content-sec items-content-sec">
        <h1 class="text-center mb-5 heading text-uppercase" id="">Conceptual Design</h1>

        <div class="row justify-content-center">
            <div class="image-full circle-img">
                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
            </div>

            <div class="image-full circle-img">
                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
            </div>


            <div class="image-full circle-img">
                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
            </div>

            <div class="image-full circle-img">
                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
            </div>

            <div class="image-full circle-img">
                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
            </div>

            <div class="image-full circle-img">
                <img src="img/58f67fe76d649$!1200x.jpg" alt="Image">
            </div>                    
        </div><!-- row -->
    </section><!-- temp-content-sec -->
</section><!-- template-sec -->