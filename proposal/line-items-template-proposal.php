<section class="bg-white px-4 p-3 template-sec line-items-template-sec">
    <header class="temp-header mb-3">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="logo-dv">
                    <img src="img/logo.jpg" alt="Logo">
                </div>
            </div><!-- col -->

            <div class="col-md-6">
                <div class="text-dv text-right text-uppercase font-weight-light">
                    <div class="name h5">JAQUELYN BURKE</div>
                    <div class="email small">allenmrbin@gmail.com</div>
                </div>
            </div><!-- col -->
        </div><!-- row -->
    </header>

    <section class="temp-content-sec line-items-content-sec">
        <h4 class="heading text-uppercase">RECEPTION FLOWERS</h4>

        <div class="temp-table line-items-table">
            <table>
                <thead>
                    <tr>
                        <th width="150">Item</th>
                        <th width="80">Qty</th>
                        <th width="100">Price</th>
                        <th width="150"></th>
                        <th width="200" class="text-left">Description</th>
                        <th width="100">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <b>Sweetheart Table</b> <br>
                            Lori Bouquet
                        </td>
                        <td>1</td>
                        <td>$187.80</td>
                        <td class="p-2">
                            <span><img src="https://api.details.flowers/image/899726" alt="Image"></span>
                        </td>
                        <td class="text-left">
                            <div class="d-flex flex-wrap">
                                <div class="thumb">
                                    <img src="https://api.details.flowers/image/899726" alt="Image">
                                </div>
                                <div class="thumb">
                                    <img src="https://api.details.flowers/image/899726" alt="Image">
                                </div>
                                <div class="thumb">
                                    <img src="https://api.details.flowers/image/899726" alt="Image">
                                </div>
                                <div class="thumb">
                                    <img src="https://api.details.flowers/image/899726" alt="Image">
                                </div>
                            </div>
                            Dahlias White, Freesia White, Hydrangea White, Ranunculus White, Roses Cream Vendela 60cm & Roses Spray Cream Majolika
                        </td>
                        <td>$187.80</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="6"><b>Reception Flowers Total: &nbsp;$187.80</b></th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- temp-table -->
    </section><!-- temp-content-sec -->
</section><!-- template-sec -->