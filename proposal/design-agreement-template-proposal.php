<section class="bg-white px-4 p-3 template-sec design-agreement-template-sec">
    <header class="temp-header mb-3">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="logo-dv">
                    <img src="img/logo.jpg" alt="Logo">
                </div>
            </div><!-- col -->

            <div class="col-md-6">
                <div class="text-dv text-right text-uppercase font-weight-light">
                    <div class="name h5">JAQUELYN BURKE</div>
                    <div class="email small">allenmrbin@gmail.com</div>
                </div>
            </div><!-- col -->
        </div><!-- row -->
    </header>

    <section class="temp-content-sec design-agreement-content-sec">
        <div class="row mb-5">
            <div class="col-md-6">
            </div>
        </div>

        <h4 class="heading">CLIENT INFORMATION</h4>
        <div class="row mb-5">
            <div class="col-md-6">
                <table class="w-100">
                    <tbody>
                        <tr>
                            <td class="text"><span>Client Name:</span></td>
                            <td><span class="value">Alexey Chasin</span></td>
                        </tr>
                        <tr>
                            <td class="text"><span>Address:</span></td>
                            <td><span class="value">1063 Aloha dr</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <h4 class="heading">EVENT INFORMATION & SCHEDULE</h4>
        <div class="row">
            <div class="col-md-6">
                <table class="w-100">
                    <tbody>
                        <tr>
                            <td class="text"><span>Event Date:</span></td>
                            <td><span class="value"></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
        </section><!-- template-sec -->