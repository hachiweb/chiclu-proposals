<section class="bg-white px-4 p-3 template-sec terms-template-sec">
    <header class="temp-header mb-3">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="logo-dv">
                    <img src="img/logo.jpg" alt="Logo">
                </div>
            </div><!-- col -->

            <div class="col-md-6">
                <div class="text-dv text-right text-uppercase font-weight-light">
                    <div class="name h5">JAQUELYN BURKE</div>
                    <div class="email small">allenmrbin@gmail.com</div>
                </div>
            </div><!-- col -->
        </div><!-- row -->
    </header>

    <section class="temp-content-sec terms-content-sec">
        <h1 class="text-center mb-5 heading text-uppercase" id="">Conceptual Design</h1>

        <div class="entry-txt">
            <p><b>DEPOSITS & PAYMENTS:</b>  A RETAINER non-refundable retainer will be required to reserve COMPANY_NAME service prior to the scheduled event. Upon confirmation of EVENT DESIGN AGREEMENT, the remaining SECOND_PAYMENT_PERCENTAGE percent is required 120 days from the date of the event. The remaining balance due is required 21 calendar days prior to the scheduled event. Any additions thereafter will be due at time of order.</p>

            <p><b>CONVENIENCE FEE:</b>  We accept Visa, MasterCard & American Express for payments towards our services. Should you choose to pay by check or cash, your total invoice will be reduced by CONVIENIENCE_FEE percent. Please discuss payment options with your Event Supervisor.</p>

            <p><b>GUEST ATTENDANCE:</b>  COMPANY_NAME shall not be responsible for the number of guests in attendance at the subject event. The determination of the number of guests to invite or to prepare for is the sole responsibility of the Customer. The prices quoted are final.</p>

            <p><b>GUEST COUNTS:</b>  A minimum guest count will be required 21 calendar days prior to the scheduled event. Based on availability and our ability to react, increases may be accepted up to 5 days prior to the event and may result in additional cost. The Customer may not change the number of guests or the quantities of florals or services without the prior written consent of COMPANY_NAME, which consent may be withheld in COMPANY_NAME sole discretion. Any increases in the number of guests, the quantities of florals or services will result in the increased cost to the customer, which the customer agrees to pay.</p>

            <p><b>CUSTOMER CANCELLATION:</b>  This agreement may be terminated by written notification to COMPANY_NAME only. If received less than sixty (60) calendar days in advance of your scheduled event, fifty percent (50) of the expected charges will apply. However, COMPANY_NAME will honor 50 percent of the total deposit(s) and charges paid towards a future event, dated within one calendar year from the date of the originally scheduled event. If not Customer agrees to full forfeiture of deposits. If cancellation is within ten (10) days of your scheduled event, one-hundred percent (100) of the expected charges will apply. Payment for liquidated damages due as a result of the cancellation of the agreement shall be made at the time of cancellation.</p>

            <p><b>SUBSTITUTIONS:</b>  COMPANY_NAME reserves the right to make substitutions in the event the flowers received are not of the quality suitable for your wedding or special event. In this event, the integrity of the proposed color scheme will be maintained and flowers of equivalent value will be used.</p>

            <p><b>ARTISTIC LICENSE:</b>  By commissioning COMPANY_NAME to design and create floral pieces for your event, we are entrusting the artists and representatives to utilize their design skills and capabilities shown during consultations and other events. We entrust COMPANY_NAME to choose all floral varieties, color schemes and textures that will best showcase the ideas for the event. By signing this agreement, we allow COMPANY_NAME full Artistic License to create distinctive and unique floral pieces and table-scapes.</p>

            <p><b>PHOTOGRAPHY & PERMITTED USES:</b>  Parties agree that COMPANY_NAME may send photographers to photograph specific works of art created at locations specified throughout contract unless otherwise discussed. COMPANY_NAME may reproduce, publish, exhibit and otherwise use the images created hereunder, without specific identifications or persons or events (1) as samples of our work to be shown to prospective clients, either in photographic or electronic form, including our website, (2) for instructional purposes to be shared with aspiring florists and working photographers, and (3) for generic institutional purposes, including use in published interviews and competitions. Any such use shall be judicious and consistent with the highest standards of taste and judgment.</p>
        </div>
    </section><!-- temp-content-sec -->
</section><!-- template-sec -->