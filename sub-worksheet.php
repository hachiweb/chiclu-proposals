<?php
  session_start(); 
  if(empty($_SESSION["username"])){
    header("location:users/login.php");
    exit();
  }
  else{
    $username = $_SESSION["username"];
  }
 
  include 'db.php';
  if (isset($_POST["submit"])) {
    $type = "New Templates";
    $name = $_POST["name"];
    $tags = $_POST["tags"];
    $allcat = $_POST["allcat"];
    $description = $_POST["description"];
    $filtred = implode(",",$filtred);
    // item 
    $item_name = implode(",",$_POST["item_name"]);
    $item_qty = implode(",",$_POST["item_qty"]);
    $item_price = implode(",",$_POST["item_price"]);
    
    //Creating category object
    $cat_name = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:curr_cat_name)/',$key))
        $cat_name[] = $value;

    $image_name = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:item_image)/',$key))
        $image_name[][$key] = $value;

    $cat_item_name = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:item_name)/',$key))
        $cat_item_name[][$key] = $value;

    $cat_item_qty = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:item_qty)/',$key))
        $cat_item_qty[][$key] = $value;


    $cat_item_price = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:item_price)/',$key))
        $cat_item_price[][$key] = $value;

    $line_total = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:line_total)/',$key))
        $line_total[][$key] = $value;
    
    $line_item_tax = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:line_item_tax)/',$key))
        $line_item_tax[][$key] = $value;
    
    $ingredient_qty = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:ingredient_qty)/',$key))
        $ingredient_qty[][$key] = $value;
    
    $ingredient_name = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:ingredient_name)/',$key))
        $ingredient_name[][$key] = $value;

    $ingredient_tax = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:ingredient_tax)/',$key))
        $ingredient_tax[][$key] = $value;

    $ingredient_cost = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:ingredient_cost)/',$key))
        $ingredient_cost[][$key] = $value;

    $ingredient_price = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:ingredient_price)/',$key))
        $ingredient_price[][$key] = $value;
    
    $ingredient_markup = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:total_ingredient_markup)/',$key))
        $ingredient_markup[][$key] = $value;

    $total_ingredient_cost = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:total_ingred_cost)/',$key))
        $total_ingredient_cost[][$key] = $value;

    $total_ingredient_price = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:total_ingred_price)/',$key))
        $total_ingredient_price[][$key] = $value;

    $ingredient_description = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:ingredient_description)/',$key))
        $ingredient_description[][$key] = $value;

    $ingredient_notes = array();
    foreach($_POST as $key => $value)
      if(preg_match('/(?:ingredient_notes)/',$key))
        $ingredient_notes[][$key] = $value;


    $i = 0;
    foreach($cat_item_name as $key => $val){
      foreach($val as $key2 => $val2){    
        foreach($val2 as $key3 => $val3){
          $item['name'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }    

    $i = 0;
    foreach($cat_item_qty as $key => $val){
      foreach($val as $key2 => $val2){    
        foreach($val2 as $key3 => $val3){
          $item['qty'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($cat_item_price as $key => $val){
      // unset($val['line_item_ingredient_price']);
      foreach($val as $key2 => $val2){  
        foreach($val2 as $key3 => $val3){
          $item['price'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($line_total as $key => $val){
      foreach($val as $key2 => $val2){  
        foreach($val2 as $key3 => $val3){
          $item['line_total'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($line_item_tax as $key => $val){
      foreach($val as $key2 => $val2){  
        foreach($val2 as $key3 => $val3){
          $item['line_item_tax'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($image_name as $key => $val){
      foreach($val as $key2 => $val2){  
        foreach($val2 as $key3 => $val3){
          $item['image'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($ingredient_qty as $key => $val){
      foreach($val as $key2 => $val2){  
        foreach($val2 as $key3 => $val3){
          $item['ingredient']['qty'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($ingredient_name as $key => $val){
      foreach($val as $key2 => $val2){  
        foreach($val2 as $key3 => $val3){
          $item['ingredient']['name'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($ingredient_cost as $key => $val){
      foreach($val as $key2 => $val2){  
        foreach($val2 as $key3 => $val3){
          $item['ingredient']['cost'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($ingredient_price as $key => $val){
      foreach($val as $key2 => $val2){  
        foreach($val2 as $key3 => $val3){
          $item['ingredient']['price'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($ingredient_tax as $key => $val){
      foreach($val as $key2 => $val2){  
        foreach($val2 as $key3 => $val3){
          $item['ingredient']['tax'][$i][$key3] = $val3;
        }
        $i+=1;
      }
    }

    $i = 0;
    foreach($total_ingredient_cost as $key => $val){
      foreach($val as $key2 => $val2){  
        $item['ingredient']['total_cost'][$i][$key2] = $val2;
      } 
      $i+=1;
    }

    $i = 0;
    foreach($ingredient_markup as $key => $val){
      foreach($val as $key2 => $val2){  
        $item['ingredient']['markup'][$i][$key2] = $val2;
      }
      $i+=1;
    }

    $i = 0;
    foreach($total_ingredient_price as $key => $val){
      foreach($val as $key2 => $val2){  
        $item['ingredient']['total_price'][$i][$key2] = $val2;
      }
      $i+=1;
    }

    $i = 0;
    foreach($ingredient_description as $key => $val){
      foreach($val as $key2 => $val2){  
        $item['ingredient']['description'][$i][$key2] = $val2;
      }
      $i+=1;
    }

    $i = 0;
    foreach($ingredient_notes as $key => $val){
      foreach($val as $key2 => $val2){  
        $item['ingredient']['notes'][$i][$key2] = $val2;
      }
      $i+=1;
    }

    $k = 0;
    foreach($cat_name as $key => $val){
      $category[$key]['category_name'] = $val;
      foreach($item['name'][$key] as $key2=>$val2){
        $category[$key]['item_name'][] = $val2;
      }
      foreach($item['qty'][$key] as $key2=>$val2){
        $category[$key]['item_qty'][] = $val2;
      }
      foreach($item['price'][$key] as $key2=>$val2){
        $category[$key]['item_price'][] = $val2;
      }
      foreach($item['line_total'][$key] as $key2=>$val2){
        $category[$key]['line_total'][] = $val2;
      }
      foreach($item['line_item_tax'][$key] as $key2=>$val2){
        $category[$key]['line_item_tax'][] = $val2;
      }
      foreach($item['image'][$key] as $key2=>$val2){
        $category[$key]['item_image'][] = $val2;
      }
      $m = 0; 
      foreach($item['name'][$key] as $key2=>$val2){ 
        foreach($item['ingredient']['qty'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['qty'][$m][] = $val3;
        }
        foreach($item['ingredient']['name'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['name'][$m][] = $val3;
        }
        foreach($item['ingredient']['cost'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['cost'][$m][] = $val3;
        }
        foreach($item['ingredient']['price'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['price'][$m][] = $val3;
        }
        foreach($item['ingredient']['tax'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['tax'][$m][] = $val3;
        }
        foreach($item['ingredient']['total_cost'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['total_cost'][$m][] = $val3;
        }
        foreach($item['ingredient']['markup'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['markup'][$m][] = $val3;
        }
        foreach($item['ingredient']['total_price'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['total_price'][$m][] = $val3;
        }
        foreach($item['ingredient']['description'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['description'][$m][] = $val3;
        }
        foreach($item['ingredient']['notes'][$k] as $key3 => $val3){
          $category[$key]['ingredient']['notes'][$m][] = $val3;
        }
        $m++;
        $k++;
      }
    }
    $category = json_encode($category);
    // echo '<pre>';
    // print_r($category);   
    
    // Table_seating 
    $Estimated_Number_of_Guests = $_POST["Table_seating"]["Estimated_Number_of_Guests"];
    $People_at_Head_Table = $_POST["Table_seating"]["People_at_Head_Table"];
    $People_at_Sweetheart_Table = $_POST["Table_seating"]["People_at_Sweetheart_Table"];
    $Guests_Per_Table = $_POST["Table_seating"]["Guests_Per_Table"];
    $Guest_Tables_Needed = $_POST["Table_seating"]["Guest_Tables_Needed"];

    // discount 
    $discount_name = implode(",",$_POST["discount_name"]);
    $discount_apply_to = implode(",",$_POST["discount_apply_to"]);
    $discount_apply_to_2 = implode(",",$_POST["discount_apply_to_2"]);  
    $discount_type = implode(",",$_POST["discount_type"]);
    $discount_amt = implode(",",$_POST["discount_amt"]);

    // fees
    $fee_name = implode(",",$_POST["fee_name"]);  
    $fee_apply_to = implode(",",$_POST["fee_apply_to"]);
    $fee_type = implode(",",$_POST["fee_type"]);
    $fee_amt = implode(",",$_POST["fee_amt"]);
    $fee_tax = implode(",",$_POST["fee_tax"]);

    // Tax
    $tax_product = $_POST["tax"]["product"];
    $tax_service = $_POST["tax"]["service"];
    $tax_labor = $_POST["tax"]["labor"];

    // staff
    $staff_title = implode(",",$_POST["staff_title"]);  
    $staff_count = implode(",",$_POST["staff_count"]);
    $staff_hours = implode(",",$_POST["staff_hours"]);
    $staff_rate = implode(",",$_POST["staff_rate"]);
    $staff_tax = implode(",",$_POST["staff_tax"]);

    // colors
    $colors = implode(",",$_POST["Colors"]);

    // user details 
    $id = $_SESSION["ID"];

    // inserting data
    if ($type == 'New Version') {
    $sql = "DELETE FROM worksheet WHERE type = 'New Version' AND user_id = '$id' ";
    mysqli_query($con, $sql);
    } 

    $sql = "INSERT INTO `worksheet` (`user_id`, `type`, `item_name`, `item_qty`, `item_price`, `Estimated_Number_of_Guests`, `People_at_Head_Table`, `People_at_Sweetheart_Table`, `Guests_Per_Table`, `Guest_Tables_Needed`, `discount_name`, `discount_apply_to`, `discount_apply_to_2`, `discount_type`, `discount_amt`, `fee_name`, `fee_apply_to`, `fee_type`, `fee_amt`, `fee_tax`, `tax_product`, `tax_service`, `tax_labor`, `staff_title`, `staff_count`, `staff_hours`, `staff_rate`, `staff_tax`, `name`, `description`, `tags`, `favorite_colors`,`category`) VALUES ('$id', '$type' ,'$item_name' ,'$item_qty' ,'$item_price' ,'$Estimated_Number_of_Guests' ,'$People_at_Head_Table' ,'$People_at_Sweetheart_Table' ,'$Guests_Per_Table' ,'$Guest_Tables_Needed' ,'$discount_name' ,'$discount_apply_to' ,'$discount_apply_to_2' ,'$discount_type' ,'$discount_amt' ,'$fee_name' ,'$fee_apply_to' ,'$fee_type' ,'$fee_amt' ,'$fee_tax' ,'$tax_product' ,'$tax_service' ,'$tax_labor' ,'$staff_title' ,'$staff_count' ,'$staff_hours' ,'$staff_rate' ,'$staff_tax','$name','$description','$tags','$colors', '$category')";
    if (mysqli_query($con, $sql)) {
    header("location: thankyou.php");
    } else {
    echo mysqli_error($con); exit();
    header("location: error.php");
    }
   
  }
  else{
    header("location: error.php");
  }
?>