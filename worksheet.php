<?php
error_reporting(0);
$page ="Worksheet";
include('header.php');
$id = $_SESSION["ID"];
$template_id =  (empty($_POST['TEMPID']))?"0":$_POST['TEMPID'];
?>
<!--header added-->
<section class="container-fluid">
    <div class="row row-5">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="worksheet-left-dv">
                <?php
                include('design_board.php');
                ?>
            </div><!-- worksheet-left-dv -->
        </div>
        <?php 
        if ($template_id<1) {
            $sql = "SELECT * FROM `worksheet` WHERE `type` = 'New Version' AND `user_id`='$id'";
        } else {
            $sql = "SELECT * FROM `worksheet` WHERE `type` = 'New Templates' AND `id`='$template_id'";
        }
        $result = $con->query($sql);
        $row = $result->fetch_assoc();
            // For Templates
        $sqlTemplates = "SELECT * FROM `worksheet` WHERE `type` = 'New Templates' AND `user_id`='$id'";
        $resultTemplates = $con->query($sqlTemplates);
            // For USERNAME
        $sqlUsername = "SELECT * FROM `event_questionare` WHERE `event_id`='$id'";
        $resultUsername = $con->query($sqlUsername);
        $rowUsername = $resultUsername->fetch_assoc();
        $username = $rowUsername['bride_name']. ' & '.$rowUsername['groom_name'];
            // item 
        $item_name  = explode(",",$row['item_name']);
        $item_qty   = explode(",", $row['item_qty']);
        $item_price = explode(",",$row['item_price']);
            // discount 
        $discount_name = explode(",",$row['discount_name']);
        $discount_apply_to = explode(",",$row['discount_apply_to']);
        $discount_apply_to_2 = explode(",",$row['discount_apply_to_2']);
        $discount_type = explode(",",$row['discount_type']);
        $discount_amt = explode(",",$row['discount_amt']);
            // fees
        $fee_name = explode(",",$row['fee_name']);
        $fee_apply_to = explode(",",$row['fee_apply_to']);
        $fee_type = explode(",",$row['fee_type']);
        $fee_amt = explode(",",$row['fee_amt']);
        $fee_tax = explode(",",$row['fee_tax']);
            // staff
        $staff_title = explode(",",$row['staff_title']);
        $staff_count = explode(",",$row['staff_count']);
        $staff_hours = explode(",",$row['staff_hours']);
        $staff_rate = explode(",",$row['staff_rate']);
        $staff_tax = explode(",",$row['staff_tax']);
            // colors
        $favorite_colors = explode(",",$row['favorite_colors']);
            //Single Value
        $name = $row['name'];
        $description = $row['description'];
        $tags = $row['tags'];
            // Table_seating 
        $Estimated_Number_of_Guests = $row['Estimated_Number_of_Guests'];
        $People_at_Head_Table = $row['People_at_Head_Table'];
        $People_at_Sweetheart_Table = $row['People_at_Sweetheart_Table'];
        $Guests_Per_Table = $row['Guests_Per_Table'];
        $Guest_Tables_Needed = $row['Guest_Tables_Needed'];
            // Tax
        $tax_product = $row['tax_product'];
        $tax_service = $row['tax_service'];
        $tax_labor = $row['tax_labor'];
        $category = json_decode($row['category'], True); 
        // echo '<pre>';
        // print_r($category);
        // $category = (!empty($row['category'])?$row['category']:"Category Group");
        ?>
        <div class="col-lg-8 col-md-8 col-sm-8 mt-3">
            <div class="worksheet-right-dv">
                <div class="bg_color_set title-dv p-3">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="text-left">
                                <p class="text-white h4">Design Worksheet</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="text-center">
                                <p class="text-white h4">Grand Total <?=(!empty($totalp))?"$".$totalp:"$0.00";?></p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="text-right">
                                <div class="dropdown float-right">
                                    <button class="btn btn-info dropdown-toggle border-white text-white corner_set"
                                    style="width:150px;" type="button" id="dropdownMenu2" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-cog fa-fw"></i>Options
                                </button>
                                <div class="dropdown-menu mr-5" aria-labelledby="dropdownMenu2">
                                    <button class="dropdown-item border-white hover_set" type="button" data-toggle="modal" data-target="#myModalC">
                                        <i class="fa fa-spinner" aria-hidden="true"></i><a href="/worksheet.php" id="load-default-temp" class="text-dark">&nbsp;Load Default</a>
                                    </button>
                                    <button class="dropdown-item border-white hover_set text-dark" type="button"
                                    data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-list-alt"></i> Load Template
                                </button>
                            </div>
                        </div>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog modal-dialog-centered modal-xl">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header bg_color_set">
                                        <h4 class="modal-title text-white"> Worksheet Templates</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <form action="" method="POST">
                                        <div class="modal-body">
                                            <div class="row mt-3">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="bg-white">
                                                        <div class="">
                                                            <div class="border-bottom">
                                                                <span class="float-left">Available Templates</span>
                                                                <div class="text-right clicks">
                                                                    <span class="ingredients_eye"><i
                                                                        class="fa fa-eye"
                                                                        aria-hidden="true"></i>
                                                                    </span>
                                                                    <span class="ingredients_show">Show</span>
                                                                    <span class="ingredients_delete">Deleted</span>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown-bx">
                                                                <table
                                                                class="table table-striped table-list m-0 text-center">
                                                                <thead>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Name</th>               
                                                                        <th>Description</th>
                                                                        <th>Tags</th>
                                                                        <th>Created</th>   
                                                                        <th>By</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody class="body-clone-payment">
                                                                    <?php 
                                                                    while ($rowTemplates = $resultTemplates->fetch_assoc()) {?>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" value="<?=$rowTemplates['id'];?>" id="defaultRadio" name="TEMPID">
                                                                            </td>
                                                                            <td><?=$rowTemplates['name'];?></td>
                                                                            <td><?=$rowTemplates['description'];?></td>
                                                                            <td><?=$rowTemplates['tags'];?></td>
                                                                            <td><?= date('d M, Y h:i:sa',strtotime($rowTemplates['created_at']));?></td>
                                                                            <td><?=$username;?></td>
                                                                            <td>
                                                                                <span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div><!-- dropdown-bx -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="border-top mx-3">
                                        <div class=" text-center">
                                            <button type="submit" class="btn btn-info corner_set btn_color">Select Template</button></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="worksheet-scroll-dv">
            <form method="POST" action="sub-worksheet.php">
                <input type="hidden" id="curr_cat_ref" name="curr_cat_ref" />                       <input type="hidden" id="curr_design_meta" />                       
                <div class="bg-white pt-5 dropdown-insert-sec">
                <?php
                    if(isset($category) && !empty($category)){
                        foreach($category as $key=>$val) {
                ?>                                                
                    <section class="dropdown-click-sec p-2 mb-4 dropdown-cat-group" id="cat-sec">
                        <div class="heading px-3">
                            <span class="dropdown-click">
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                            </span>
                            <a href="javascript:void(0);" class="text-black cat-rename">
                                <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                                <span class="cat-name"><?=$val['category_name'];?></span>
                                <input type="hidden" class="curr_cat_name" name="curr_cat_name" value="Category Group"/>
                            </a>
                            <span class="float-right">
                                <span class="delete-clk">
                                    <i class="fa fa-trash fa-fw"></i>
                                    <span class="txt">Remove Section</span>
                                </span>
                            </span>
                        </div>
                        <div class="dropdown-bx">
                            <table class="table table-striped table-list m-0 text-center">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Item Name</th>
                                        <th></th>
                                        <th>Qty</th>
                                        <th>Est Price</th>
                                        <!-- <th>Lock</th> -->
                                        <th>Total</th>
                                        <th>Tax</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="body-clone-category">
                                <input type="hidden" class="curr_unique_id" value=""/>
                                        <?php 
                                            $item_length = sizeof($val['item_name']);
                                            for($i=0; $i<$item_length; $i++){                   
                                        ?> 
                                        <tr class="ui-sortable-handle">
                                            <td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>
                                            <td>
                                                <span class="design-board-image-click" id="design-meta_1" data-toggle="modal" data-target="#designBModal">
                                                <img src="<?=empty($val['item_image'][$i])?"":$val['item_image'][$i]; ?>" class="fa fa-picture-o" height="60" width="80">
                                                </span>
                                            </td>
                                          
                                            <td class="rename-item">
                                                <button type="button" class="item-name-bx">
                                                    <span
                                                    class="item-name-txt"><?=empty($val['item_name'][$i])?"Bride":$val['item_name'][$i]; ?></span>
                                                    <input type="text" name="item_name[]"  class="item-name-input"
                                                    oninput="set_val(this)" value="<?=$val['item_name'][$i]; ?>">
                                                    <input type="hidden" id="curr_user_id" name="curr_user_id" value="<?=$id?>" />

                                                </button>
                                            </td>
                                            <td><span class="ingredients-toggle"><i class="fa fa-plus" aria-hidden="true"></i></span></td>
                                            <td><input type="text" name="item_qty[]" class="qty"
                                                value="<?=empty($val['item_qty'][$i])?"1":$val['item_qty'][$i]; ?>"></td>
                                                <td><input type="text" name="item_price[]" class="price"
                                                    value="<?=empty($val['item_price'][$i])?"$0.00":$val['item_price'][$i]; ?>"></td>
                                                <!-- <td>
                                                    <span class="lock"><i class="fa fa-unlock" aria-hidden="true"></i></span>
                                                </td> -->
                                                <td class="total"><?=(!empty($c))?$c:"$0.00"; ?></td>
                                                <input type="hidden" name="line_total[]" value="0.00">
                                                <td>TP</td>
                                                <input type="hidden" name="line_item_tax[]" value="TP"/>
                                                <td><span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span>
                                            </td>
                                            </tr>
                                            <tr class="none-display initial_ingredient">
                                                <td colspan="10" class="bg-greenish">
                                                <div class="text-left ingredients-section">
                                                <div class="row">
                                                <div class="col-lg-3">
                                                <div class="left-image-bx mb-5">
                                                <h6 class="heading item-heading"><?=$val['item_name'][$i]; ?></h6>
                                                <div class="img-dv">
                                                <img src="<?=empty($val['item_image'][$i])?"":$val['item_image'][$i]; ?>" alt="Image">
                                                </div>
                                                </div>
                                                </div>
                                                <div class="col-lg-9">
                                                <section class="dropdown-click-sec-inner p-2 mb-4" id="ingredient-sec">
                                                <div class="heading px-3">
                                                <span class="dropdown-click-inner">
                                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                </span>
                                                <a href="javascript:void(0);" class="text-black">
                                                <span>Ingredients</span>
                                                </a>
                                                <span class="float-right">
                                                <span class="delete-clk-inner"><i class="fa fa-trash fa-fw"></i></span>
                                                </span>
                                                </div>

                                                <div class="dropdown-bx">
                                                <table class="table table-striped table-list m-0 text-center">
                                                <thead>
                                                <tr>
                                                <th></th>
                                                <th>Qty</th>
                                                <th>Item Name</th>
                                                <th>Cost</th>
                                                <th>Price</th>
                                                <th>Tax</th>
                                                <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                    $ingredients = $val['ingredient'];
                                                    $ingredient_length = sizeof($ingredients['name'][$i]);
                                                for($j=0; $j<$ingredient_length; $j++){
                                                    // echo '<pre>';
                                                    // print_r($ingredients['qty'][$i][$j]);    
                                                ?>
                                                <tr class="ui-sortable-handle tr-inner">
                                                <td><span><i class="fa fa-plus" aria-hidden="true"></i></span></td>
                                                <td><input type="text" class="ingredient_qty" name="ingredient_qty[]"  value="<?=$ingredients['qty'][$i][$j]?>"></td>
                                                <td><?=$ingredients['name'][$i][$j]?></td>
                                                <td><input type="text" name="total_ingred_cost"  value="<?=$ingredients['cost'][$i][$j]?>"></td>
                                                <td><?=$ingredients['price'][$i][$j]?></td>
                                                <td><?=$ingredients['tax'][$i][$j]?></td>
                                                <td><span class="cross-td-inner"><i class="fa fa-times" aria-hidden="true"></i></span></td>
                                                </tr>
                                                <?php  } ?>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                <td colspan="2">Total Cost: </td>
                                                <td><?=$ingredients['total_cost'][$i][0]?></td>
                                                <td>Markup %</td>
                                                <td><input type="text" class="ingredient_markup" name="total_ingredient_markup"  value="<?=$ingredients['markup'][$i][0]?>"></td>
                                                <td>Total Price:</td>
                                                <td><input type="text" name="total_ingred_price"  value="<?=$ingredients['total_price'][$i][0]?>" disabled="disable"></td>
                                                </tr>
                                                </tfoot>
                                                </table>
                                                </div>
                                                </section>

                                                <section class="dropdown-click-sec-inner p-2 mb-4">
                                                <div class="heading px-3">
                                                <span class="dropdown-click-inner">
                                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                </span>
                                                <a href="javascript:void(0);" class="text-black">
                                                <span>Description</span>
                                                </a>
                                                <span class="float-right">
                                                <span class="delete-clk-inner">
                                                <i class="fa fa-trash fa-fw"></i>
                                                </span>
                                                </span>
                                                </div>

                                                <div class="text">
                                                <textarea class="form-control" rows="5"><?=$ingredients['description'][$i][0]?></textarea>
                                                </div>
                                                </section>

                                                <section class="dropdown-click-sec-inner p-2 mb-4">
                                                <div class="heading px-3">
                                                <span class="dropdown-click-inner">
                                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                </span>
                                                <a href="javascript:void(0);" class="text-black">
                                                <span>Notes</span>
                                                </a>
                                                <span class="float-right">
                                                <span class="delete-clk-inner">
                                                <i class="fa fa-trash fa-fw"></i>
                                                </span>
                                                </span>
                                                </div>

                                                <div class="text">
                                                <textarea class="form-control" rows="5"><?=$ingredients['notes'][$i][0]?></textarea>
                                                </div>
                                                </section>
                                                </div>
                                                </div>
                                                </div>
                                                </td>
                                            </tr> 
                                            <?php } ?>
                                        <tfoot>
                                            <tr>
                                                <td class="p-0" colspan="10">
                                                    <button type="button" class="btn w-100 add-row">+Add New Row</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-right" colspan="8">Section Subtotal:</th>
                                                <th class="subtotal">$0.00</th>
                                            </tr>
                                            <tr>
                                                <td class="p-0" colspan="10">
                                                    <div class="text-center p-3">
                                                        <div class="add-sec-plus addplus-cat" data-toggle="modal" data-target="#newModalPlusCat"></div>
                                                    </div>
                                                    <div class="modal fade" id="newModalPlusCat" tabindex="-1" role="dialog" aria-labelledby="newModalPlusCat" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                                                            <?php include('worksheet-modal.php'); ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </tbody>
                                </table>
                            </div><!-- dropdown-bx -->
                        </section><!-- dropdown-click-sec -->
                        <?php
                                }
                            }
                            else{       
                        ?>
                        <section class="dropdown-click-sec p-2 mb-4 dropdown-cat-group" id="cat-sec">
                        <div class="heading px-3">
                            <span class="dropdown-click">
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                            </span>
                            <a href="javascript:void(0);" class="text-black cat-rename">
                                <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                                <span class="cat-name">Category Group</span>
                                <input type="hidden" class="curr_cat_name" name="curr_cat_name" value="Category Group"/>
                            </a>
                            <span class="float-right">
                                <span class="delete-clk">
                                    <i class="fa fa-trash fa-fw"></i>
                                    <span class="txt">Remove Section</span>
                                </span>
                            </span>
                        </div>
                        <div class="dropdown-bx">
                            <table class="table table-striped table-list m-0 text-center">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Item Name</th>
                                        <th></th>
                                        <th>Qty</th>
                                        <th>Est Price</th>
                                        <!-- <th>Lock</th> -->
                                        <th>Total</th>
                                        <th>Tax</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="body-clone-category">
                                <input type="hidden" class="curr_unique_id" value=""/>
                                        <tr class="ui-sortable-handle">
                                            <td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>
                                            <td>
                                                <span class="design-board-image-click" data-toggle="modal" id="design-meta_1" data-target="#designBModal">
                                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </span>
                                            </td>
                                            <td class="rename-item">
                                                <button type="button" class="item-name-bx">
                                                    <span
                                                    class="item-name-txt">Bride</span>
                                                    <input type="text" name="item_name[]"  class="item-name-input"
                                                    oninput="set_val(this)" value="Bride">
                                                    <input type="hidden" id="curr_user_id" name="curr_user_id" value="<?=$id?>" />
                                                </button>
                                            </td>
                                            <td><span class="ingredients-toggle"><i class="fa fa-plus" aria-hidden="true"></i></span></td>
                                            <td><input type="text" name="item_qty[]" class="qty"
                                                value="1"></td>
                                                <td><input type="text" name="item_price[]" class="price"
                                                    value="$0.00"></td>
                                                <!-- <td>
                                                    <span class="lock"><i class="fa fa-unlock" aria-hidden="true"></i></span>
                                                </td> -->
                                                <td class="total"><?=(!empty($c))?$c:"$0.00"; ?></td>
                                                <input type="hidden" name="line_total[]" value="0.00">
                                                <td>TP</td>
                                                <input type="hidden" name="line_item_tax[]" value="TP"/>
                                                <td><span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span>
                                                </td>
                                            </tr>
                                            <tr class="none-display initial_ingredient">
                                                <td colspan="10" class="bg-greenish">
                                                <div class="text-left ingredients-section">
                                                <div class="row">
                                                <div class="col-lg-3">
                                                <div class="left-image-bx mb-5">
                                                <h6 class="heading item-heading">Delude Inspiration</h6>
                                                <div class="img-dv">
                                                <img src="img/2019_bridescom-Editorial_Images-06-Peony-Wedding-Bouquets-Large-Peony-Bouquet-Refresh-Catherine-Hall-Studios.jpg" alt="Image">
                                                </div>
                                                </div>
                                                </div>
                                                <div class="col-lg-9">
                                                <section class="dropdown-click-sec-inner p-2 mb-4" id="ingredient-sec">
                                                <div class="heading px-3">
                                                <span class="dropdown-click-inner">
                                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                </span>
                                                <a href="javascript:void(0);" class="text-black">
                                                <span>Ingredients</span>
                                                </a>
                                                <span class="float-right">
                                                <span class="delete-clk-inner"><i class="fa fa-trash fa-fw"></i></span>
                                                </span>
                                                </div>

                                                <div class="dropdown-bx">
                                                <table class="table table-striped table-list m-0 text-center">
                                                <thead>
                                                <tr>
                                                <th></th>
                                                <th>Qty</th>
                                                <th>Item Name</th>
                                                <th>Cost</th>
                                                <th>Price</th>
                                                <th>Tax</th>
                                                <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="ui-sortable-handle tr-inner">
                                                <td><span><i class="fa fa-plus" aria-hidden="true"></i></span></td>
                                                <td><input type="text" class="ingredient_qty" name="ingredient_qty[]"  value="1"></td>
                                                <td>Text Name</td>
                                                <td><input type="text" name="total_ingred_cost"  value="$0.00"></td>
                                                <td>$17.00</td>
                                                <td>TP</td>
                                                <td><span class="cross-td-inner"><i class="fa fa-times" aria-hidden="true"></i></span></td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                <td colspan="2">Total Cost: </td>
                                                <td>$46.95</td>
                                                <td>Markup %</td>
                                                <td><input type="text" class="ingredient_markup" name="total_ingredient_markup"  value="$300"></td>
                                                <td>Total Price:</td>
                                                <td><input type="text" name="total_ingred_price"  value="$187.80" disabled="disable"></td>
                                                </tr>
                                                </tfoot>
                                                </table>
                                                </div>
                                                </section>

                                                <section class="dropdown-click-sec-inner p-2 mb-4">
                                                <div class="heading px-3">
                                                <span class="dropdown-click-inner">
                                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                </span>
                                                <a href="javascript:void(0);" class="text-black">
                                                <span>Description</span>
                                                </a>
                                                <span class="float-right">
                                                <span class="delete-clk-inner">
                                                <i class="fa fa-trash fa-fw"></i>
                                                </span>
                                                </span>
                                                </div>

                                                <div class="text">
                                                <textarea class="form-control" rows="5"></textarea>
                                                </div>
                                                </section>

                                                <section class="dropdown-click-sec-inner p-2 mb-4">
                                                <div class="heading px-3">
                                                <span class="dropdown-click-inner">
                                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                </span>
                                                <a href="javascript:void(0);" class="text-black">
                                                <span>Notes</span>
                                                </a>
                                                <span class="float-right">
                                                <span class="delete-clk-inner">
                                                <i class="fa fa-trash fa-fw"></i>
                                                </span>
                                                </span>
                                                </div>

                                                <div class="text">
                                                <textarea class="form-control" rows="5"></textarea>
                                                </div>
                                                </section>
                                                </div>
                                                </div>
                                                </div>
                                                </td>
                                            </tr>
                                        <tfoot>
                                            <tr>
                                                <td class="p-0" colspan="10"><button type="button" class="btn w-100 add-row">+
                                                Add New Row</button></td>
                                            </tr>
                                            <tr>
                                                <th class="text-right" colspan="8">Section Subtotal:</th>
                                                <th>$0.00</th>
                                            </tr>
                                            <tr>
                                                <td class="p-0" colspan="10">
                                                    <div class="text-center p-3">
                                                        <div class="add-sec-plus addplus-cat" data-toggle="modal" data-target="#newModalPlusCat"></div>
                                                    </div>
                                                    <div class="modal fade" id="newModalPlusCat" tabindex="-1" role="dialog" aria-labelledby="newModalPlusCat" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                                                            <?php include('worksheet-modal.php'); ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </tbody>
                                </table>
                            </div><!-- dropdown-bx -->
                        </section><!-- dropdown-click-sec -->
                        <?php
                            }
                        ?>    
                        <div class="modal fade show" id="designBModal" tabindex="-1" role="dialog" aria-labelledby="designBModal" aria-modal="true">
                            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-info text-white">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Design Board Section</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?php include('design_board.php'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="changeNameMd" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-info text-white">
                                        <h5 class="modal-title">Change Name</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h6 class="heading item-heading">Change Name to:</h6>
                                        <div class="text mb-4">
                                            <input type="text" class="form-control" value="" id="CatNameVal">
                                        </div>
                                    </div>
                                    <div class="modal-footer text-center">
                                        <button class="btn btn-info m-auto corner_set btn_color" data-dismiss="modal" type="button" id="CatNameSub">
                                            <i class="fa fa-save fa-fw"></i>
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- change-name-modal --> 
                        <section class="dropdown-click-sec p-2 mb-4 dropdown-discounts" id="discount-sec">
                            <div class="heading px-3">
                                <span class="dropdown-click">
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </span>
                                <span>Discounts</span>
                                <span class="float-right">
                                    <span class="delete-clk">
                                        <i class="fa fa-trash fa-fw"></i>
                                        <span class="txt">Remove Section</span>
                                    </span>
                                </span>
                            </div>
                            <div class="dropdown-bx">
                                <table class="table table-striped table-list m-0 text-center">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Discount Name</th>
                                            <th>Apply To</th>
                                            <th></th>
                                            <th>Type</th>
                                            <th>Amount</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="body-clone-discounts">
                                        <?php 
                                        for ($i=0; $i < count($discount_name); $i++) { 
                                            ?>
                                            <tr>
                                                <td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>
                                                <td>
                                                    <button type="button" class="item-name-bx">
                                                        <span
                                                        class="item-name-txt"><?= empty($discount_name[$i])?"Discount Name":$discount_name[$i]; ?></span>
                                                        <input type="text" name="discount_name[]"  class="item-name-input"
                                                        oninput="set_val(this)" value="<?=$discount_name[$i]; ?>">
                                                    </button>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="discount_apply_to[]" >
                                                        <option selected value="<?=$discount_apply_to[$i]; ?>">
                                                            <?=empty($discount_apply_to[$i])?"Select":$discount_apply_to[$i]; ?>
                                                        </option>
                                                        <option value="Products">Products</option>
                                                        <option value="Services">Services</option>
                                                        <option value="Labor">Labor</option>
                                                        <option value="Event">Event</option>
                                                        <option value="Total">Total</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="discount_apply_to_2[]" >
                                                        <option selected value="<?=$discount_apply_to_2[$i]; ?>">
                                                            <?=empty($discount_apply_to_2[$i])?"Select":$discount_apply_to_2[$i]; ?>
                                                        </option>
                                                        <option value="Evently">Evently</option>
                                                        <option value="Taxable First">Taxable First</option>
                                                        <option value="Non-Taxable First">Non-Taxable First</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="discount_type[]" >
                                                        <option selected value="<?=$discount_type[$i]; ?>">
                                                            <?=empty($discount_type[$i])?"Select":$discount_type[$i]; ?>
                                                        </option>
                                                        <option value="Flat Amount">Flat Amount</option>
                                                        <option value="Percentage">Percentage</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" name="discount_amt[]"  value="<?=empty($discount_amt[$i])?"6.5":$discount_amt[$i]; ?>">
                                                </td>
                                                <td><span class="cross-distx-td"><i class="fa fa-times" aria-hidden="true"></i></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="80px">Taxes</td>
                                                <td colspan="2">
                                                    <span class="mr-2">Product</span>
                                                    <input type="text" name="tax[product]" 
                                                    value="<?=empty($tax_product)?"2":$tax_product; ?>">
                                                </td>
                                                <td colspan="2">
                                                    <span class="mr-2">Service</span>
                                                    <input type="text" name="tax[service]" 
                                                    value="<?=empty($tax_service)?"6.5":$tax_service; ?>">
                                                </td>
                                                <td colspan="2">
                                                    <span class="mr-2">Labor</span>
                                                    <input type="text" name="tax[labor]" 
                                                    value="<?=empty($tax_labor)?"6.5":$tax_labor; ?>">
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tfoot>
                                            <tr>
                                                <td class="p-0" colspan="7"><button type="button" class="btn w-100 add-row">+
                                                Add New Row</button></td>
                                            </tr>
                                            <tr>
                                                <td class="p-0" colspan="7">
                                                    <div class="text-center p-3">
                                                        <div class="add-sec-plus" data-toggle="modal" data-target="#newModalPlusDiscounts"></div>
                                                    </div>
                                                    <div class="modal fade" id="newModalPlusDiscounts" tabindex="-1" role="dialog" aria-labelledby="newModalPlusDiscounts" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                                                            <?php include('worksheet-modal.php'); ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </tbody>
                                </table>
                            </div><!-- dropdown-bx -->
                        </section><!-- dropdown-click-sec -->
                        <section class="dropdown-click-sec p-2 mb-4 dropdown-colorPalette" id="color-sec">
                            <div class="heading px-3">
                                <span class="dropdown-click">
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </span>
                                <span>Color Palette</span>
                                <span class="float-right">
                                    <span class="delete-clk">
                                        <i class="fa fa-trash fa-fw"></i>
                                        <span class="txt">Remove Section</span>
                                    </span>
                                </span>
                            </div>
                            <div class="dropdown-bx">
                                <table class="table table-striped table-list m-0 text-center">
                                    <tbody class="body-clone-colorPalette">
                                        <tr>
                                            <td class="p-0" colspan="">
                                                <div class="row slcolors ml-2">
                                                    <?php
                                                    for ($i=0; $i < count($favorite_colors); $i++) {
                                                        if (!empty($favorite_colors[$i])) {
                                                            ?>
                                                            <div class="col-1 platti-back p-3 ml-2 mt-3 mb-3" style="background-color:<?=$favorite_colors[$i];?>">
                                                                <span class="color-remove"><i class="fa fa-trash fa-fw"></i></span>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="text-center">
                                                    <div class="add-sec-plus myModalpalette" data-toggle="modal" data-target="#myModalpalette"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="p-0 bg-white" colspan="3">
                                                <div class="text-center p-3">
                                                    <div class="add-sec-plus" data-toggle="modal" data-target="#newModalPlusColor"></div>
                                                </div>
                                                <div class="modal fade" id="newModalPlusColor" tabindex="-1" role="dialog" aria-labelledby="newModalPlusColor" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                                                        <?php include('worksheet-modal.php'); ?>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!-- dropdown-bx -->
                        </section><!-- dropdown-click-sec -->
                        <!-- The Modal -->
                        <div class="modal fade" id="myModalpalette">
                            <div class="modal-dialog modal-dialog-centered modal-xl">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header bg_color_set">
                                        <h4 class="modal-title text-white"> Choose A Color</h4>
                                        <button type="button" class="close" id="ColorClose" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="row mt-3">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="card card-list-header">
                                                    <div class="card-header bg-white">

                                                        <ul class="nav nav-tabs card-header-tabs m-0" id="myTab" role="tablist">
                                                            <li class="bg-white">
                                                                <a class="nav-link active" id="ColorPalette-tab" data-toggle="tab" href="#ColorPalette" role="tab" aria-controls="ColorPalette" aria-selected="true">
                                                                    <i class="fa fa-paint-brush" aria-hidden="true"></i>
                                                                    <span>Color Palette</span>
                                                                </a>
                                                            </li>
                                                            <li class="bg-white">
                                                                <a class="nav-link" id="InputColor-tab" data-toggle="tab" href="#InputColor" role="tab" aria-controls="InputColor" aria-selected="false">
                                                                    <i class="fa fa-keyboard-o" aria-hidden="true" ></i>
                                                                    <span>Input Color</span>
                                                                </a>
                                                            </li>
                                                            <li class="bg-white">
                                                                <a class="nav-link" id="Favorites-tab" data-toggle="tab" href="#Favorites" role="tab" aria-controls="Favorites" aria-selected="false">
                                                                    <i class="fa fa-star" aria-hidden="true" ></i>
                                                                    <span>Favorites</span>
                                                                </a>
                                                            </li>
                                                        </ul>

                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                                                                <div class="m-2">
                                                                    <span>Selected Color</span>

                                                                    <div class="border" style="padding:38%;" id="back-pickcolor-set"></div>

                                                                    <div class="text-center mt-3">
                                                                        <input type="text" id="hex" class="text-center border d-2 text-dark" readonly />
                                                                        <h6>History</h6>
                                                                        <h5>
                                                                            <a href="" class="text-warning" id="addtofav"><i class="fa fa-star text-warning" aria-hidden="true" ></i><span>Add to Favorites</span></a>
                                                                            <br>
                                                                            <a href="" class="text-warning" id="selectcolor"><i class="fa fa-check-circle" aria-hidden="true" ></i><span>Select Color</span></a>
                                                                        </h5>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                                                                <div class="tab-content" id="myTabContent">
                                                                    <div class="tab-pane fade show active" id="ColorPalette" role="tabpanel" aria-labelledby="ColorPalette-tab">
                                                                        <div class="text-center m-2">
                                                                            <strong class="text-dark">Use the palette below to pick a color.</strong>
                                                                            <div id="colorWrapper" class="d-flex colorWrapper1">
                                                                                <div id="picker"></div>
                                                                                <div id="slider"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane fade" id="InputColor" role="tabpanel" aria-labelledby="InputColor-tab">
                                                                        <div class="row">
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                                                <div class="mt-4">
                                                                                    <label for="" class="mb-0 text-dark">HEX/HTML Color Code</label>
                                                                                    <p class="p-1 platti-back text-center">Code:<input type="text" id="hex2" class="mb-0"></p>
                                                                                    <label for="" class="mb-0 text-dark">Pantone</label>
                                                                                    <p class="p-1 platti-back text-center">Code:<input type="text" class="mb-0"></p>
                                                                                    <label for="" class="mb-0 text-dark">CMYK</label>
                                                                                    <div class="pt-3 platti-back text-center">
                                                                                        <p class="text-dark">
                                                                                            C:<input type="text" class="w-25 mx-3">
                                                                                            M:<input type="text" class="w-25 mx-3">
                                                                                        </p>
                                                                                        <p class="text-dark">
                                                                                            Y:<input type="text" class="w-25 mx-3">
                                                                                            K:<input type="text" class="w-25 mx-3">
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">

                                                                                <label for="" class="mb-0 mt-4 text-dark">HSV</label>
                                                                                <div class="text-center">
                                                                                    <p class="platti-back py-1">
                                                                                        H:<input type="text" id="H" class="mx-2" style="width:60px;">
                                                                                        S:<input type="text" id="S" class="mx-2" style="width:60px;">
                                                                                        V:<input type="text" id="V" class="mx-2" style="width:60px;">
                                                                                    </p>
                                                                                </div>

                                                                                <label for="" class="mb-0 text-dark">HSL</label>
                                                                                <div class="text-center">
                                                                                    <p class="platti-back py-1 text-dark">
                                                                                        H:<input type="text" class="mx-2" style="width:60px;">
                                                                                        S:<input type="text" class="mx-2" style="width:60px;">
                                                                                        L:<input type="text" class="mx-2" style="width:60px;">
                                                                                    </p>
                                                                                </div>

                                                                                <label for="" class="mb-0 text-dark">RGB</label>
                                                                                <div class="text-center">
                                                                                    <p class="platti-back py-1 text-dark">
                                                                                        R:<input type="text" id="R" class="mx-2" style="width:60px;">
                                                                                        G:<input type="text" id="G" class="mx-2" style="width:60px;">
                                                                                        B:<input type="text" id="B" class="mx-2" style="width:60px;">
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="tab-pane fade" id="Favorites" role="tabpanel" aria-labelledby="Favorites-tab">
                                                                        <p class="mb-1 text-dark mt-3 border-bottom">Favorite Colors</p>
                                                                        <div class="row favorite-color-container FCUPDATE" id="FCUPDATE">
                                                                            <?php 
                                                                            $fc_sql = "SELECT * FROM `favorite_colors` WHERE user_id='$id'";  
                                                                            $fc_result = mysqli_query($con, $fc_sql);
                                                                            while($fc_color = mysqli_fetch_assoc($fc_result)){
                                                                                $fc=(explode(",",$fc_color['color_code']));
                                                                                for ($i=0; $i < count($fc) ; $i++) {
                                                                                    if ($fc[$i] !="") {?>
                                                                                        <div class="">
                                                                                            <a href=""><div class="platti-back favcdiv p-4 m-1" id="<?=$fc[$i];?>" style="background-color:<?=$fc[$i];?>">
                                                                                                <span class="color-remove" id="<?=$fc[$i];?>">
                                                                                                    <i class="fa fa-trash fa-fw"></i>
                                                                                                    <input type="hidden" id="workCid" value="<?=$id;?>">
                                                                                                </span>
                                                                                            </div></a>
                                                                                        </div>
                                                                                    <?php }
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- colorpatelModel -->
                    </div>

                    <section class="p-2 add-new-sec">
                        <a class="add-new-worksheet text-center" href="javascript:void(0);" data-toggle="modal" data-target="#newWorksheetModal">
                            + Add New Section
                        </a>
                        <div class="modal fade" id="newWorksheetModal" tabindex="-1" role="dialog" aria-labelledby="newWorksheetModal" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-info text-white">
                                        <h5 class="modal-title">Add Content</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <ul class="add-new-sec-list">
                                            <li>
                                                <a class="click-new-sec click-cat-btn" href="javascript:void(0);">
                                                    <i class="fa fa-th-list" aria-hidden="true"></i>
                                                    <span>Category Group</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="click-new-sec click-colorPalette-btn" href="javascript:void(0);">
                                                    <i class="fa fa-paint-brush" aria-hidden="true"></i>
                                                    <span>Color Palette</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="click-new-sec click-discounts-btn" href="javascript:void(0);">
                                                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                    <span>Discounts/Taxes</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section><!-- add-new-sec -->
                </div><!-- worksheet-scroll-dv -->
            </div><!-- worksheet-right-dv -->
        </div><!-- col-8 -->
    </div>


    <div class="row worksheet-fix-save p-3" style="border-top:solid 2px rgb(220,220,220);">
        <div class="col-lg-12">
            <div class="text-center">
                <button class="btn btn-info pl-4 m-0 pr-4 corner_set btn_color" id="dtaSave" data-toggle="modal" data-target="#myModal2" type="button" name="submit">
                    <i class="fa fa-save fa-fw"></i>
                    Save
                </button>
            </div>
        </div>
    </div>
    <!-- The Modal -->
    <div class="modal fade" id="myModal2">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg_color_set">
                    <h4 class="modal-title text-white"> Save As</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <p class="heading mx-2 font-weight-normal">Save Options</p>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-4 mt-3" id="show_name1">
                                    <h6 for="" class="text-right">Name</h6>
                                </div>
                                <div class="col-md-8 mt-3" id="show_name2">
                                    <input type="text" class="form-control corner_set" placeholder="" name="name" value="">
                                </div>
                                <div class="col-md-4 mt-3">
                                    <h6 for="" class="text-right mt-2">Description</h6>
                                </div>
                                <div class="col-md-8 mt-3">
                                    <input type="text" class="form-control corner_set" placeholder="i.e. Added Toss Bouquet" name="description" value="">
                                </div>
                                <div class="col-md-4 mt-3" id="show_tag1">
                                    <h6 for="" class="text-right mt-2">Tags</h6>
                                </div>
                                <div class="col-md-8 mt-3" id="show_tag2">
                                    <input type="text" class="form-control corner_set" placeholder="corporate, magical, perfect" name="tags" value="">
                                </div>

                            </div>
                            <!-- Modal footer -->
                            <div class="border-top mt-2">
                                <div class=" text-center">
                                    <button class="btn btn-info corner_set btn_color" type="submit"
                                    name="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <input type="hidden" id="curr_sec" name="curr_sec" />
    <input type="hidden" id="curr_add" name="curr_add" />
    <input type="hidden" id="allcat" name="allcat">

    <!-- Modal footer -->
</form>
</div>
</div>
</div>
</div>
</section>
</div>
<?php //include('worksheet/worsheet-var.php'); ?>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="js/color-palette.js"></script>
<script src="js/worksheet.js"></script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
$(document).ready(function() {
    $("#table_remove").click(function() {
        $("#table_show").toggle();
    });
});
$(document).ready(function() {
    $("#table_remove1").click(function() {
        $("#table_show1").toggle();
    });
});
$(document).ready(function() {
    $("#Bouquets_hide_show").click(function() {
        $("#Bouquets_hide").toggle();
    });
});
$(document).ready(function() {
    $("#Boutonnieres_show_hide").click(function() {
        $("#Boutonnieres_show").toggle();
    });
});
$(document).ready(function() {
    $("#Corsages_show_hide").click(function() {
        $("#Corsages_hide").toggle();
    });
});
$(document).ready(function() {
    $("#Ceremony_show_hide").click(function() {
        $("#Ceremony_hide").toggle();
    });
});
$(document).ready(function() {
    $("#Cocktail_show_hide").click(function() {
        $("#Cocktail_hide").toggle();
    });
});
$(document).ready(function() {
    $("#Reception_show_hide").click(function() {
        $("#Reception_hide").toggle();
    });
});
$(document).ready(function() {
    $("#Chargers_show_hide").click(function() {
        $("#Chargers_hide").toggle();
    });
});
$(document).ready(function() {
    $("#Services_show_hide").click(function() {
        $("#Services_hide").toggle();
    });
});
$(document).ready(function() {
    $("#Fees_show_hide").click(function() {
        $("#Fees_hide").toggle();
    });
});
$(document).ready(function() {
    $("#Event_show_hide").click(function() {
        $("#Event_hide").toggle();
    });
});
$(document).ready(function() {
    $("#Summary_show_hide").click(function() {
        $("#Summary_hide").toggle();
    });
});
function set_file(element) {
    if (element.value == "New Templates") {
        $("#show_name1").show();
        $("#show_name2").show();
        $("#show_tag1").show();
        $("#show_tag2").show();
    } else {
        $("#show_name1").hide();
        $("#show_name2").hide();
        $("#show_tag1").hide();
        $("#show_tag2").hide();
    }
}
</script>
<script>
  $( function() {
    $( "#sortable1, #sortable2" ).sortable({
      connectWith: ".connectedSortable"
  }).disableSelection();
} );
</script> 
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on("click",".color-remove",function(){
            var id = $("#workCid").val();
            var color = $(this).attr('id');
            $(this).parents(".platti-back").remove();
            $.ajax({
                url:'update-worksheet.php',
                type:'POST',
                data:{id:id,color:color},
                success:function(response){
                }
            });
        });
    });
</script>
</body>
</html>