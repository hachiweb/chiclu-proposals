<?php
$page ="NULL";
include('header.php');
?>
<section class="container-fluid mt-3">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="bg_color_set p-3">
                <div class="row p-1">
                    <div class="col-lg-6 col-md-4 col-sm-4">
                        <h4 class="d-inline text-white ml-2 font-weight-normal">Design Board</h4>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4">
                    </div>
                </div>
            </div>
            <div class="bg-white pb-4">
                <div class="row pt-5">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="text-right font-weight-normal text-dark">
                            <p>Event Grand Total:</p>
                            <p>Your Projected Expenses:</p>
                            <p>Actual Costs:</p>
                            <p>Under/Over Projection by:</p>
                            <p>Need by Date:</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="text-center font-weight-normal text-dark">
                            <p>$0.00</p>
                            <p>$0.00</p>
                            <p>$0.00</p>
                            <p>$0.00</p>
                        </div>
                        <div>
                            <input id="datepicker" width="200" style="border-radius:0;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 ">
            <div class="bg_color_set p-3">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="text-left">
                            <p class="text-white h4 font-weight-normal ml-2">Wholesale Quotes</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="">

                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="text-right">
                            <div class="">
                                <button class="btn btn-info  border-white text-white corner_set" type="button"
                                    id="tabel_remove">
                                    <i class="fa fa-envelope fa-fw"></i> Request A Quotes
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="tabel_remove1">
                <div class="bg-white">
                    <div class="row pt-5">
                        <div class="col-md-12 px-4">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="text-center">
                                        <th>Wholesaler</th>
                                        <th>Total Stems</th>
                                        <th>Ask Total</th>
                                        <th>Status</th>
                                        <th>Quote Total</th>
                                        <th>Quote Expiration</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td><a href="#" style="color: #009198;">Farm Fresh Exports</a></td>
                                        <td></td>
                                        <td>$</td>
                                        <td>Started</td>
                                        <td></td>
                                        <td></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div style="display:none;" id="tabel_show">
                <div class="bg-white">
                    <div class="row p-3 d-flex">
                        <div class="col-md-1"></div>
                        <div class="col-md-3 border text-center ml-4">
                            <a href="farm_export.php"><img src="img/farmexport.png" style="width:30%;" alt="Not Uploaded"> </a>
                        </div>
                        <div class="col-md-3 border text-center ml-4">
                            <a href="flower_farm.php"><img src="img/flower_farm.png" style="width:100%;" alt="Not Uploaded"> </a></div>
                        <div class="col-md-3 border text-center ml-4">
                            <a href="remirez.php"><img src="img/ramirez.png" style="width:33%;" alt="Not Uploaded"> </a></div>
                        <div class="col-md-1"></div>

                    </div>
                    <div class="text-center">
                        <button class="btn btn-info corner_set btn_color" type="submit">More Wholesalers</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
</section>
<script>
$('#datepicker').datepicker({
    uiLibrary: 'bootstrap4'
}); //datepicker

$(document).ready(function() {
    $("#tabel_remove").click(function() {

        if ($("#tabel_remove1").is(":hidden")) {

            $("#tabel_remove1").show();
            $("#tabel_show").hide();
            $("#tabel_remove").html("<i class='fa fa-envelope fa-fw'></i> Request A Quotes");
        } 
        else {
            $("#tabel_remove1").hide();
            $("#tabel_show").show();
            $("#tabel_remove").html("<i class='fa fa-file fa-fw'></i> View Quotes");
        }

    });
});
</script>
</body>

</html>