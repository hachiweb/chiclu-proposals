<?php
// if(isset($_SESSION["groom_bride"]))
// {
//     session_unregister($_SESSION["groom_bride"]);
    
// }
session_start();
$_SESSION["groom_bride"] = "";
$page ="Home";
include('header.php');
?> <!---header added--->

        <div class="container-fluid">
            <div class="row">
                <?php 
             $sql = "SELECT * FROM event_questionare";
             $result = $con->query($sql);
             ?>
             <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs p-0">
                <div class="card card-list-header">
                    <!-- <div class="card-header bg-white">
                        <ul class="nav nav-tabs card-header-tabs m-0" id="bologna-list" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#description" role="tab" aria-controls="description" aria-selected="true">
                                    <h2 class="forn-weight-normal"> </h2>
                                    <h2 class="forn-weight-normal">Current</h2>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#history" role="tab" aria-controls="history" aria-selected="false">
                                    <h2 class="forn-weight-normal">0</h2>
                                    <h2 class="forn-weight-normal">Inquiry</h2>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#deals" role="tab" aria-controls="deals" aria-selected="false">
                                    <h2 class="forn-weight-normal">0</h2>
                                    <h2 class="forn-weight-normal">Plan</h2>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#deals1" role="tab" aria-controls="deals" aria-selected="false">
                                    <h2 class="forn-weight-normal">0</h2>
                                    <h2 class="forn-weight-normal">Review</h2>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#deals2" role="tab" aria-controls="deals" aria-selected="false">
                                    <h2 class="forn-weight-normal">0</h2>
                                    <h2 class="forn-weight-normal">Final</h2>
                                </a>
                            </li>
                        </ul>
                    </div> -->

                        <div class="event-table-bar p-3" style="background-color:rgb(23,162,184);">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <h5 class="font-weight-normal text-white m-0">
                                        <i class="fa fa-map fa-fw"></i>
                                        Event List
                                    </h5>
                                </div>
                                <div class="col-lg-4">
                                    <div class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default text-white dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                                style="font-size:20px;">
                                                Showing <?php echo $result->num_rows; ?> Records
                                            </button>

                                            <div class="dropdown-menu" style="width:300px;">
                                                <div>
                                                    <h5 class="ml-3 mt-2 font-weight-normal">Views</h5>
                                                </div>
                                                <div class="border">
                                                    <div class="hover_set border-top py-2">
                                                        <div class="form-check ml-3 hover_set">
                                                            <input class="form-check-input" type="radio"
                                                                name="exampleRadios" id="exampleRadios1" value="option1"
                                                                checked>
                                                            <label class="form-check-label" for="exampleRadios1"><i
                                                                    class="fa fa-book fa-fw"></i>
                                                                ALL
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="hover_set border-top py-2">
                                                        <div class="form-check ml-3 ">
                                                            <input class="form-check-input" type="radio"
                                                                name="exampleRadios" id="exampleRadios1" value="option1"
                                                                checked>
                                                            <label class="form-check-label" for="exampleRadios1"><i
                                                                    class="fa fa-leaf fa-fw"></i>
                                                                Current
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="hover_set border-top py-2">
                                                        <div class="form-check ml-3">
                                                            <input class="form-check-input" type="radio"
                                                                name="exampleRadios" id="exampleRadios1" value="option1"
                                                                checked>
                                                            <label class="form-check-label" for="exampleRadios1"><i
                                                                    class="fa fa-leaf fa-fw"></i>
                                                                No Status
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="hover_set border-top py-2">
                                                        <div class="form-check  ml-3">
                                                            <input class="form-check-input" type="radio"
                                                                name="exampleRadios" id="exampleRadios1" value="option1"
                                                                checked>
                                                            <label class="form-check-label" for="exampleRadios1"><i
                                                                    class="fa fa-clock fa-fw"></i>
                                                                Past Events
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="hover_set border-top py-2">

                                                        <div class="form-check ml-3">
                                                            <input class="form-check-input" type="radio"
                                                                name="exampleRadios" id="exampleRadios1" value="option1"
                                                                checked>
                                                            <label class="form-check-label" for="exampleRadios1"><i
                                                                    class="fa fa-thumbs-down fa-fw"></i>
                                                                Did Not Book
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="hover_set border-top py-2">
                                                        <div class="form-check ml-3">
                                                            <input class="form-check-input" type="radio"
                                                                name="exampleRadios" id="exampleRadios1" value="option1"
                                                                checked>
                                                            <label class="form-check-label" for="exampleRadios1"><i
                                                                    class="fa fa-archive fa-fw"></i>
                                                                Archived
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs">
                                    <div class="btn-options text-right">
                                        <button class="btn btn-info border-white  btn-responsive" type="button">
                                            <i class="fa fa-print fa-fw"></i>
                                            Print
                                        </button>
                                        <button class="btn btn-info border-white  btn-responsive" type="button">
                                            <i class="fa fa-download fa-fw"></i>
                                            Export
                                        </button>
                                        <button class="btn btn-info border-white  btn-responsive " type="button">
                                            <i class="fa fa-cog fa-fw"></i>
                                            Options
                                        </button>
                                        <button class="btn btn-info  border-white  btn-responsive" type="button">
                                            <i class="fa fa-refresh fa-fw"></i>
                                            Refresh
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- event-table-bar -->

            </div>
            <div class="card-body">
                <div class="tab-content mt-3 table-responsive">
                    <table class="tab-pane active table" id="description" role="tabpanel">
                    <thead>
                        <tr>
                            <th >
                                <span class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Event
                                Date</span>
                            </th>
                            <th >
                                <span class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Client
                                Email</span>
                            </th>
                            <th >
                                <span class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Groom & Bride
                                Name</span>
                            </th>
                            <th >
                                <span class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Location
                                </span>
                            </th>
                            <th >
                                <span class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Status
                                </span>
                            </th>
                            <th >
                                <span class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Last Updated at
                                </span>
                            </th>

                                <th>

                                    <!-- <input type="checkbox" class="form-check-input" value="" style="font-size:20px;"> -->

                                </th>

                            </tr></thead>
                            <tbody>
                            <?php 
                                if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) {
                            ?>
                                    <tr class="border-top border-bottom pt-4 pb-3 bg-white m-0">

                                            <td>
                                                <span><i class="fa re-tooltip fa-comment"></i> <?php echo $row['event_date']; ?></span>
                                            </td>
                                            <td >
                                            <a href="detail_form.php?ID=<?php echo $row['event_id']; ?>"><span class="font-weight-normal"><?php echo $row['contact_email']; ?></span>
                                            </td>
                                            <td >
                                                <a href="detail_form.php?ID=<?php echo $row['event_id']; ?>"><span class="font-weight-normal"><?php echo $row['groom_name']." & ".$row['bride_name']; ?></span></a>
                                            </td>
                                            <td >
                                            <a href="detail_form.php?ID=<?php echo $row['event_id']; ?>"><span class="font-weight-normal"><?php echo $row['reception_location']; ?></span></a>
                                            </td>
                                            <td >
                                            <a href="detail_form.php?ID=<?php echo $row['event_id']; ?>"><span class="font-weight-normal"><?php echo $row['status']; ?></span></a>
                                            </td>
                                            <td >
                                            <a href="detail_form.php?ID=<?php echo $row['event_id']; ?>"><span class="font-weight-normal"> <?= date('d M, Y h:i a',strtotime($row['last_updated'])) ;?></></a>
                                            </td>
                                        
                                            <td>
                                                <input type="checkbox" class="form-check-input" value=""
                                                style="font-size:20px;">
                                            </td>
                                        </tr>
                                        <!-- <div class="col-md-1 col-sm-1 col-xs"></div>
                                        <div class="col-md-1 col-sm-1 col-xs"></div> -->
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table> 
                        </div>
                        <?php /*
                       <div class="tab-pane" id="history" role="tabpanel" aria-labelledby="history-tab">
                            <div class="row m-0">

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs">
                                    <p class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Event
                                        Date</p>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs">
                                    <p class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Client
                                        Email</p>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs">
                                    <p class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Groom & Bride
                                        Name</p>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs">
                                    <p class="text-dark"><i class="fa fa-sort sort-column-icon fa-fw"></i> Location
                                    </p>
                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs">

                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs">

                                    <input type="checkbox" class="form-check-input" value="" style="font-size:20px;">

                                </div>

                            </div>
                            <?php if ($result->num_rows > 0) {
                            while($row2 = $result->fetch_assoc()) {
                                ?>
                            <div class="row border-top border-bottom pt-4 pb-3 bg-white m-0">

                                <div class="col-md-2 col-sm-2 col-xs">
                                    <p><i class="fa re-tooltip fa-comment"></i> <?php echo $row2['event_date']; ?></p>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">
                                    <p class="font-weight-normal"><?php echo $row2['contact_email']; ?></p>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">
                                    <p class="font-weight-normal"><?php echo $row2['event_name']; ?></p>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">
                                    <a href="detail_form.php?ID=<?php echo $row2['event_id']; ?>">
                                        <p class="font-weight-normal">
                                            <?php echo $row2['address'].", ".$row2['city'].", ".$row2['state'].", ".$row2['zip']; ?>
                                        </p>
                                    </a>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs"></div>
                                <div class="col-md-1 col-sm-1 col-xs"></div>
                                <div class="col-md-1 col-sm-1 col-xs">
                                    <input type="checkbox" class="form-check-input" value="" style="font-size:20px;">
                                </div>

                            </div>
                            <?php
                            }
                        }
                        ?>
                        </div> 

                        <div class="tab-pane" id="deals" role="tabpanel" aria-labelledby="deals-tab">
                            <div class="row m-0">

                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>

                                <div class="col-md-2 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>

                                <div class="col-md-1 col-sm-1 col-xs">



                                </div>

                            </div>
                            <div class="row border-top border-bottom pt-4 pb-3 bg-white m-0">

                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">
                                    <p class="h4">No Records</p>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs"></div>
                                <div class="col-md-1 col-sm-1 col-xs"></div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>


                            </div>
                        </div>

                        <div class="tab-pane" id="deals1" role="tabpanel" aria-labelledby="deals1-tab">
                            <div class="row m-0">

                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>

                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>

                                <div class="col-md-1 col-sm-1 col-xs">



                                </div>

                            </div>
                            <div class="row border-top border-bottom pt-4 pb-3 bg-white m-0">

                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">
                                    <p class="h4">No Records</p>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs"></div>
                                <div class="col-md-1 col-sm-1 col-xs"></div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>


                            </div>
                        </div>

                        <div class="tab-pane" id="deals2" role="tabpanel" aria-labelledby="deals2-tab">
                            <div class="row m-0">

                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>

                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>

                                <div class="col-md-1 col-sm-1 col-xs">



                                </div>

                            </div>
                            <div class="row border-top border-bottom pt-4 pb-3 bg-white m-0">

                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">

                                </div>
                                <div class="col-md-2 col-sm-2 col-xs">
                                    <p class="h4">No Records</p>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>
                                <div class="col-md-1 col-sm-1 col-xs"></div>
                                <div class="col-md-1 col-sm-1 col-xs"></div>
                                <div class="col-md-1 col-sm-1 col-xs">

                                </div>


                            </div> */ ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row m-0">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs" style="height:380px">

            </div>
        </div>
        <div class="border-top pt-3">
            <div class="text-center bottom-submit-actions">
                <div class="form-group clearfix">
                    <button class="btn btn-info border-white" type="button">
                        <i class="fa fa-save fa-fw"></i>
                        Save Changes
                    </button>
                    <span>
                        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
                    </span>
                    <button class="btn btn-default"
                        style="background-color:rgb(23,162,184);color:white;">Search</button>

                    <button class="btn btn-info" data-toggle="modal" data-target=".modal-newClient">New Client /
                        Event</button>
                </div>
            </div>
        </div>

    </div>
    </div>

    <div class="modal fade modal-newClient" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content modal_width_set">
            <form method="POST" action="sub-questionare.php">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title">Add Event</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <p class="text-center">
                        <b>Use this form to add the Primary Event.</b><br>
                        If you would like to add more Event click "Save & Go to Event"
                    </p>
                </div>
                
                    <h6 class="heading ml-3 ">Primary Event Info</h6>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="bg-white pl-3">
                                <h4 class="text-white p-3" style="background-color:rgb(23,162,184);">Event Details</h4>

                                <div class="mt-3 ml-2 ">
                                        <p class="border-bottom">Event Info</p>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="mt-3">
                                                    <div class="md-form">
                                                        <label for="form1">Bride Name<span class="text-danger">*</span></label>
                                                        <input type="text" id="form1" class="form-control corner_set"
                                                            value="" name="bride_name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="mt-3">
                                                    <div class="md-form">
                                                        <label for="form1">Groom Name<span class="text-danger">*</span></label>
                                                        <input type="text" id="form1" class="form-control corner_set"
                                                            value="" name="groom_name">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="mt-3">
                                                    <div class="md-form">
                                                        <label for="form1">Referred By</label>
                                                        <input type="text" id="form1" class="form-control corner_set" name="referred_by">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="mt-3">
                                                    <div class="md-form">
                                                        <label for="form1">Event Date</label>
                                                        <input type="date" id="form1" class="form-control corner_set" name="event_date">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="mt-3">
                                                    <div class="md-form">
                                                        <label for="form1">Number of Guests</label>
                                                        <input type="number" id="form1" class="form-control corner_set" name="number_guests">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="mt-3">
                                                    <div class="md-form">
                                                        <label for="form1">Contact Email</label>
                                                        <input type="email" id="form1" class="form-control corner_set" name="contact_email">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="mt-3">
                                                    <div class="md-form">
                                                        <label for="form1">Phone Number 1</label>
                                                        <input type="number" id="form1" class="form-control corner_set"  name="contact_number_first">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="mt-3">
                                                    <div class="md-form">
                                                        <label for="form1">Phone Number 2</label>
                                                        <input type="number" id="form1" class="form-control corner_set" name="contact_number_second">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group mt-3">
                                            <label for="comment">Event Notes</label>
                                            <textarea class="form-control corner_set" rows="3"
                                                id="comment" name="event_notes_description"></textarea>
                                        </div>
                                        <div class="mt-3">
                                            <div class="md-form">
                                                <label for="form1">Event Budget $</label>
                                                <input type="number" id="form1" class="form-control corner_set" name="event_budget">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="bg-white pr-3">
                                <h4 class="text-white p-3" style="background-color:rgb(23,162,184);">Event Schedule</h4>
                                <p class="border-bottom  mr-1">Times & Locations</p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mt-3  mr-1">
                                            <div class="md-form">
                                                <label for="form1">Ceremony Start Time</label>
                                                <input type="time" id="form1" class="form-control corner_set" name="ceremony_start_time">
                                            </div>
                                        </div>
                                        <div class="mt-3  mr-1">
                                            <div class="md-form">
                                                <label for="form1">Cocktails Start Time</label>
                                                <input type="time" id="form1" class="form-control corner_set" name="cocktails_start_time">
                                            </div>
                                        </div>
                                        <div class="mt-3 mr-1">
                                            <div class="md-form">
                                                <label for="form1">Reception Start Time</label>
                                                <input type="time" id="form1" class="form-control corner_set" name="reception_start_time">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="mt-3 mr-1">
                                            <div class="md-form">
                                                <label for="form1">Ceremony Location</label>
                                                <input type="text" id="form1" class="form-control corner_set" name="ceremony_location">
                                            </div>
                                        </div>
                                        <div class="mt-3 mr-1">
                                            <div class="md-form">
                                                <label for="form1">Cocktails Location</label>
                                                <input type="text" id="form1" class="form-control corner_set" name="cocktails_location">
                                            </div>
                                        </div>
                                        <div class="mt-3 mr-1">
                                            <div class="md-form">
                                                <label for="form1">Reception Location</label>
                                                <input type="text" id="form1" class="form-control corner_set" name="reception_location">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <p class="border-bottom  mr-1 mt-5">Important Times</p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mt-3  mr-">
                                            <div class="md-form">
                                                <label for="form1">Setup Available</label>
                                                <input type="time" id="form1" class="form-control corner_set" name="setup_available">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="mt-3  mr-1">
                                            <div class="md-form">
                                                <label for="form1">Event End</label>
                                                <input type="time" id="form1" class="form-control corner_set" name="event_end">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="bg-white p-3 mt-4">
                                <h4 class="text-white p-3" style="background-color:rgb(23,162,184);">Delivery Info</h4>
                                <p class="border-bottom ml-3 mr-3">The Bouquet</p>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="md-form">
                                                <label for="form1">Bouquet Delivery</label>
                                                <input type="time" id="form1" class="form-control corner_set" name="bouquet_deliverey_time">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="md-form">
                                                <label for="form1">Bouquet Delivery Location</label>
                                                <input type="text" id="form1" class="form-control corner_set" name="bouquet_delivery_location">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="form-group mt-3">
                                                <label for="comment">Bridal bouquet desire type (description)</label>
                                                <textarea class="form-control corner_set" rows="3"
                                                    id="comment" name="bridal_bouquet_desire_type"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="form-group mt-3">
                                                <label for="comment">Bridesmaids bouquets desire type
                                                    (description)</label>
                                                <textarea class="form-control corner_set" rows="3"
                                                    id="comment" name="bridalmaids_bouquet_desire_type"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="mt-3 mx-3">
                                    <div class="md-form">
                                        <label for="form1">How many Bridesmaids bouquets</label>
                                        <input type="number" id="form1" class="form-control corner_set" name="bridals_maids_bouquets_number">
                                    </div>
                                </div>
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="form-group mt-3">
                                        <label for="comment">Groomsmen boutonnières desire type (description)</label>
                                        <textarea class="form-control corner_set" rows="3" id="comment" name="groomsmen_boutnaries_desire_type"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="form-group mt-3">
                                                <label for="comment">How many Groomsmen boutonnieres</label>
                                                <input type="number" id="form1" class="form-control corner_set" name="groomsmen_boutnaries">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="mt-3 ml-3 mr-3">
                                            <div class="form-group mt-3">
                                                <label for="comment">How Many tables?</label>
                                                <input type="number" id="form1" class="form-control corner_set" name="number_tables">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="form-group mt-3">
                                        <label for="comment">Tables type(round/rectangular)</label>
                                        <input type="text" id="form1" class="form-control corner_set" name="type_tables">
                                    </div>
                                </div>
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="form-group mt-3">
                                        <label for="comment">Would you like an arch with ﬂorals and greenery? desire
                                            type
                                            (description)</label>
                                        <textarea class="form-control corner_set" rows="3" id="comment" name="arch_florals_greenery" ></textarea>
                                    </div>
                                </div>
                                <div class="mt-3 ml-3 mr-3">
                                    <div class="form-group mt-3">
                                        <label for="comment">Arch type</label>
                                        <input type="text" id="form1" class="form-control corner_set" name="arch_type">
                                        <p>Will venue provide arch? or you want it rented from us?</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center  pb-4">
                        <button type="submit" class="btn btn-primary corner_set btn_color">Save Event</button>
                    </div>

                    </form>
            </div>
            <!--modal content -->
        </div>
        <!--form body -->
    </div>
    <!--modal body -->
    </div>
    </div>
    <!-- <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header bg-info text-white">
            <h5 class="modal-title">Add Client</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
        <p class="text-center">
            <b>Use this form to add the primary contact.</b><br>
            If you would like to add more contacts or a note click "Save & Go to Client"
        </p>
        <form action="sub-client.php" method="POST">
            <h6 class="heading">Primary Contact Info</h6>

            <div class="row">
            <div class="col-md-6">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name">
                    </div>

                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name">
                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" id="phone" name="phone_one">
                    </div>

                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control" id="email" name="email">
                    </div>
                </div> --col --
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" id="address" name="address">
                    </div>

                    <div class="form-group">
                        <label>City</label>
                        <input type="text" class="form-control" id="city" name="city">
                    </div>

                    <div class="form-group">
                        <label>State</label>
                        <input type="text" class="form-control" id="state" name="state">
                    </div>

                    <div class="form-group">
                        <label>Zip</label>
                        <input type="text" class="form-control" id="zip" name="zip">
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="source" name="source" value="">
                    </div>
                </div> col --
            </div> row --
            <div class="text-center mt-3">
                <button type="submit" href="javascript:void(0)" onmouseover="set_mouseover('Client')" id="subclient" name="subclient" class="btn btn-primary">Save & Go to Client</button>
                <button type="submit" href="javascript:void(0)" onmouseover="set_mouseover('Event')" id="subevent" name="subevent" class="btn btn-primary">Save & Go to Event</button>
            </div>
        </form>
    </div>modal-body
</div>
</div> -->
    </div>
    <script type="text/javascript">
    function set_mouseover(subclient) {
        jQuery('#source').val(subclient);
    }
    </script>
    <script type="text/javascript">
    function set_mouseover(subevent) {
        jQuery('#source').val(subevent);
    }
    </script>
    <script>
    $('#bologna-list a').on('click', function(e) {
        e.preventDefault()
        $(this).tab('show')
    })
    </script>
</body>
<html>