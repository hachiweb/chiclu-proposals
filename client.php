<?php
$page ="NULL";
include('header.php');
?>
        <section class="container-fluid">
        <form action="sub-edit-client.php" method="POST">
        <?php 
            $sql = "SELECT * FROM `client` WHERE `email` = '$tempemail'";
            $resultclient = $con->query($sql);
            if ($resultclient->num_rows > 0) {
                // print_r($resultclient); exit();
                while($clientdata = $resultclient->fetch_assoc()) {
            ?> 
            <div class="row mt-3">
                <div class="col-md-4">
                    <div class="bg_color_set p-3">
                        <h4 class="text-white pt-1">Client-Info</h4>
                    </div>
                    <div class="bg-white">
                        <div class="pt-3">
                            <div class="pill_padding">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                    aria-orientation="vertical">

                                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill"
                                        href="#v-pills-home" role="tab" aria-controls="v-pills-home"
                                        aria-selected="true"><i class="fa fa-fw fa-address-book"></i> Contact</a>
                                    <!-- <a class="nav-link mt-1" id="v-pills-profile-tab" data-toggle="pill"
                                        href="#v-pills-profile" role="tab" aria-controls="v-pills-profile"
                                        aria-selected="false">Log, Notes & Events</a> -->


                                </div>
                            </div>

                        </div>


                    </div>
                    <div class="bg_color_set p-3 mt-3">
                        <h4 class="text-white "><?php echo $clientdata['first_name']." ".$clientdata['last_name']; ?></h4>
                    </div>
                    <div class="bg-white">
                        <div class="mx-auto text-center">
                                <div class="file-field">
                                    <div class="mx-auto pt-4">
                                        <img src="img/missing_user.png" class="rounded-circle z-depth-1-half avatar-pic"
                                            alt="example placeholder avatar" height="300">
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <div class="input-group choose_file mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" name="file" class="custom-file-input" id="inputGroupFile01"
                                                    aria-describedby="inputGroupFileAddon01">
                                                <label class="custom-file-label" for="inputGroupFile01">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="mx-auto">
                            <button type="submit" name="client_pic" class="btn corner_set btn_color">Change / Upload Photo</button>
                            </div>

                        </div>
                    </div>
                    <div class="bg_color_set p-3 mt-3">
                        <h4 class="text-white pt-1">Contact List</h4>
                    </div>
                    <div class="bg-white">
                    <div class="text after-edit clone-sec">
                            <div class="alert flex-wrap flex-row alert-dark d-flex justify-content-between">
                              <div>Active Contact</div>
                              <div class="text-right clicks">
                                <span class="ingredients_eye"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                <span class="ingredients_show">Show</span>
                                <span class="ingredients_delete">Deleted</span>
                              </div>
                            </div>
                            <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 clone-row">
                              <!-- <div class="mr-2"><input class="form-control ingredient_auto" type="text" value="" name="item_name[]" id=""></div> -->
                              <div>
                                <span><input class="form-control" type="text" value="<?php echo $clientdata['first_name']." ".$clientdata['last_name']; ?>" id="qty"></span>
                                <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                              </div>
                            </div>

                            <div class="clone-result"></div>

                            <div class="text-center ">
                              <button class="btn add_ingredient corner_set btn_color" type="submit" name="client_pic">Add Contact</button>
                            </div>
                          </div>
                    </div>


                </div>
                <div class="col-md-8">
                    <div class="bg_color_set p-4">
                        <h4 class="text-white d-inline"><?php echo $clientdata['first_name']." ".$clientdata['last_name']; ?> -Contact Details</h4>
                        <label class="form-check-label float-right">
                            <input type="checkbox" class="form-check-input text-white" value=""><i
                                class="fa fa-phone-square fa-fw text-white contact_font"></i><span
                                class="text-white contact_font"> Day Of
                                Contact</span>
                        </label>
                    </div>
                    <div class="bg-white">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                                aria-labelledby="v-pills-home-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="general_info">General Info</p>
                                        <hr class="w-80 ml-4 p-0">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="p-2 mt-4">
                                                    <select class="form-control corner_set mt-2" id="sel1"
                                                        name="name_prefix"value="<?php echo $clientdata['name_prefix']; ?>">
                                                        <option value="Mr.">Mr.</option>
                                                        <option value="Ms.">Ms.</option>
                                                        <option value="Mrs.">Mrs.</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <label for="name" class="mt-2 ml-0">First Name</label>
                                                <input type="text" name="first_name" class="form-control corner_set" value="<?php echo $clientdata['first_name']; ?>">
                                                <label for="name" class="mt-2 ml-0">Last Name</label>
                                                <input type="text" name="last_name" class="form-control corner_set"
                                                    value="<?php echo $clientdata['last_name']; ?>">
                                                <label for="name" class="mt-2 ml-0">Role</label>
                                                <input type="text" name="role" class="form-control corner_set"
                                                    value="<?php echo $clientdata['role']; ?>">
                                            </div>
                                        </div>
                                        <p class="general_info">Mailing Address</p>
                                        <hr class="w-80 ml-4 p-0">
                                        <label for="name" class="mt-0 ml-2">Address</label>
                                        <input type="text" name="address" class="form-control corner_set ml-2 mr-2"
                                            value="<?php echo $clientdata['address']; ?>">
                                        <label for="name" class="mt-2 ml-2">City</label>
                                        <input type="text" name="city" class="form-control corner_set ml-2 mr-2"
                                            value="<?php echo $clientdata['city']; ?>">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class=" ">
                                                    <label for="name" class="mt-2 ml-2 ">State</label>
                                                    <input type="text" name="state" class="form-control corner_set ml-2 mr-2"
                                            value="<?php echo $clientdata['state']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="name" class="mt-2">ZIP</label>
                                                <input type="number" name="zip" class="form-control corner_set ml-2" value="<?php echo $clientdata['zip']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="general_info">Contact Info</p>
                                        <hr class="w-80 ml-4 mr-3">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label for="name" class="mt-2 ml-0">Phone 1</label>
                                                <input type="text" name="phone_one" class="form-control corner_set"
                                                    value="<?php echo $clientdata['phone_one']; ?>">
                                                <label for="name" class="mt-2 ml-0">Phone 2</label>
                                                <input type="text" name="phone_two" class="form-control corner_set" value="<?php echo $clientdata['phone_two']; ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mr-3">
                                                    <label for="name" class="mt-2 ml-0">Phone Type</label>
                                                    <select class="form-control corner_set mr-4" id="sel1"
                                                        name="phone_type_one" value="<?php echo $clientdata['phone_type_one']; ?>">
                                                        <option value="Mobile">Mobile</option>
                                                        <option value="Landline">Landline</option>
                                                    </select>
                                                </div>
                                                <div class="mr-3">
                                                    <label for="name" class="mt-2 ml-0">Phone Type</label>
                                                    <select class="form-control corner_set mr-4" id="sel1"
                                                        name="phone_type_two" value="<?php echo $clientdata['phone_type_two']; ?>">
                                                        <option value="Mobile">Mobile</option>
                                                        <option value="Landline">Landline</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mr-3">
                                            <label for="name" class="mt-2 ml-0">Email-Id</label>
                                            <input type="email" name="email" class="form-control corner_set"
                                                value="<?php echo $clientdata['email']; ?>">
                                        </div>
                                        <p class="general_info ">Contact Notes</p>
                                        <hr class="w-80 ml-4 mr-3">
                                        <div class="form-group mr-3 pt-4">
                                            <textarea class="form-control corner_set" rows="5" id="comment" name="contact_notes" value="<?php echo $clientdata['contact_notes']; ?>"></textarea>
                                        </div>
                                    </div>

                                    <div class=" mx-auto">
                                        <button type="submit" name="client_pic" class="btn corner_set btn_color">Save Contact
                                            Info</button>
                                    </div>

                                </div>

                            </div>
                            <!-- <div class="tab-pane fade" id="v-pills-profile" role="tabpanel"
                                aria-labelledby="v-pills-profile-tab">
                                dolor sit amet, consectetur adipisicing elit. Eligendi beatae atque veniam illum, ex
                                amet omnis quae, vitae, similique quasi dolor itaque eum, vel incidunt minima? Amet
                                debitis aliquid, similique!
                            </div> -->

                        </div>
                    </div>
                </div>
            </div>
            <?php 
                }
            }
            ?>
            </form>
        </section>
        <script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>


</body>

</html>