<?php
include 'db.php';
if (isset($_POST['img_upload'])) {

$name            = $_FILES['file']['name'];
$recipe_name     = $_POST['recipe_name'];
$description     = $_POST['description'];
$markup          = $_POST['markup'];
$price           = $_POST['price'];
$category        = $_POST['category'];
$tempstyles      = $_POST['styles'];
$styles          = implode(",", $tempstyles);
$is_active       = $_POST['is_active'];
$source          = $_POST['source'];
$in              = implode(",",$_POST['item_name']);
$qt              = implode(",",$_POST['qty']);
$target_dir      = "img/";
$target_file     = $target_dir . basename($_FILES["file"]["name"]);
// Select file type
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Valid file extensions
$extensions_arr = array("jpg", "jpeg", "png", "gif", "webp");
// Check extension
  if (in_array($imageFileType, $extensions_arr)) {
    // Insert record
    $query = "INSERT INTO `gallery` (`name`, `recipe_name`, `description`, `markup`, `price`, `category`, `styles`, `is_active`, `source`,`item_name`,`qty`) VALUE ('" . $name . "', '$recipe_name', '$description', '$markup', '$price', '$category', '$styles', '$is_active', '$source','$in','$qt')";
    if(!mysqli_query($con, $query)){
      echo(mysqli_error($con)); exit();
    }
    // Upload file
    move_uploaded_file($_FILES['file']['tmp_name'], $target_dir . $name);
  }
  if (isset($in, $qt)) {
    $query1 = "INSERT INTO `ingredients` (`recipe_name`, `item_name`, `qty`) VALUE ('$recipe_name', '$in', '$qt')";
    mysqli_query($con, $query1);
  }
  if (isset($_POST["hide_header"])) {
    header("location: recipe-gallery.php?hide_header=1");
  } else {
    header("location: recipe-gallery.php");
  }
  
}
?>