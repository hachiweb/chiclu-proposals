<?php 
        $resource_detail_sql =  "SELECT * FROM `gallery` WHERE source='Recipe'";  
        $resource_detail_res = mysqli_query($con, $resource_detail_sql);
        while($row = mysqli_fetch_assoc($resource_detail_res)){ ?>
          <div class="modal fade custom-modal" id="recipe_model<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
              <div class="modal-content">
                <form action="sub-edit.php" method="post" enctype="multipart/form-data">
                <?php
         if (isset($_GET["hide_header"])) {
           echo "<input type='hidden' name='hide_header' value='1'>";
         }
      ?>
                  <div class="modal-header bg-info text-white flex-wrap">
                    <h5 class="modal-title" id="exampleModalCenterTitle"><?php echo $row['recipe_name']; ?></h5>

                    <button id="edit-btn<?php echo $row['id']; ?>" type="button" class="btn btn-outline-light ml-sm-auto editbtn">
                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      <span>Edit</span>
                    </button>
                    <button type="submit" value="Upload" name="img_upload" class="btn btn-outline-light ml-2">
                      <i class="fa fa-check-circle" aria-hidden="true"></i>
                      <span>Save</span>
                    </button>
                    <button type="button" class="btn btn-outline-light ml-2" onclick="imgDeleteRecipe(<?php echo $row['id']; ?>)">
                      <i class="fa fa-trash" aria-hidden="true"></i>
                      <span>Delete</span>
                    </button>
                    <script>
                      function imgDeleteRecipe(id) {
                        if(confirm("Are you sure you want to Delete?")){
                          del="delete.php?id="+id+"&source=Recipe<?php if (isset($_GET["hide_header"])) {
                        echo "&hide_header=1";
                      } ?>";
                          window.location.href = del;
                        }

                      }
                    </script>
                    <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                    <div id="before-edit" class="row before-edit">
                      <div class="col-lg-4">
                        <div class="image-dv mb-5">
                          <h6 class="heading">Recipe Image</h6>
                          <div class="img">
                            <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                          </div>
                        </div>

                        <div class="image-dv image-dv-thumb">
                          <h6 class="heading">Ingredient Images</h6>
                          <div class="row row-5">
                            <?php
                            $id =  $row['id'];
                            $match = explode(",",$row['item_name']);
                            $match = implode("','",$match); //str_replace(' ','',implode("','",$match));
                            $ingsql = "SELECT * FROM `item_gallery` WHERE `item_name` IN ('".$match."')";
                            $ingresult = mysqli_query($con, $ingsql);
                            while($ing = mysqli_fetch_assoc($ingresult)){
                              $dsc[$ing['item_name']] = $ing['default_stem_cost'];
                          ?>
                          <div class="col-4">
                            <div class="img"><img src="<?php echo "img/".$ing['name']; ?>" alt="Image"></div>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->
                  <div class="col-lg-8">
                    <div class="entry-txt mb-4">
                      <div class="row">
                        <div class="col-md-6">
                          <h6 class="heading">Description</h6>
                          <div class="text mih-200 mb-4">
                            <p><?php echo $row['description']; ?></p>
                          </div>

                          <h6 class="heading">Ingredients</h6>
                          <div class="text">
                            <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2">
                              <span>Item Name</span>
                              <span>Qty.</span>
                            </div>
                            <?php
                            $id =  $row['id'];
                            $get_item_name = explode(",",$row['item_name']);

                            foreach($get_item_name as $key1=>$val1){
                              $item_data[$key1]['item'] = $val1;
                            }
                            
                            $get_item_qty = explode(",",$row['qty']);
                            foreach($get_item_qty as $key2=>$val2){
                              $item_data[$key2]['qty'] = $val2;
                            }

                            $dscost=0;
                            foreach($item_data as $key=>$data){
                              $dscost += $dsc[$data['item']]*$data['qty'];
                              ?>
                              <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                                <span><?php echo $data['item']; ?></span>
                                <span><?php echo $data['qty']; ?></span>
                              </div>
                            <?php } ?>
                          </div>
                        </div><!-- col -->                       

                        <div class="col-md-6">
                          <h6 class="heading">Rates</h6>
                          <div class="text mb-4">
                            <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                              <span class="mr-3">Cost</span>
                              <span>$<?php echo number_format($dscost, 2, '.', ''); ?></span>
                            </div>
                            <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                              <span class="mr-3">Markup</span>
                              <span><?php echo $row['markup']; ?></span>
                            </div>
                            <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                              <span class="mr-3">Price</span>
                              <span>$<?php echo number_format((float)$row['price'], 2, '.', ''); ?></span>
                            </div>
                          </div>

                          <h6 class="heading">Category</h6>
                          <div class="text mb-4"><?php echo $row['category']; ?></div>

                          <h6 class="heading">Styles</h6>
                          <div class="text mb-4"><?php echo $row['styles']; ?></div>

                          <h6 class="heading">Colors</h6>
                          <div class="text mb-4">
                            Green, Silver, Brown, Neutral, White, Pink, Orange
                          </div>

                          <h6 class="heading">Months</h6>
                          <div class="text mb-4">
                            May, June, July, August, September, October, November, December
                          </div>
                        </div><!-- col -->
                      </div><!-- row -->
                    </div>

                  </div><!-- col -->
                </div><!-- row -->

                <div id="after-edit" class="row after-edit">
                  <div class="col-lg-4">

                    <div class="image-dv mb-5">
                      <h6 class="heading">Recipe Image</h6>
                      <div class="img">
                        <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                      </div>
                      <div><input type="hidden" name="source" value="Recipe"></div>
                    </div>
                    <div class="input-group mb-3">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                      </div>
                    </div>

                    <div class="image-dv">
                      <h6 class="heading">Ingredient Images</h6>
                      <div class="row row-5">
                        <?php
                        $id =  $row['id'];
                        $ingresult2 = mysqli_query($con, $ingsql);
                        while($ing2s = mysqli_fetch_assoc($ingresult2)){ ?>
                          <div class="col-4">
                            <div class="img"><img src="<?php echo "img/".$ing2s['name']; ?>" alt="Image"></div>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div><!-- col -->

                  <div class="col-lg-8">
                    <div class="entry-txt mb-4">
                      <div class="row">
                        <div class="col-md-6">
                          <h6 class="heading">Recipe Name</h6>
                          <div class="text mb-4">
                            <input type="text" class="form-control" value="<?php echo $row['recipe_name']; ?>" name="recipe_name">
                          </div>
                          <div><input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>"></div>

                          <h6 class="heading">Description</h6>
                          <div class="text mb-4">
                            <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                          </div>

                          <div class="text mb-5 after-edit">
                            <div class="alert flex-wrap flex-row alert-dark d-flex justify-content-between mb-2">
                              <div>Ingredients</div>
                              <div>Show Deleted</div>
                            </div>
                            <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                              <div>Item Name</div>
                              <div>Qty.</div>
                            </div>
                            <?php
                            $id =  $row['id'];
                            foreach($item_data as $key=>$data){ ?>

                              <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 clone-row">

                                <div class="mr-2">
                                  <input class="form-control ingredient_auto" type="text" value="<?php echo $data['item']; ?>" name="item_name[]" id="">
                                </div>
                                <div>
                                  <span><input class="form-control" type="number" value="<?php echo $data['qty']; ?>" name="qty[]" id=""></span>
                                  <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                                </div>
                              </div>
                            <?php } unset($item_data);?>
                            <div class="clone-result"></div>

                          </div>

                          <div class="my-5">
                            <button class="btn btn-info edit_ingredient_btn" type="button">Add Ingredient</button>
                          </div>
                        </div><!-- col -->                       

                        <div class="col-md-6">
                          <h6 class="heading">Rates</h6>
                          <div class="text mb-4 after-edit">
                            <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                              <span class="mr-3">Cost</span>
                              <span>$<?php echo number_format($dscost, 2, '.', ''); ?></span>
                            </div>
                            <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                              <span class="mr-3">Markup</span>
                              <span><input type="text" value="<?php echo $row['markup']; ?>" name="markup" class="form-control"></span>
                            </div>
                            <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                              <span class="mr-3">Price</span>
                              <span><input type="price" value="<?php echo $row['price']; ?>" name="price" class="form-control"></span>
                            </div>
                          </div>

                          <h6 class="heading">Category</h6>
                          <div class="text mb-4">
                            <select name="category" id="category" class="form-control">
                              <?php
                              $category2 = array('Arch Arrangement','Bouquets', 'Boutonnieres', 'Cake Flowers', 'Chair Back Flowers', 'Chandelier Floral', 'Corsages', 'Crowns Floral', 'Flowers Girl Baskets', 'Garlands', 'Petals Designs Roses', 'Short Centerpiece', 'Sweetheart Table Centerpiece', 'Tall Centerpiece');
                              for($i=0; $i<sizeof($category2); $i++){ ?>
                                <option value="<?php echo $category2[$i] ?>"><?php echo $category2[$i] ?></option>
                              <?php } ?>
                            </select>
                          </div>

                          <h6 class="heading">Styles</h6>
                          <div class="text mb-4 check-bx">
                            <?php
                            $style2 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                            for($i=0; $i<sizeof($style2); $i++){ ?>
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="checkbox" name="styles[]" class="form-check-input" value="<?php echo $style2[$i] ?>"
                                  <?php if (strpos($row['styles'], $style2[$i]) !== false) {
                                    echo 'checked';}?>><?php echo $style2[$i] ?>
                                  </label>
                                </div>
                              <?php } ?>
                            </div>

                            <h6 class="heading">Show in Gallery</h6>
                            <div class="text mb-4">
                              <select id="is_active" value="<?php echo $row['is_active']; ?>" name="is_active" class="form-control">
                                <option value="Show">Show</option>
                                <option value="Hide">Hide</option>
                              </select>
                            </div>
                          </div><!-- col -->
                        </div><!-- row -->
                      </div>

                    </div><!-- col -->

                  </div><!-- row -->
                </div>
              </form>
            </div>
          </div>
        </div>
      <?php } ?>

      <?php 
      $resource_detail_sql =  "SELECT * FROM `item_gallery` WHERE source='Item'";  
      $resource_detail_res = mysqli_query($con, $resource_detail_sql);
      while($row = mysqli_fetch_assoc($resource_detail_res)){ ?>
        <div class="modal fade custom-modal" id="item_model<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header flex-wrap bg-info text-white">
                <h5 class="modal-title" id="exampleModalCenterTitle"><?php echo $row['item_name']; ?></h5>

                <button id="edit-btn<?php echo $row['id']; ?>" type="button" class="btn btn-outline-light ml-sm-auto editbtn">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  <span>Edit</span>
                </button>
                <button type="button" class="btn btn-outline-light ml-2" onclick="imgDeleteItem(<?php echo $row['id']; ?>)">
                  <i class="fa fa-trash" aria-hidden="true"></i>
                  <span>Delete</span>
                </button>
                <script>
                  function imgDeleteItem(id) {
                    if(confirm("Are you sure you want to Delete?")){
                      del="delete.php?id="+id+"&source=Item<?php if (isset($_GET["hide_header"])) {
                        echo "&hide_header=1";
                      } ?>";
                      window.location.href = del;
                    }

                  }
                </script>
                <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                <div id="before-edit" class="row before-edit">
                  <div class="col-lg-3">
                    <div class="image-dv">
                      <h6 class="heading"><?php echo $row['item_name']; ?></h6>
                      <div class="img">
                        <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                      </div>
                    </div>
                  </div><!-- col -->
                  <div class="col-lg-9">
                    <div class="entry-txt mb-4">
                      <div class="row">
                        <div class="col-md-5">
                          <h6 class="heading">Costs</h6>
                          <div class="text">
                            <div class="alert alert-dark d-flex justify-content-between mb-2">
                              <span>Avg Cost</span>
                              <span>$0.00</span>
                            </div>
                            <div class="alert alert-dark d-flex justify-content-between mb-2">
                              <span>Default Stem Cost</span>
                              <span>$<?php echo number_format((float)$row['default_stem_cost'], 2, '.', ''); ?></span>
                            </div>
                            <div class="alert alert-light d-flex justify-content-between mb-2">
                              <span>Stems Per Bunch</span>
                              <span><?php echo $row['stems_per_bunch']; ?></span>
                            </div>
                          </div>
                        </div><!-- col -->

                        <div class="col-md-3">
                          <h6 class="heading">Colors</h6>
                          <div class="text"><?php echo $row['colors']; ?></div>
                        </div><!-- col -->

                        <div class="col-md-4">
                          <h6 class="heading">Months</h6>
                          <div class="text"><?php echo $row['months']; ?></div>
                        </div><!-- col -->
                      </div><!-- row -->
                    </div>

                    <div class="entry-txt">                            
                      <h6 class="heading">Description</h6>
                      <p><?php echo $row['description']; ?></p>
                    </div>
                  </div><!-- col -->
                </div><!-- row -->

                <div id="after-edit" class="row after-edit">
                  <div class="col-lg-3">
                    <form action="sub-edit-item.php" method="post" enctype="multipart/form-data">
                    <?php
         if (isset($_GET["hide_header"])) {
           echo "<input type='hidden' name='hide_header' value='1'>";
         }
      ?>
                      <div class="image-dv">
                        <h6 class="heading">Item Name</h6>
                        <div class="text mb-4">
                          <input type="text" value="<?php echo $row['item_name']; ?>" name="item_name" class="form-control">
                        </div>
                        <div><input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>"></div>
                        <h6 class="heading">Item Type</h6>
                        <div class="text mb-4">
                          <select class="form-control" value="<?php echo $row['item_type']; ?>" name="item_type" id="item_type">
                            <?php
  $itemtype = array('Flowers', 'Greenery', 'Vases and Candles', 'Lanterns', 'Chargers', 'Chairs', 'Chargers', 'Linens', 'Corsages', 'Drapery', 'Services');      
                            for($i=0; $i<sizeof($itemtype); $i++){ ?>
                              <option value="<?php echo $itemtype[$i] ?>"><?php echo $itemtype[$i] ?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="image-dv mb-3">
                          <h6 class="heading">Item Image</h6>
                          <div class="img">
                            <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                          </div>
                          <div><input type="hidden" name="source" value="Recipe"></div>
                        </div>

                        <div class="custom-file my-4">
                          <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                          <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                      </div>
                    </div><!-- col -->

                    <div class="col-lg-9">
                      <div class="entry-txt mb-4">
                        <div class="row">
                          <div class="col-md-5">
                            <h6 class="heading">Costs</h6>
                            <div class="text mb-4">
                          <?php /*<div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                             <span class="mr-3">Cost</span>
                              <span>$<?php echo number_format(((float)$row['default_stem_cost'])*(int)$row['stems_per_bunch'], 2, '.', ''); ?></span>
                             
                              </div>  */ ?>
                              <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Default Stem Cost</span>
                                <span><input type="price" value="<?php echo $row['default_stem_cost']; ?>" name="default_stem_cost" class="form-control"></span>
                              </div>
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Stems Per Bunch</span>
                                <span><input type="number" value="<?php echo $row['stems_per_bunch']; ?>" name="stems_per_bunch" class="form-control"></span>
                              </div>
                            </div>
                          </div><!-- col -->                       

                          <div class="col-md-3">
                            <h6 class="heading">Colors</h6>
                            <div class="text mb-4 check-bx">
                              <?php $colors2 = array('White', 'Green', 'Red', 'Purple', 'Blue', 'Magenta','Black','Orange','Yellow','Brown','Bush','Valvet');
                              for($i=0; $i<sizeof($colors2); $i++){ ?>
                                <div class="form-check">
                                  <label class="form-check-label">
                                    <input type="checkbox" name="colors[]" class="form-check-input" value="<?php echo $colors2[$i]; ?>"  
                                    <?php if (strpos($row['colors'], $colors2[$i]) !== false) {
                                      echo 'checked';}?>><?php echo $colors2[$i]; ?>
                                    </label>
                                  </div>
                                <?php } ?>
                              </div>
                            </div><!-- col -->

                            <div class="col-md-4">
                              <h6 class="heading">Months</h6>
                              <div class="text mb-4 check-bx">
                                <?php $months2 = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                                for($i=1; $i<=sizeof($months2); $i++){ ?>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                     <input name="months[]" type="checkbox" class="form-check-input" value="<?php echo $months2[$i] ?>"
                                     <?php if (strpos($row['months'], $months2[$i]) !== false) {
                                      echo 'checked';}?>><?php echo $months2[$i] ?>
                                    </label>
                                  </div>
                                <?php } ?>
                              </div>
                            </div><!-- col -->
                          </div><!-- row -->

                          <div class="row">
                            <div class="col-lg-12">
                              <h6 class="heading">Description</h6>
                              <div class="text mb-4">
                                <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                              </div>

                              <div><input type="hidden" name="source" value="Item"></div>
                              <div class="mt-5">
                                <button class="btn btn-info" type="submit" value="Upload" name="img_upload">Update Item</button>
                              </div>
                            </div>
                          </div><!-- row -->

                        </div>

                      </div><!-- col -->
                    </form>
                  </div><!-- row -->

                </div>
              </div>
            </div>
          </div>
        <?php } ?>
