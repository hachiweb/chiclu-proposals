<?php
$page ="NULL";
if(!isset($_SESSION)) 
{ 
  session_start(); 
}  
if(empty($_SESSION["username"])){
  header("location:users/login.php");
  exit();
}
else{
  $username = $_SESSION['username'];
}
include('db.php'); 
?>

<?php
include('header.php');
?> <!--header added-->

<main class="min-height">
  <div class="container-fluid">
    <div class="row row-5">
      <?php
      if (!isset($_GET["hide_header"])) {
        ?>
        <div class="col-lg-4">
          <?php
          $page = "recipe-gallery";
          include('design_board.php'); 
          ?>
        </div>
        <div class="col-lg-8">
          <?php
        } else {
          echo "<div class='col-lg-12'>";
        }

        ?>
        <div class="right-sec py-3">

          <div class="p-3 mb-2 bg-info text-white func-dv">
            <div class="row align-items-center">
              <div class="col-sm-4">
                <div class="btn-dv">
                  <button id="filter-btn" type="button" class="btn btn-outline-light">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    <span>Filter / Search</span>
                  </button>
                </div>
              </div><!-- col -->

              <div class="col-sm-4">

                <h5 class="m-sm-0 font-weight-normal text-sm-center"> 
                  <?php
                  $recipe_count = "SELECT count(*) AS total FROM `gallery`";  
                  $count = mysqli_query($con, $recipe_count);
                  $count = mysqli_fetch_assoc($count);
                  echo 'Showing '.$count['total'].' recipes.';
                  ?>
                </h5>
              </div><!-- col -->

              <div class="col-sm-4 text-sm-right">
                <div class="btn-dv">
                  <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#newItem">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span>New Recipe</span>
                  </button>

                  <button id="table-image-btn" type="button" class="btn btn-outline-light ml-2">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span>Table</span>
                  </button>
                </div>
              </div><!-- col -->
            </div><!-- row -->
          </div>

          <div id="filter-dv" class="p-3 mb-2 bg-white filter-bx">
            <form action="search.php" method="get" >
              <div class="row">
                <div class="col-md-4">
                  <div class="entry-txt">
                    <h6 class="heading">Colors</h6>
                    <div class="text mb-4 check-bx">
                      <?php $colorslist = array('White', 'Green', 'Red', 'Purple', 'Blue', 'Magenta','Black','Orange','Yellow','Brown','Bush','Valvet');
                      for($i=0; $i<sizeof($colorslist); $i++){ ?>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input type="checkbox" name="colors[]" class="form-check-input" value="<?php echo $colorslist[$i]; ?>"><?php echo $colorslist[$i]; ?>
                          </label>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div><!-- col -->

                <div class="col-md-4">
                  <div class="entry-txt">
                    <h6 class="heading">Months</h6>
                    <div class="text mb-4 check-bx">
                      <?php $monthslist = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                      for($i=1; $i<=sizeof($monthslist); $i++){ ?>
                       <div class="form-check">
                        <label class="form-check-label">
                          <input name="months[]" type="checkbox" class="form-check-input month-filter" value="<?php echo $monthslist[$i] ?>"><?php echo $monthslist[$i] ?>
                        </label>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div><!-- col -->

              <div class="col-md-4">
                <div class="entry-txt">
                  <h6 class="heading">Categories</h6>
                  <div class="text mb-4 check-bx">
                    <?php
                    $categories = array('Arch Arrangement','Bouquets', 'Boutonnieres', 'Cake Flowers', 'Chair Back Flowers', 'Chandelier Floral', 'Corsages', 'Crowns Floral', 'Flowers Girl Baskets', 'Garlands', 'Petals Designs Roses', 'Short Centerpiece', 'Sweetheart Table Centerpiece', 'Tall Centerpiece');
                    for($i=0; $i<sizeof($categories); $i++){ ?>
                      <div class="form-check">
                        <label class="form-check-label">
                          <input type="checkbox" name="categories[]" class="form-check-input" value="<?php echo $categories[$i] ?>"><?php echo $categories[$i] ?>
                        </label>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div><!-- col -->
            </div><!-- row -->

            <div class="filter-form-bx">
              <div class="row mt-4 align-items-end">
                <div class="col-md-3">
                 <div class="form-group">
                  <label for="uname">Search By</label>
                  <select name="search_by" id="search_by" class="form-control">
                    <option value="Recipe">Recipe</option>
                    <option value="Item">Item</option>
                  </select>
                </div>
              </div><!-- col -->

              <div class="col-md-6">
               <div class="form-group">
                <label for="uname">Search Term:</label>
                <input name="search_term" type="text" class="form-control">
              </div>
            </div><!-- col -->

            <div class="col-md-3">
             <div class="form-group">
              <button class="btn btn-info" type="submit">Search/Filter</button>
              <button class="btn btn-info" type="Reset">Clear</button>
            </div>
          </div><!-- col -->
        </div><!-- row -->
      </div>
    </form>
  </div>

  <!-- Image Click Modal -->
  <div class="modal fade custom-modal" id="newItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
      <div class="modal-content">
        <form action="sub-recipe.php" method="post" enctype="multipart/form-data">
          <?php
          if (isset($_GET["hide_header"])) {
           echo "<input type='hidden' name='hide_header' value='1'>";
         }
         ?>
         <div class="modal-header bg-info text-white">
          <h5 class="modal-title" id="exampleModalCenterTitle">Add Recipe</h5>

          <button type="submit" value="Upload" name="img_upload" class="btn btn-outline-light ml-sm-auto">
            <i class="fa fa-check-circle" aria-hidden="true"></i>
            <span>Save</span>
          </button>
            <!-- <button type="submit" value="" name="" class="btn btn-outline-light ml-2">
              <i class="fa fa-trash" aria-hidden="true"></i>
              <span>Delete</span>
            </button> -->
            <button type="button" class="close btn-outline-light ml-2" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-3">

                <div class="image-dv  mb-3">
                  <h6 class="heading">Recipe Image</h6>
                  <div class="img">
                    <img src="img/default.png" id="preview_recipe" alt="Image">
                  </div>
                </div>
                <div class="input-group mb-3">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                  </div>
                </div>
                <!-- <div class="text-center"><br><button class="btn btn-info add_ingredient" id="recipe_img_btn">Change / Upload Image</button></div> -->
              </div><!-- col -->
              <div class="col-lg-9">
                <div class="entry-txt mb-4">
                  <div class="row">
                    <div class="col-md-6">
                      <h6 class="heading">Recipe Name</h6>
                      <div class="text mb-4">
                        <input type="text" class="form-control" name="recipe_name" >
                      </div>

                      <h6 class="heading">Description</h6>
                      <div class="text mb-4">
                        <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                      </div>
                      <div>
                        <input type="hidden" name="source" value="Recipe">
                      </div>

                      <div class="text mb-4 after-edit clone-sec">
                        <div class="alert flex-wrap flex-row alert-dark d-flex justify-content-between mb-2">
                          <div>Ingredients</div>
                          <div class="text-right clicks">
                            <span class="ingredients_eye"><i class="fa fa-eye" aria-hidden="true"></i></span>
                            <span class="ingredients_show">Show</span>
                            <span class="ingredients_delete">Deleted</span>
                          </div>
                        </div>
                        <div class="alert flex-wrap alert-light d-flex justify-content-between mb-2">
                          <div>Item Name</div>
                          <div class="text-right">Qty.</div>
                        </div>
                        <div class="alert flex-wrap alert-dark d-flex justify-content-between mb-2 clone-row">
                          <div class="mr-2"><input class="form-control ingredient_auto" type="text" value="" name="item_name[]" id=""></div>
                          <div>
                            <span><input class="form-control" type="number" value="" name="qty[]" id="qty"></span>
                            <span class="cross"><i class="fa fa-times" aria-hidden="true"></i></span>
                          </div>
                        </div>

                        <div class="clone-result"></div>

                        <div class="mt-5">
                          <button class="btn btn-info add_ingredient" type="button">Add Ingredient</button>
                        </div>
                      </div>
                    </div><!-- col -->                       

                    <div class="col-md-6">
                      <h6 class="heading">Rates</h6>
                      <div class="text mb-4 after-edit">
                            <?php /*<div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                              <span class="mr-3">Cost</span>
                              <span>$<?php echo number_format($dscost, 2, '.', ''); ?></span>
                              </div> */ ?>
                              <div class="alert alert-light d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Markup</span>
                                <span><input type="text" class="form-control" name="markup" ></span>
                              </div>
                              <div class="alert alert-dark d-flex justify-content-between mb-2 p-2 align-items-center">
                                <span class="mr-3">Price</span>
                                <span><input type="price" class="form-control" name="price" ></span>
                              </div>
                            </div>

                            <h6 class="heading">Category</h6>
                            <div class="text mb-4">
                              <select name="category" id="category" class="form-control">
                                <?php
                                $category1 = array('Arch Arrangement','Bouquets', 'Boutonnieres', 'Cake Flowers', 'Chair Back Flowers', 'Chandelier Floral', 'Corsages', 'Crowns Floral', 'Flowers Girl Baskets', 'Garlands', 'Petals Designs Roses', 'Short Centerpiece', 'Sweetheart Table Centerpiece', 'Tall Centerpiece');
                                for($i=0; $i<sizeof($category1); $i++){ ?>
                                  <option value="<?php echo $category1[$i] ?>"><?php echo $category1[$i] ?></option>
                                <?php } ?>
                              </select>
                            </div>

                            <h6 class="heading">Styles</h6>
                            <div class="text mb-4 check-bx">
                              <?php
                              $style1 = array('Beach', 'Birthday', 'Destination', 'Edgy', 'Elegant', 'Garden', 'Holiday', 'Modern', 'Nautical', 'Organic', 'Outdoor', 'Romantic', 'Rustic', 'Tall', 'Themed', 'Traditional', 'Vintage');
                              for($i=0; $i<sizeof($style1); $i++){ ?>
                                <div class="form-check">
                                  <label class="form-check-label">
                                    <input type="checkbox" name="styles[]" class="form-check-input" value="<?php echo $style1[$i] ?>"><?php echo $style1[$i] ?>
                                  </label>
                                </div>
                              <?php } ?>
                            </div>

                            <h6 class="heading">Show in Gallery</h6>
                            <div class="text mb-4">
                              <select name="is_active" id="is_active" class="form-control">
                                <option value="Show">Show</option>
                                <option value="Hide">Hide</option>
                              </select>
                            </div>
                          </div><!-- col -->
                        </div><!-- row -->
                      </div>

                    </div><!-- col -->

                  </div><!-- row -->
                </div>
              </form>
            </div>
          </div>
        </div><!-- modal -->

        <?php

        $get_image = "SELECT * FROM `gallery` WHERE source='Recipe'";  
        $result = mysqli_query($con, $get_image); ?>
        <div id="image-grid-bx" class="image-grid-bx">
          <div class="row row-5">
            <?php while($img = mysqli_fetch_assoc($result)){ ?>
              <div class="col-lg-4 <?php if (isset($_GET["hide_header"])) {echo 'col-xl-3';} ?> col-md-4 col-sm-6 col" >
                <div class="image-grid-dv d-flex flex-column justify-content-end">
                  <div class="img">

                    <?php
                    if (!isset($_GET["hide_header"])) {
                      ?>
                      <button style="<?php if(in_array($img['name'],$recipe_images)){echo " ";}else{echo "display:none";} ?>" type="button" class="btn star-btn text-right" onclick="return del_img(this,'recipe')" ><i class="fa fa-star fa-fw text-warning"></i></button>
                      <button style="<?php if(in_array($img['name'],$recipe_images)){echo "display:none";}else{echo " ";} ?>" type="button" class="btn star-btn text-right" onclick="return save_img(this,'recipe',<?=$img['id']?>)" ><i class="fa fa-star fa-fw"></i></button>

                      <?php
                    }

                    ?>

                    <img data-toggle="modal" data-target="#recipe_model<?php echo $img['id']; ?>" src="<?php echo "img/".$img['name']; ?>" alt="Image">
                  </div><!-- img -->
                  <h6 class="title m-0 p-3"><?php echo $img['recipe_name']; ?></h6>
                </div>
              </div><!-- col -->
            <?php } ?>

          </div><!-- row -->
        </div><!-- image-grid-bx -->

        <?php
        $tblsql = "SELECT * FROM `gallery` WHERE source='Recipe'";  
        $tblresult = mysqli_query($con, $tblsql); ?>
        <div id="table-list-bx" class="table-list-bx bg-white d-none">
          <table class="table table-striped">
            <thead>
              <tr>
                <th></th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php while($tbl = mysqli_fetch_assoc($tblresult)){ ?>
                <tr>
                  <td data-toggle="modal" data-target="#model1">
                    <div class="img max-50">
                      <img class="img-thumbnail" src="<?php echo "img/".$tbl['name']; ?>" alt="Image">
                    </div>
                  </td>
                  <td><?php echo $tbl['recipe_name']; ?></td>                
                  <td class="img max-50"><?php echo $tbl['description']; ?></td>                
                  <td>$<?php echo $tbl['price']; ?></td>                
                  <td>
                    <span class="delete"><i class="fa fa-times" onclick="imgDelete(<?php echo $tbl['id']; ?>)" aria-hidden="true"></i></span>
                  </td>                
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div><!-- table-list-bx -->
        <?php
        include "pop-up-modal.php";
        ?>

        <!-- Image Click Modal -->



      </div>
    </div><!-- col -->
  </div><!-- row -->
</div><!-- container-fluid -->
</main>
</div><!-- wrapper -->

<!-- <script src="js/custom.js"></script> -->
<script src="js/jquery.auto-complete.min.js"></script>

</body>
</html>