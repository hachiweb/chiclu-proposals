<section class="dropdown-click-sec p-2 mb-5 dropdown-cat-group">
    <div class="heading px-3">
        <span class="dropdown-click">
            <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
        <a href="javascript:void(0);" class="text-black">
            <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
            <span>Category Group</span>
        </a>
        <span class="float-right">
            <span class="delete-clk">
                <i class="fa fa-trash fa-fw"></i>
                <span class="txt">Remove Section</span>
            </span>
        </span>
    </div>

    <div class="dropdown-bx">
        <table class="table table-striped table-list m-0 text-center">
            <thead>
                <tr>
                    <?php /* ?>
                    <th>Wholesaler</th>
                    <th>Total Stems Total Quote Expiration</th>
                    <th>Ask Total</th>
                    <th>Status</th>
                    <th>Quote Total</th>
                    <th>Quote Expiration</th>
                    <?php */ ?>

                    <th></th>
                    <th>Item Name</th>
                    <th></th>
                    <th>Qty</th>
                    <th>Est Price</th>
                    <th>Lock</th>
                    <th>Total</th>
                    <th>Tax</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="body-clone-category">
               <?php 
               for ($i=0; $i < count($item_name); $i++) { 
                  ?>
                  <tr>
                    <td><span><i class="fa fa-arrows-v" aria-hidden="true"></i></span></td>
                    <td>
                        <button type="button" class="item-name-bx">
                            <span class="item-name-txt"><?=empty($item_name[$i])?"Bride":$item_name[$i]; ?></span>
                            <input type="text" name="item_name[]" id="" class="item-name-input"
                            oninput="set_val(this)" value="<?=$item_name[$i]; ?>">

                        </button>
                    </td>
                    <td><span><i class="fa fa-plus" aria-hidden="true"></i></span></td>
                    <td><input type="text" name="item_qty[]" id="" value="<?=empty($item_qty[$i])?"1":$item_qty[$i]; ?>"></td>
                    <td><input type="text" name="item_price[]" id="" value="<?=empty($item_price[$i])?"$0.00":$item_price[$i]; ?>"></td>
                    <td>
                        <span class="lock"><i class="fa fa-unlock" aria-hidden="true"></i></span>
                    </td>
                    <td>$0.00</td>
                    <td>TP</td>
                    <td><span class="cross-td"><i class="fa fa-times" aria-hidden="true"></i></span>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tfoot>
                <tr>
                    <td class="p-0" colspan="9"><button type="button" class="btn w-100 add-row">+
                    Add New Row</button></td>
                </tr>
                <tr>
                    <th class="text-right" colspan="8">Section Subtotal:</th>
                    <th>$0.00</th>
                </tr>
            </tfoot>
        </tbody>

    </table>

</div><!-- dropdown-bx -->
</section><!-- dropdown-click-sec -->