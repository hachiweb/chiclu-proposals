<?php
$page ="NULL";
if(!isset($_SESSION)) 
{ 
    session_start(); 
}  
if(empty($_SESSION["username"])){
    header("location:users/login.php");
    exit();
}
else{
    $username = $_SESSION["username"];
}
include('db.php'); 
if(isset($_POST['img_upload'])){
 
  $name = $_FILES['file']['name'];
  $source          = $_POST['source'];
  $target_dir = "img/";
  $target_file = $target_dir . basename($_FILES["file"]["name"]);

  // Select file type
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  // Valid file extensions
  $extensions_arr = array("jpg","jpeg","png","gif", "webp");

  // Check extension
  if( in_array($imageFileType,$extensions_arr) ){
 
     // Insert record
     $query = "INSERT INTO `gallery`(`name`, `source`) values('".$name."','$source')";
     mysqli_query($con,$query);
  
     // Upload file
     move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);

  }
 
}
?>

<?php
include('header.php');
?> <!--header added-->

    <main class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-4">
            
          </div><!-- col -->
          <div class="col-lg-4">
            <div class="left-sec py-3">
              <div class="p-3 mb-3 bg-info text-white">
                <h4 class="m-0">Upload Images</h4>                
              </div>

              <div class="container entry-txt p-4 bg-white">
                <h6 class="heading">Inspiration Gallery</h6>

                <form method="post" action="inspiration-gallery.php" enctype='multipart/form-data'>
                <input class="mb-2"type='file' name='file' />
                <input class="btn btn-primary mb-2" type='submit' value='Upload' name='img_upload'>
                <div>
                <input type="hidden" name="source" value="Inspiration">
                </div>
                </form>
                <?php if(mysqli_query($con,$query)) { ?>
                <div>
                  <p>
                  <?php echo"Image Uploaded Successfully"; }?>
                </p>
              </div>
            </div>
          </div>
          </div><!-- col -->
          <div class="col-lg-4">
            
          </div><!-- col -->
        </div><!-- row -->
        <div class="image-grid-bx">
            <div class="row row-3 ml-3 mr-3 mt-3">
              <?php 
                  include 'db.php';
                  $get_image = "SELECT * FROM `gallery` WHERE source='Inspiration'";  
                  $result = mysqli_query($con, $get_image);
                  while($row = mysqli_fetch_assoc($result)){ ?>
                    <div class="col-md-4 col-sm-6 col" data-toggle="modal" data-target="#model<?php echo $row['id']; ?>">
                    <div class="image-grid-dv d-flex flex-column justify-content-end">
                      <div class="img">
                        <img src="<?php echo "img/".$row['name']; ?>" alt="Image">
                      </div><!-- img -->
                      <h5 class="title m-0 p-3"><?php echo $row['name']; ?></h5>
                    </div>
                    <button type="button" class="btn btn-danger float-right mt-3" onclick="imgDelete(<?php echo $row['id']; ?>)">
                        <span>Delete</span>
                      </button>
                      <script>
                        function imgDelete(id) {
                          if(confirm("Are you sure you want to Delete?")){
                            del="delete.php?id="+id+"&source=Inspiration";
                            window.location.href = del;
                          }
                        
                        }
                        </script>
                  </div><!-- col -->
                  <?php $imgdata['name'] = $row['name']; } ?>
                  
                </div><!-- row -->
              </div><!-- image-grid-bx -->
      </div><!-- container-fluid -->
    </main>
  </div><!-- wrapper -->
</body>
</html>
